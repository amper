/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module egfx.util;

import iv.alice;


// ////////////////////////////////////////////////////////////////////////// //
alias KIdentity(T...) = T[0];


// ////////////////////////////////////////////////////////////////////////// //
public struct KRC(T) if (is(T == struct)) {
private:
  usize intrp__;

private:
  static void doIncRef (usize ox) nothrow @trusted @nogc {
    pragma(inline, true);
    if (ox) {
      *cast(uint*)(ox-uint.sizeof) += 1;
      version(krc_debug) { import core.stdc.stdio : printf; printf("doIncRef for 0x%08x (%u)\n", cast(uint)ox, *cast(uint*)(ox-uint.sizeof)); }
    }
  }

  static void doDecRef() (ref usize ox) {
    if (ox) {
      version(krc_debug) { import core.stdc.stdio : printf; printf("doDecRef for 0x%08x (%u)\n", cast(uint)ox, *cast(uint*)(ox-uint.sizeof)); }
      if ((*cast(uint*)(ox-uint.sizeof) -= 1) == 0) {
        // kill and free
        scope(exit) {
          import core.stdc.stdlib : free;
          import core.memory : GC;

          version(krc_debug) { import core.stdc.stdio : printf; printf("CG CLEANUP FOR WRAPPER 0x%08x\n", cast(uint)ox); }
          version(krc_debug) import core.stdc.stdio : printf;
          void* mem = cast(void*)ox;
          version(krc_debug) { import core.stdc.stdio : printf; printf("DESTROYING WRAPPER 0x%08x\n", cast(uint)mem); }
          enum instSize = T.sizeof;
          auto pbm = __traits(getPointerBitmap, T);
          version(krc_debug) printf("[%.*s]: size=%u (%u) (%u)\n", cast(uint)T.stringof.length, T.stringof.ptr, cast(uint)pbm[0], cast(uint)instSize, cast(uint)(pbm[0]/size_t.sizeof));
          immutable(ubyte)* p = cast(immutable(ubyte)*)(pbm.ptr+1);
          size_t bitnum = 0;
          immutable end = pbm[0]/size_t.sizeof;
          while (bitnum < end) {
            if (p[bitnum/8]&(1U<<(bitnum%8))) {
              size_t len = 1;
              while (bitnum+len < end && (p[(bitnum+len)/8]&(1U<<((bitnum+len)%8))) != 0) ++len;
              version(krc_debug) printf("  #%u (%u)\n", cast(uint)(bitnum*size_t.sizeof), cast(uint)len);
              GC.removeRange((cast(size_t*)mem)+bitnum);
              bitnum += len;
            } else {
              ++bitnum;
            }
          }

          free(cast(void*)(ox-uint.sizeof));
          ox = 0;
        }
        (*cast(T*)ox).destroy;
      }
    }
  }

public:
  this(A...) (auto ref A args) {
    intrp__ = newOx!T(args);
  }

  this() (auto ref typeof(this) src) nothrow @trusted @nogc {
    intrp__ = src.intrp__;
    doIncRef(intrp__);
  }

  ~this () { doDecRef(intrp__); }

  this (this) nothrow @trusted @nogc { doIncRef(intrp__); }

  void opAssign() (typeof(this) src) {
    if (!intrp__ && !src.intrp__) return;
    version(krc_debug) { import core.stdc.stdio : printf; printf("***OPASSIGN(0x%08x -> 0x%08x)\n", cast(void*)src.intrp__, cast(void*)intrp__); }
    if (intrp__) {
      // assigning to non-empty
      if (src.intrp__) {
        // both streams are active
        if (intrp__ == src.intrp__) return; // nothing to do
        auto oldo = intrp__;
        auto newo = src.intrp__;
        // first increase rc for new object
        doIncRef(newo);
        // replace object for this
        intrp__ = newo;
        // release old object
        doDecRef(oldo);
      } else {
        // just close this one
        scope(exit) intrp__ = 0;
        doDecRef(intrp__);
      }
    } else if (src.intrp__) {
      // this is empty, but other is not; easy deal
      intrp__ = src.intrp__;
      doIncRef(intrp__);
    }
  }

  usize toHash () const pure nothrow @safe @nogc { pragma(inline, true); return intrp__; } // yeah, so simple
  bool opEquals() (auto ref typeof(this) s) const { pragma(inline, true); return (intrp__ == s.intrp__); }

  @property bool hasObject () const pure nothrow @trusted @nogc { pragma(inline, true); return (intrp__ != 0); }

  @property inout(T)* intr_ () inout pure nothrow @trusted @nogc { pragma(inline, true); return cast(typeof(return))intrp__; }

  // hack!
  static if (__traits(compiles, ((){T s; bool v = s.valid;}))) {
    @property bool valid () const nothrow @trusted @nogc { pragma(inline, true); return (intrp__ ? intr_.valid : false); }
  }

  alias intr_ this;

static private:
  usize newOx (CT, A...) (auto ref A args) if (is(CT == struct)) {
    import core.exception : onOutOfMemoryErrorNoGC;
    import core.memory : GC;
    import core.stdc.stdlib : malloc;
    import core.stdc.string : memset;
    import std.conv : emplace;
    enum instSize = CT.sizeof;
    // let's hope that malloc() aligns returned memory right
    auto memx = malloc(instSize+uint.sizeof);
    if (memx is null) onOutOfMemoryErrorNoGC(); // oops
    memset(memx, 0, instSize+uint.sizeof);
    *cast(uint*)memx = 1;
    auto mem = memx+uint.sizeof;
    emplace!CT(mem[0..instSize], args);

    version(krc_debug) import core.stdc.stdio : printf;
    auto pbm = __traits(getPointerBitmap, CT);
    version(krc_debug) printf("[%.*s]: size=%u (%u) (%u)\n", cast(uint)CT.stringof.length, CT.stringof.ptr, cast(uint)pbm[0], cast(uint)instSize, cast(uint)(pbm[0]/size_t.sizeof));
    immutable(ubyte)* p = cast(immutable(ubyte)*)(pbm.ptr+1);
    size_t bitnum = 0;
    immutable end = pbm[0]/size_t.sizeof;
    while (bitnum < end) {
      if (p[bitnum/8]&(1U<<(bitnum%8))) {
        size_t len = 1;
        while (bitnum+len < end && (p[(bitnum+len)/8]&(1U<<((bitnum+len)%8))) != 0) ++len;
        version(krc_debug) printf("  #%u (%u)\n", cast(uint)(bitnum*size_t.sizeof), cast(uint)len);
        GC.addRange((cast(size_t*)mem)+bitnum, size_t.sizeof*len);
        bitnum += len;
      } else {
        ++bitnum;
      }
    }
    version(krc_debug) { import core.stdc.stdio : printf; printf("CREATED WRAPPER 0x%08x\n", mem); }
    return cast(usize)mem;
  }
}
