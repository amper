/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module egfx.backx;
private:

import arsd.simpledisplay;

import iv.cmdcon;
import iv.cmdcongl;
import iv.sdpyutil;

import egfx.base : GxRect, c2img;
import egfx.text;
import egfx.util;


// ////////////////////////////////////////////////////////////////////////// //
public struct XlibTCImage {
  XImage* handle;

  @disable this (this);

  this (MemoryImage img) {
    if (img is null || img.width < 1 || img.height < 1) throw new Exception("can't create xlib image from empty MemoryImage");
    create(img.width, img.height, img);
  }

  this (int wdt, int hgt) {
    if (wdt < 1 || hgt < 1) throw new Exception("invalid xlib image");
    create(wdt, hgt, null);
  }

  ~this () { dispose(); }

  @property bool valid () const pure nothrow @trusted @nogc { pragma(inline, true); return (handle !is null); }

  @property int width () const pure nothrow @trusted @nogc { pragma(inline, true); return (handle !is null ? handle.width : 0); }
  @property int height () const pure nothrow @trusted @nogc { pragma(inline, true); return (handle !is null ? handle.height : 0); }

  void setup (MemoryImage aimg) {
    dispose();
    if (aimg is null || aimg.width < 1 || aimg.height < 1) throw new Exception("can't create xlib image from empty MemoryImage");
    create(aimg.width, aimg.height, aimg);
  }

  private void create (int width, int height, MemoryImage ximg) {
    import core.stdc.stdlib : malloc, free;
    if (glconCtlWindow is null || glconCtlWindow.closed) assert(0, "wtf?!");
    auto dpy = glconCtlWindow.impl.display;
    assert(dpy !is null);
    auto screen = DefaultScreen(dpy);
    // this actually needs to be malloc to avoid a double free error when XDestroyImage is called
    auto rawData = cast(uint*)malloc(width*height*4);
    scope(failure) free(rawData);
    if (ximg is null || ximg.width < width || ximg.height < height) rawData[0..width*height] = 0;
    if (ximg && ximg.width > 0 && ximg.height > 0) {
      foreach (immutable int y; 0..height) {
        foreach (immutable int x; 0..width) {
          rawData[y*width+x] = c2img(ximg.getPixel(x, y));
        }
      }
    }
    handle = XCreateImage(dpy, DefaultVisual(dpy, screen), 24/*bpp*/, ImageFormat.ZPixmap, 0/*offset*/, cast(ubyte*)rawData, width, height, 8/*FIXME*/, 4*width); // padding, bytes per line
  }

  void dispose () {
    // note: this calls free() for us
    if (handle !is null) {
      XDestroyImage(handle);
      handle = null;
    }
  }

  // blit to window buffer
  final void blitAt (SimpleWindow w, int destx, int desty) {
    blitRect(w, destx, desty, GxRect(0, 0, width, height));
  }

  // blit to window buffer
  final void blitRect (SimpleWindow w, int destx, int desty, GxRect srect) {
    if (w is null || handle is null || w.closed) return;
    XPutImage(w.impl.display, cast(Drawable)w.impl.buffer, w.impl.gc, handle, srect.x0, srect.y0, destx, desty, srect.width, srect.height);
  }

  // blit to window
  final void blitAtWin (SimpleWindow w, int destx, int desty) {
    blitRectWin(w, destx, desty, GxRect(0, 0, width, height));
  }

  // blit to window
  final void blitRectWin (SimpleWindow w, int destx, int desty, GxRect srect) {
    if (w is null || handle is null || w.closed) return;
    XPutImage(w.impl.display, cast(Drawable)w.impl.window, w.impl.gc, handle, srect.x0, srect.y0, destx, desty, srect.width, srect.height);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public struct EPixmapImpl {
  Pixmap xpm;
  private int mWidth, mHeight;

  this (int wdt, int hgt) {
    //if (width < 1 || height < 1) throw new Exception("invalid pixmap dimensions");
    if (wdt < 1) wdt = 1;
    if (hgt < 1) hgt = 1;
    if (wdt > 1024) wdt = 1024;
    if (hgt > 1024) hgt = 1024;
    xpm = XCreatePixmap(glconCtlWindow.impl.display, cast(Drawable)glconCtlWindow.impl.window, wdt, hgt, 24);
    mWidth = wdt;
    mHeight = hgt;
  }

  this (ref XlibTCImage xtc) {
    if (!xtc.valid) throw new Exception("can't create pixmap from empty xlib image");
    int wdt = xtc.width;
    int hgt = xtc.height;
    if (wdt < 1) wdt = 1;
    if (hgt < 1) hgt = 1;
    if (wdt > 1024) wdt = 1024;
    if (hgt > 1024) hgt = 1024;
    xpm = XCreatePixmap(glconCtlWindow.impl.display, cast(Drawable)glconCtlWindow.impl.window, wdt, hgt, 24);
    // source x, source y
    XPutImage(glconCtlWindow.impl.display, cast(Drawable)xpm, glconCtlWindow.impl.gc, xtc.handle, 0, 0, 0, 0, wdt, hgt);
    mWidth = wdt;
    mHeight = hgt;
  }

  @disable this (this);

  ~this () {
    if (glconCtlWindow is null || glconCtlWindow.closed) { xpm = 0; return; }
    if (xpm) {
      XFreePixmap(glconCtlWindow.impl.display, xpm);
      xpm = 0;
    }
  }

  @property bool valid () const pure nothrow @trusted @nogc { pragma(inline, true); return (xpm != 0); }

  @property int width () const pure nothrow @trusted @nogc { pragma(inline, true); return mWidth; }
  @property int height () const pure nothrow @trusted @nogc { pragma(inline, true); return mHeight; }

  // blit to window buffer
  final void blitAt (SimpleWindow w, int x, int y) {
    blitRect(w, x, y, GxRect(0, 0, width, height));
  }

  // blit to window buffer
  final void blitRect (SimpleWindow w, int destx, int desty, GxRect srect) {
    if (w is null || !xpm || w.closed) return;
    XCopyArea(w.impl.display, cast(Drawable)xpm, cast(Drawable)w.impl.buffer, w.impl.gc, srect.x0, srect.y0, srect.width, srect.height, destx, desty);
  }
}


public alias EPixmap = KRC!EPixmapImpl;


// ////////////////////////////////////////////////////////////////////////// //
shared static this () {
  gxCreatePixmap1bpp = delegate (int wdt, int hgt, scope uint delegate (int x, int y) getPixel) {
    if (glconCtlWindow !is null && !glconCtlWindow.closed) {
      auto dpy = glconCtlWindow.impl.display;
      auto drw = cast(Drawable)cast(Drawable)glconCtlWindow.impl.window;
      Pixmap px = XCreatePixmap(dpy, drw, wdt, hgt, 24);
      // alas, painter can set clip mask, and we have no way to save and restore it, so...
      GC gc = XCreateGC(dpy, drw, 0, null);
      scope(exit) XFreeGC(dpy, gc);
      XCopyGC(dpy, DefaultGC(dpy, DefaultScreen(dpy)), 0xffffffff, gc);
      XSetClipMask(dpy, gc, None);
      /+
      foreach (immutable int dy; 0..hgt) {
        foreach (immutable int dx; 0..wdt) {
          auto c = (getPixel(dx, dy) ? ~0 : 0);
          XSetForeground(dpy, gc, c);
          XDrawPoint(dpy, cast(Drawable)px, gc, dx, dy);
        }
      }
      +/
      import core.stdc.stdlib : malloc;
      ubyte* rawData = cast(ubyte*)malloc(wdt*hgt*4);
      rawData[0..wdt*hgt*4] = 0;
      uint* rd = cast(uint*)rawData;
      foreach (immutable int y; 0..hgt) {
        foreach (immutable int x; 0..wdt) {
          auto c = (getPixel(x, y) ? ~0 : 0);
          rd[y*wdt+x] = c;
        }
      }
      import arsd.simpledisplay : ImageFormat;
      auto handle = XCreateImage(dpy, DefaultVisual(dpy, DefaultScreen(dpy)), 24/*bpp*/, ImageFormat.ZPixmap, 0/*offset*/, rawData, wdt, hgt, 8/*FIXME*/, 4*wdt); // padding, bytes per line
      scope(exit) XDestroyImage(handle);
      XPutImage(dpy, cast(Drawable)px, gc, handle, 0, 0, 0, 0, wdt, hgt);
      return px;
    } else {
      return 0;
    }
  };
  gxDeletePixmap = delegate (Pixmap px) {
    if (glconCtlWindow is null || glconCtlWindow.closed) return;
    XFreePixmap(glconCtlWindow.impl.display, px);
  };
}


// ////////////////////////////////////////////////////////////////////////// //
public class EWindow {
protected:
  SimpleWindow swin;
  int mWidth, mHeight;
  bool mActive;
  EWidget[] widgets;
  int mActiveWidget = -1;

public:
  this (int awidth, int aheight) {
    if (awidth < 1) awidth = 1;
    if (aheight < 1) aheight = 1;
    mWidth = awidth;
    mHeight = aheight;
  }

  final @property int width () const pure nothrow @safe @nogc { pragma(inline, true); return mWidth; }
  final @property int height () const pure nothrow @safe @nogc { pragma(inline, true); return mHeight; }

  void setSize (int awdt, int ahgt) {
    if (awdt < 1) awdt = 1;
    if (ahgt < 1) ahgt = 1;
    mWidth = awdt;
    mHeight = ahgt;
  }

  EWidget addWidget (EWidget w) {
    if (w is null) return null;
    if (w.parent !is null) throw new Exception("widget already owned");
    widgets ~= w;
    w.parent = this;
    return w;
  }

  void defocused () {
    if (mActive) {
      activeWidget = null;
      mActive = false;
      glconPostScreenRepaint();
    }
  }

  void focused () {
    if (!active) {
      mActive = true;
      glconPostScreenRepaint();
    }
  }

  final void focusChanged (bool focused) { if (focused) this.focused(); else this.defocused(); }

  final @property bool active () const pure nothrow @safe @nogc { pragma(inline, true); return mActive; }
  final @property void active (bool v) { if (mActive == v) return; if (v) focused(); else defocused(); }

  final @property EWidget activeWidget () pure nothrow @safe @nogc { pragma(inline, true); return (mActiveWidget >= 0 && mActiveWidget < widgets.length ? widgets[mActiveWidget] : null); }

  final @property void activeWidget (EWidget w) {
    EWidget oaw = (mActiveWidget >= 0 && mActiveWidget < widgets.length ? widgets[mActiveWidget] : null);
    if (w is null || w.parent !is this) {
      mActiveWidget = -1;
      if (oaw !is null) oaw.onDeactivate();
      return;
    }
    foreach (immutable idx, EWidget ww; widgets) {
      if (ww is w) {
        if (mActiveWidget == idx) return;
        mActiveWidget = cast(int)idx;
        if (oaw !is null) oaw.onDeactivate();
        ww.onActivate();
        return;
      }
    }
    mActiveWidget = -1;
    if (oaw !is null) oaw.onDeactivate();
  }

  final EWidget widgetAt (int x, int y) pure nothrow @safe @nogc {
    foreach_reverse (EWidget w; widgets) {
      if (w.rc.inside(x, y)) return w;
    }
    return null;
  }

  protected void paintWidgets () {
    foreach (EWidget w; widgets) w.onPaint();
  }

  protected void paintBackground () {}
  protected void paintFinished () {}

  void onPaint () {
    paintBackground();
    paintWidgets();
    paintFinished();
  }

  bool onKeyPre (KeyEvent event) { return false; }
  bool onKeyPost (KeyEvent event) { return false; }

  bool onKey (KeyEvent event) {
    if (onKeyPre(event)) return true;
    if (auto aw = activeWidget) {
      if (aw.onKey(event)) return true;
    }
    if (onKeyPost(event)) return true;
    return false;
  }

  bool onCharPre (dchar ch) { return false; }
  bool onCharPost (dchar ch) { return false; }

  bool onChar (dchar ch) {
    if (onCharPre(ch)) return true;
    if (auto aw = activeWidget) {
      if (aw.onChar(ch)) return true;
    }
    if (onCharPost(ch)) return true;
    return false;
  }

  bool onMousePre (MouseEvent event) { return false; }
  bool onMousePost (MouseEvent event) { return false; }

  bool onMouse (MouseEvent event) {
    bool wasAwOrAct = false;
    if (onMousePre(event)) return true;
    auto aw = activeWidget;
    if (aw !is null) {
      if (aw.onMouse(event)) return true;
      wasAwOrAct = true;
    }
    if (auto ww = widgetAt(event.x, event.y)) {
      if (ww !is aw) {
        if (ww.onMouse(event)) return true;
        wasAwOrAct = true;
      }
    }
    if (onMousePost(event)) return true;
    if (wasAwOrAct && event.type != MouseEventType.motion) return true;
    return false;
  }

  final void drawFillRect (int x0, int y0, int wdt, int hgt, uint clr) { drawFillRect(GxRect(x0, y0, wdt, hgt), clr); }

  final void drawFillRect() (in auto ref GxRect rc, uint clr) {
    if (rc.empty) return;
    XSetForeground(swin.impl.display, swin.impl.gc, clr);
    XFillRectangle(swin.impl.display, cast(Drawable)swin.impl.buffer, swin.impl.gc, rc.x0, rc.y0, rc.width+1, rc.height+1);
  }

  final void setClipRect (int x0, int y0, int wdt, int hgt) {
    if (wdt < 0) wdt = 0; else ++wdt;
    if (hgt < 0) hgt = 0; else ++hgt;
    auto cr = XRectangle(cast(short)x0, cast(short)y0, cast(short)wdt, cast(short)hgt);
    XSetClipRectangles(swin.impl.display, swin.impl.gc, 0, 0, &cr, 1, 3/*YXBanded*/);
  }

  final void setClipRect() (in auto ref GxRect rc) { setClipRect(rc.x0, rc.y0, rc.width, rc.height); }

  final void resetClip () { XSetClipMask(swin.impl.display, swin.impl.gc, 0/*None*/); }
}


// ////////////////////////////////////////////////////////////////////////// //
public class EWidget {
  EWindow parent;
  GxRect rc;

  this (GxRect arc) {
    rc = arc;
  }

  final @property SimpleWindow swin () { pragma(inline, true); return parent.swin; }

  final @property bool active () nothrow @trusted @nogc { pragma(inline, true); return (parent !is null && parent.activeWidget is this); }
  final @property void active (bool v) { pragma(inline, true); if (parent !is null) parent.activeWidget = (v ? this : null); }

  void onPaint () {}

  void onActivate () {} // parent.activeWidget is this
  void onDeactivate () {} // parent.activeWidget already changed

  bool onKey (KeyEvent event) { return false; }
  bool onChar (dchar ch) { return false; }
  bool onMouse (MouseEvent event) { return false; }

  final void drawFillRect (int x0, int y0, int wdt, int hgt, uint clr) { parent.drawFillRect(x0, y0, wdt, hgt, clr); }
  final void drawFillRect() (in auto ref GxRect rc, uint clr) { parent.drawFillRect(rc, clr); }
  final void setClipRect (int x0, int y0, int wdt, int hgt) { parent.setClipRect(x0, y0, wdt, hgt); }
  final void setClipRect() (in auto ref GxRect rc) { parent.setClipRect(rc); }
  final void resetClip () { parent.resetClip(); }
}


// ////////////////////////////////////////////////////////////////////////// //
/*
shared static this () {
  import core.stdc.stdlib : malloc;
  vglTexBuf = cast(uint*)malloc((VBufWidth*VBufHeight+4)*4);
  if (vglTexBuf is null) assert(0, "out of memory!");
  vglTexBuf[0..VBufWidth*VBufHeight] = 0;
}
*/


// ////////////////////////////////////////////////////////////////////////// //
__gshared EgfxWindow mainwin;

public void egfxSetMainWindow (EgfxWindow win) {
  /*
  if (win !is null) {
    if (mainwin is win) return;
    static if (is(typeof(&mainwin.closeQuery))) {
      if (mainwin !is null) mainwin.closeQuery = null;
    }
    mainwin = win;
    static if (is(typeof(&mainwin.closeQuery))) {
      mainwin.closeQuery = delegate () { concmd("quit"); glconPostDoConCommands(); };
    }
    glconBackBuffer = win.backbuf;
  } else {
    glconBackBuffer = null;
  }
  */
}


// ////////////////////////////////////////////////////////////////////////// //
public class EgfxWindow : SimpleWindow {
  EWindow ampw;

  // minsize will be taken from aampw
  // if resizestep is zero, size on that dimension is fixed
  this (EWindow aampw, string winclass, string title, int resizeXStep=0, int resizeYStep=0) {
    if (aampw is null) assert(0, "wtf?! no EWindow!");
    if (winclass.length) sdpyWindowClass = winclass;
    ampw = aampw;
    ampw.swin = this;
    int minw = aampw.width;
    int maxw = aampw.width;
    int minh = aampw.height;
    int maxh = aampw.height;
    setupHandlers();
    super(minw, minh, title, OpenGlOptions.no, Resizability.allowResizing, WindowTypes.undecorated, WindowFlags.normal|WindowFlags.dontAutoShow);
    XSetWindowBackground(impl.display, impl.window, 0);
    {
      int acount = 0;
      Atom[16] atoms;
      atoms[acount++] = GetAtom!("_NET_WM_ACTION_MOVE", true)(impl.display);
      if (resizeXStep > 0 || resizeYStep > 0) atoms[acount++] = GetAtom!("_NET_WM_ACTION_RESIZE", true)(impl.display);
      atoms[acount++] = GetAtom!("_NET_WM_ACTION_CLOSE", true)(impl.display);
      atoms[acount++] = GetAtom!("_NET_WM_ACTION_CHANGE_DESKTOP", true)(impl.display);
      atoms[acount++] = GetAtom!("_NET_WM_ACTION_ABOVE", true)(impl.display);
      atoms[acount++] = GetAtom!("_NET_WM_ACTION_BELOW", true)(impl.display);
      atoms[acount++] = GetAtom!("_NET_WM_ACTION_STICK")(impl.display);
      XChangeProperty(
        impl.display,
        impl.window,
        GetAtom!("_NET_WM_ALLOWED_ACTIONS", true)(impl.display),
        XA_ATOM,
        32 /* bits */,
        0 /*PropModeReplace*/,
        atoms.ptr,
        acount);
    }

    if (resizeXStep > 0) maxw = 4096;
    if (resizeYStep > 0) maxh = 4096;
    setMinSize(minw, minh);
    setMaxSize(maxw, maxh);
    if (resizeXStep > 0 || resizeYStep > 0) {
      if (resizeXStep <= 0) resizeXStep = 1;
      if (resizeYStep <= 0) resizeYStep = 1;
      setResizeGranularity(resizeXStep, resizeYStep);
    }

    //show();
  }

  void delegate () onDismiss;
  void delegate () onSetup;

  override void close () {
    if (!closed && !hidden && onDismiss !is null) onDismiss();
    super.close();
  }

  override void hide () {
    if (closed || hidden) return;
    if (onDismiss !is null) onDismiss();
    super.hide();
  }

  final void hideInternal () {
    auto ood = onDismiss;
    onDismiss = null;
    scope(exit) onDismiss = ood;
    hide();
  }

  override void show () {
    if (closed || !hidden) return;
    super.show();
    if (onSetup !is null) onSetup();
  }

  void redraw () {
    if (closed) return;
    ampw.onPaint();
    if (mainwin is this) glconDraw();
    /*
    if (backbuf.usingXshm) {
      XShmPutImage(impl.display, cast(Drawable)impl.window, impl.gc, backbuf.handle, 0, 0, 0, 0, backbuf.width, backbuf.height, false);
    } else {
      XPutImage(impl.display, cast(Drawable)impl.window, impl.gc, backbuf.handle, 0, 0, 0, 0, backbuf.width, backbuf.height);
    }
    */
    XCopyArea(impl.display, cast(Drawable)impl.buffer, cast(Drawable)impl.window, impl.gc, 0, 0, width, height, 0, 0);
  }

  protected void setupHandlers () {
    handleKeyEvent = delegate (KeyEvent event) {
      scope(exit) if (!conQueueEmpty()) glconPostDoConCommands();
      if (mainwin is this && glconKeyEvent(event)) { glconPostScreenRepaint(); return; }
      if (isQuitRequested) { close(); return; }
      if ((event.modifierState&ModifierState.numLock) == 0) {
        switch (event.key) {
          case Key.Pad0: event.key = Key.Insert; break;
          case Key.Pad1: event.key = Key.End; break;
          case Key.Pad2: event.key = Key.Down; break;
          case Key.Pad3: event.key = Key.PageDown; break;
          case Key.Pad4: event.key = Key.Left; break;
          //case Key.Pad5: event.key = Key.Insert; break;
          case Key.Pad6: event.key = Key.Right; break;
          case Key.Pad7: event.key = Key.Home; break;
          case Key.Pad8: event.key = Key.Up; break;
          case Key.Pad9: event.key = Key.PageUp; break;
          case Key.PadEnter: event.key = Key.Enter; break;
          case Key.PadDot: event.key = Key.Delete; break;
          default: break;
        }
      } else {
        if (event.key == Key.PadEnter) event.key = Key.Enter;
      }
      ampw.onKey(event);
      glconPostScreenRepaint();
    };

    handleMouseEvent = delegate (MouseEvent event) {
      scope(exit) if (!conQueueEmpty()) glconPostDoConCommands();
      if (isQuitRequested) { close(); return; }
      ampw.onMouse(event);
      glconPostScreenRepaint();
    };

    handleCharEvent = delegate (dchar ch) {
      scope(exit) if (!conQueueEmpty()) glconPostDoConCommands();
      if (mainwin is this && glconCharEvent(ch)) { glconPostScreenRepaint(); return; }
      if (isQuitRequested) { close(); return; }
      ampw.onChar(ch);
      glconPostScreenRepaint();
    };

    windowResized = delegate (int wdt, int hgt) {
      scope(exit) if (!conQueueEmpty()) glconPostDoConCommands();
      if (isQuitRequested) { close(); return; }
      if (wdt < 1) wdt = 1;
      if (hgt < 1) hgt = 1;
      if (mainwin is this) glconResize(wdt, hgt);
      ampw.mWidth = wdt;
      ampw.mHeight = hgt;
      redraw();
      //glconPostScreenRepaint();
    };

    onFocusChange = delegate (bool focused) {
      ampw.active = focused;
    };

    handleExpose = delegate (int x, int y, int wdt, int hgt, int eventsLeft) {
      if (eventsLeft == 0) redraw();
      return true; // so sdpy will not draw backbuffer
    };
  }
}
