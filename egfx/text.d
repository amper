/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module egfx.text;
private:

import arsd.simpledisplay;

import iv.alice;
import iv.bclamp;
import iv.cmdcon;
import iv.fontconfig;
import iv.freetype;
import iv.utfutil;
import iv.vfs;

import egfx.base;


// ////////////////////////////////////////////////////////////////////////// //
// x and y are always in range; !0 is set
public __gshared Pixmap delegate (int wdt, int hgt, scope uint delegate (int x, int y) getPixel) gxCreatePixmap1bpp;
public __gshared void delegate (Pixmap px) gxDeletePixmap;


// ////////////////////////////////////////////////////////////////////////// //
enum {
  GXclear        = 0x0, /* 0 */
  GXand          = 0x1, /* src AND dst */
  GXandReverse   = 0x2, /* src AND NOT dst */
  GXcopy         = 0x3, /* src */
  GXandInverted  = 0x4, /* NOT src AND dst */
  GXnoop         = 0x5, /* dst */
  GXxor          = 0x6, /* src XOR dst */
  GXor           = 0x7, /* src OR dst */
  GXnor          = 0x8, /* NOT src AND NOT dst */
  GXequiv        = 0x9, /* NOT src XOR dst */
  GXinvert       = 0xa, /* NOT dst */
  GXorReverse    = 0xb, /* src OR NOT dst */
  GXcopyInverted = 0xc, /* NOT src */
  GXorInverted   = 0xd, /* NOT src OR dst */
  GXnand         = 0xe, /* NOT src OR NOT dst */
  GXset          = 0xf, /* 1 */
}


// ////////////////////////////////////////////////////////////////////////// //
struct GlyphCache {
  int gidx;
  Pixmap px;
  GxRect dims; // x0,y0: image left and top; width,height: real image size; in pixels
  int advance; // in pixels
  GlyphCache* prev;
  GlyphCache* next;

  @disable this (this);

  @property bool valid () const pure nothrow @trusted @nogc { pragma(inline, true); return (px != 0); }

  void clear () {
    if (px) {
      gxDeletePixmap(px);
      px = 0;
      dims = GxRect.init;
      advance = 0;
    }
  }
}

// list is sorted by access time; head has the latest access time
__gshared GlyphCache* head;
__gshared GlyphCache* tail;
__gshared GlyphCache*[int] gcache; // by glyph index
enum GlyphCacheMaxGlyphs = 256;


//TODO: use XImage to transfer bits
GlyphCache* wantGlyph (int gidx, FT_BitmapGlyph fgi) {
  GlyphCache* gcp;
  auto gcpp = gidx in gcache;

  if (gcpp !is null) {
    gcp = *gcpp;
    // move to list head
    if (gcp.prev !is null) {
      gcp.prev.next = gcp.next;
      if (gcp.next !is null) {
        gcp.next.prev = gcp.prev;
      } else {
        tail = gcp.prev;
      }
      // add as first
      gcp.prev = null;
      gcp.next = head;
      head = gcp;
    }
  } else {
    FT_Bitmap* bitmap = &fgi.bitmap;
    if (bitmap.pixel_mode != FT_PIXEL_MODE_MONO) return null; // alas
    if (bitmap.rows < 1 || bitmap.width < 1) return null; // nothing to do
    // want new glyph
    if (gcache.length == GlyphCacheMaxGlyphs) {
      // delete last glyph, reuse it's struct
      gcp = tail;
      tail = gcp.prev;
      tail.next = null;
      gcache.remove(gcp.gidx);
      gcp.clear();
    } else {
      // create new glyph
      gcp = new GlyphCache();
    }

    gcp.gidx = gidx;

    // draw pixels
    bool topdown = true;
    const(ubyte)* src = bitmap.buffer;
    int spt = bitmap.pitch;
    if (spt < 0) {
      topdown = false;
      spt = -spt;
      //src += spt*(bitmap.rows-1);
    }

    gcp.px = gxCreatePixmap1bpp(bitmap.width, bitmap.rows,
      (int x, int y) {
        if (!topdown) y = bitmap.rows-y-1;
        return src[y*spt+x/8]&(0x80>>(x%8));
      },
    );

    gcp.dims.x0 = fgi.left;
    gcp.dims.y0 = fgi.top;
    gcp.dims.width = bitmap.width;
    gcp.dims.height = bitmap.rows;
    gcp.advance = cast(int)(fgi.root.advance.x>>16);

    // add as first
    gcp.prev = null;
    gcp.next = head;
    head = gcp;
    gcache[gidx] = gcp;
  }
  return gcp;
}


// ////////////////////////////////////////////////////////////////////////// //
//__gshared string chiFontName = "Arial:pixelsize=16";
//__gshared string chiFontName = "Verdana:pixelsize=16";
__gshared string chiFontName = "Verdana:pixelsize=12";
__gshared string chiFontFile;
__gshared int fontSize;
__gshared int fontHeight;
__gshared int fontBaselineOfs;


// ////////////////////////////////////////////////////////////////////////// //
__gshared ubyte* ttfontdata;
__gshared uint ttfontdatasize;

__gshared FT_Library ttflibrary;
__gshared FTC_Manager ttfcache;
__gshared FTC_CMapCache ttfcachecmap;
__gshared FTC_ImageCache ttfcacheimage;


shared static ~this () {
  if (ttflibrary) {
    if (ttfcache) {
      FTC_Manager_Done(ttfcache);
      ttfcache = null;
    }
    FT_Done_FreeType(ttflibrary);
    ttflibrary = null;
  }
}


enum FontID = cast(FTC_FaceID)1;


extern(C) nothrow {
  void ttfFontFinalizer (void* obj) {
    import core.stdc.stdlib : free;
    if (obj is null) return;
    auto tf = cast(FT_Face)obj;
    if (tf.generic.data !is ttfontdata) return;
    if (ttfontdata !is null) {
      //conwriteln("TTF CACHE: freeing loaded font...");
      free(ttfontdata);
      ttfontdata = null;
      ttfontdatasize = 0;
    }
  }

  FT_Error ttfFontLoader (FTC_FaceID face_id, FT_Library library, FT_Pointer request_data, FT_Face* aface) {
    if (face_id == FontID) {
      try {
        if (ttfontdata is null) {
          //conwriteln("TTF CACHE: loading '", chiFontFile, "'...");
          import core.stdc.stdlib : malloc;
          auto fl = VFile(chiFontFile);
          auto fsz = fl.size;
          if (fsz < 16 || fsz > int.max/8) throw new Exception("invalid ttf size");
          ttfontdatasize = cast(uint)fsz;
          ttfontdata = cast(ubyte*)malloc(ttfontdatasize);
          if (ttfontdata is null) assert(0, "out of memory");
          fl.rawReadExact(ttfontdata[0..ttfontdatasize]);
        }
        auto res = FT_New_Memory_Face(library, cast(const(FT_Byte)*)ttfontdata, ttfontdatasize, 0, aface);
        if (res != 0) throw new Exception("error loading ttf: '"~chiFontFile~"'");
        (*aface).generic.data = ttfontdata;
        (*aface).generic.finalizer = &ttfFontFinalizer;
      } catch (Exception e) {
        if (ttfontdata !is null) {
          import core.stdc.stdlib : free;
          free(ttfontdata);
          ttfontdata = null;
          ttfontdatasize = 0;
        }
        conwriteln("ERROR loading font: ", e.msg);
        return FT_Err_Cannot_Open_Resource;
      }
      return FT_Err_Ok;
    } else {
      conwriteln("TTF CACHE: invalid font id");
    }
    return FT_Err_Cannot_Open_Resource;
  }
}


void ttfLoad () nothrow {
  if (FT_Init_FreeType(&ttflibrary)) assert(0, "can't initialize FreeType");
  if (FTC_Manager_New(ttflibrary, 0, 0, 0, &ttfFontLoader, null, &ttfcache)) assert(0, "can't initialize FreeType cache manager");
  if (FTC_CMapCache_New(ttfcache, &ttfcachecmap)) assert(0, "can't initialize FreeType cache manager");
  if (FTC_ImageCache_New(ttfcache, &ttfcacheimage)) assert(0, "can't initialize FreeType cache manager");
  {
    FTC_ScalerRec fsc;
    fsc.face_id = FontID;
    fsc.width = 0;
    fsc.height = fontSize;
    fsc.pixel = 1; // size in pixels

    FT_Size ttfontsz;
    if (FTC_Manager_LookupSize(ttfcache, &fsc, &ttfontsz)) assert(0, "cannot find FreeType font");
    fontHeight = cast(int)(ttfontsz.metrics.height>>6); // 26.6
    fontBaselineOfs = cast(int)((ttfontsz.metrics.height+ttfontsz.metrics.descender)>>6);
    if (fontHeight < 2 || fontHeight > 128) assert(0, "invalid FreeType font metrics");
  }
  //conwriteln("TTF CACHE initialized.");
}


void initFontEngine () nothrow {
  if (ttflibrary is null) {
    import std.string : fromStringz, toStringz;
    if (!FcInit()) assert(0, "cannot init fontconfig");
    FcPattern* pat = FcNameParse(chiFontName.toStringz);
    if (pat is null) assert(0, "cannot parse font name");
    if (!FcConfigSubstitute(null, pat, FcMatchPattern)) assert(0, "cannot find fontconfig substitute");
    FcDefaultSubstitute(pat);
    // find the font
    FcResult result;
    FcPattern* font = FcFontMatch(null, pat, &result);
    if (font !is null) {
      char* file = null;
      if (FcPatternGetString(font, FC_FILE, 0, &file) == FcResultMatch) {
        //conwriteln("font file: [", file, "]");
        chiFontFile = file.fromStringz.idup;
      }
      double pixelsize;
      if (FcPatternGetDouble(font, FC_PIXEL_SIZE, 0, &pixelsize) == FcResultMatch) {
        //conwriteln("pixel size: ", pixelsize);
        fontSize = cast(int)pixelsize;
      }
    }
    FcPatternDestroy(pat);
    // arbitrary limits
    if (fontSize < 6) fontSize = 6;
    if (fontSize > 42) fontSize = 42;
    ttfLoad();
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public void utfByDChar (const(char)[] s, scope void delegate (dchar ch) nothrow @trusted dg) nothrow @trusted {
  if (dg is null) return;
  Utf8DecoderFast dc;
  foreach (char ch; s) {
    if (dc.decode(cast(ubyte)ch)) dg(dc.complete ? dc.codepoint : dc.replacement);
  }
}


public void utfByDCharSPos (const(char)[] s, scope void delegate (dchar ch, usize stpos) nothrow @trusted dg) nothrow @trusted {
  if (dg is null) return;
  Utf8DecoderFast dc;
  usize stpos = 0;
  foreach (immutable idx, char ch; s) {
    if (dc.decode(cast(ubyte)ch)) {
      dg(dc.complete ? dc.codepoint : dc.replacement, stpos);
      stpos = idx+1;
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
/+
void drawFTBitmap (int x, int y, in ref FT_Bitmap bitmap, uint clr) nothrow @trusted @nogc {
  if (bitmap.pixel_mode != FT_PIXEL_MODE_MONO) return; // alas
  if (bitmap.rows < 1 || bitmap.width < 1) return; // nothing to do
  if (gxIsTransparent(clr)) return; // just in case
  // prepare
  bool topdown = true;
  const(ubyte)* src = bitmap.buffer;
  int spt = bitmap.pitch;
  if (spt < 0) {
    topdown = false;
    spt = -spt;
    src += spt*(bitmap.rows-1);
  }
  if (!gxIsSolid(clr)) {
    // let this be slow
    foreach (immutable int dy; 0..bitmap.rows) {
      ubyte count = 0, b = 0;
      auto ss = src;
      foreach (immutable int dx; 0..bitmap.width) {
        if (count-- == 0) { count = 7; b = *ss++; } else b <<= 1;
        if (b&0x80) gxPutPixel(x+dx, y, clr);
      }
      ++y;
      if (topdown) src += spt; else src -= spt;
    }
    return;
  }
  // check if we can use fastest path
  auto brc = GxRect(x, y, bitmap.width, bitmap.rows);
  if (gxClipRect.contains(brc) && GxRect(0, 0, VBufWidth, VBufHeight).contains(brc)) {
    // yay, the fastest one!
    uint* dptr = vglTexBuf+y*VBufWidth+x;
    foreach (immutable int dy; 0..bitmap.rows) {
      ubyte count = 0, b = 0;
      auto ss = src;
      auto curptr = dptr;
      foreach (immutable int dx; 0..bitmap.width) {
        if (count-- == 0) { count = 7; b = *ss++; } else b <<= 1;
        if (b&0x80) *curptr = clr;
        ++curptr;
      }
      if (topdown) src += spt; else src -= spt;
      dptr += VBufWidth;
    }
  } else {
    // do it slow
    foreach (immutable int dy; 0..bitmap.rows) {
      ubyte count = 0, b = 0;
      auto ss = src;
      foreach (immutable int dx; 0..bitmap.width) {
        if (count-- == 0) { count = 7; b = *ss++; } else b <<= 1;
        if (b&0x80) gxSetPixel(x+dx, y, clr);
      }
      ++y;
      if (topdown) src += spt; else src -= spt;
    }
  }
}
+/


// y is baseline; returns advance
int ttfDrawGlyph (Display* dpy, Drawable d, GC gc, bool firstchar, int x, int y, int glyphidx, uint clr) {
  enum mono = true;
  if (glyphidx == 0) return 0;

  FTC_ImageTypeRec fimg;
  fimg.face_id = FontID;
  fimg.width = 0;
  fimg.height = fontSize;
  static if (mono) {
    fimg.flags = FT_LOAD_TARGET_MONO|(gxIsTransparent(clr) ? 0 : FT_LOAD_MONOCHROME|FT_LOAD_RENDER);
  } else {
    fimg.flags = (gxIsTransparent(clr) ? 0 : FT_LOAD_RENDER);
  }

  FT_Glyph fg;
  if (FTC_ImageCache_Lookup(ttfcacheimage, &fimg, glyphidx, &fg, null)) return 0;

  int advdec = 0;
  if (!gxIsTransparent(clr)) {
    if (fg.format != FT_GLYPH_FORMAT_BITMAP) return 0;
    FT_BitmapGlyph fgi = cast(FT_BitmapGlyph)fg;
    int x0 = x+fgi.left;
    /*
    if (firstchar && fgi.bitmap.width > 0) { x0 -= fgi.left; advdec = fgi.left; }
    drawFTBitmap(x0, y-fgi.top, fgi.bitmap, clr);
    */
    auto glp = wantGlyph(glyphidx, fgi);
    if (glp !is null) {
      // erase the parts not covered by the mask
      XSetFunction(dpy, gc, GXand);
      XSetForeground(dpy, gc, 0);
      XSetBackground(dpy, gc, ~0);
      XCopyPlane(dpy, cast(Drawable)glp.px, d, gc, 0, 0, glp.dims.width, glp.dims.height, x0, y-fgi.top, 1);
      // draw pixels
      XSetFunction(dpy, gc, GXor);
      XSetForeground(dpy, gc, clr);
      XSetBackground(dpy, gc, 0);
      XCopyPlane(dpy, cast(Drawable)glp.px, d, gc, 0, 0, glp.dims.width, glp.dims.height, x0, y-fgi.top, 1);
      // done
      XSetFunction(dpy, gc, GXcopy);
    }
  }
  return cast(int)(fg.advance.x>>16)-advdec;
}


int ttfGetKerning (int gl0idx, int gl1idx) nothrow @trusted {
  if (gl0idx == 0 || gl1idx == 0) return 0;

  FTC_ScalerRec fsc;
  fsc.face_id = FontID;
  fsc.width = 0;
  fsc.height = fontSize;
  fsc.pixel = 1; // size in pixels

  FT_Size ttfontsz;
  if (FTC_Manager_LookupSize(ttfcache, &fsc, &ttfontsz)) return 0;
  if (!FT_HAS_KERNING(ttfontsz.face)) return 0;

  FT_Vector kk;
  if (FT_Get_Kerning(ttfontsz.face, gl0idx, gl1idx, FT_KERNING_UNSCALED, &kk)) return 0;
  if (!kk.x) return 0;
  auto kadvfrac = FT_MulFix(kk.x, ttfontsz.metrics.x_scale); // 1/64 of pixel
  return cast(int)((kadvfrac/*+(kadvfrac < 0 ? -32 : 32)*/)>>6);
}


// ////////////////////////////////////////////////////////////////////////// //
public int gxCharWidth (dchar ch) nothrow @trusted {
  initFontEngine();

  int glyph = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, ch);
  if (glyph == 0) glyph = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, '\u25A1');
  if (glyph == 0) return 0;

  FTC_ImageTypeRec fimg;
  fimg.face_id = FontID;
  fimg.width = 0;
  fimg.height = fontSize;
  fimg.flags = FT_LOAD_TARGET_MONO;

  FT_Glyph fg;
  if (FTC_ImageCache_Lookup(ttfcacheimage, &fimg, glyph, &fg, null)) return -666;

  int res = cast(int)(fg.advance.x>>16);
  return (res > 0 ? res : 0);
}


// ////////////////////////////////////////////////////////////////////////// //
// return char width
public int gxDrawChar (Display* dpy, Drawable d, GC gc, int x, int y, dchar ch, uint clr, int prevcp=-1) {
  int glyph = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, ch);
  if (glyph == 0) glyph = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, '\u25A1');
  if (glyph == 0) return 0;
  int kadv = ttfGetKerning((prevcp >= 0 ? FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, prevcp) : 0), glyph);
  return ttfDrawGlyph(dpy, d, gc, false, x+kadv, y+fontBaselineOfs, glyph, clr);
}


// ////////////////////////////////////////////////////////////////////////// //
public struct GxKerning {
  int prevgidx = 0;
  int wdt = 0;
  int lastadv = 0;
  int lastcw = 0;
  int tabsize = 0;
  bool firstchar = true;

nothrow @trusted:
  this (int atabsize, bool firstCharIsFull=false) {
    initFontEngine();
    firstchar = !firstCharIsFull;
    if ((tabsize = (atabsize > 0 ? atabsize : 0)) != 0) tabsize = tabsize*gxCharWidth(' ');
  }

  void reset (int atabsize) {
    initFontEngine();
    prevgidx = 0;
    wdt = 0;
    lastadv = 0;
    lastcw = 0;
    if ((tabsize = (atabsize > 0 ? atabsize : 0)) != 0) tabsize = tabsize*gxCharWidth(' ');
    firstchar = true;
  }

  // tab length for current position
  int tablength () { pragma(inline, true); return (tabsize > 0 ? (wdt/tabsize+1)*tabsize-wdt : 0); }

  int fixWidthPre (dchar ch) {
    immutable int prevgl = prevgidx;
    wdt += lastadv;
    lastadv = 0;
    lastcw = 0;
    prevgidx = 0;
    if (ch == '\t' && tabsize > 0) {
      // tab
      lastadv = lastcw = tablength;
      firstchar = false;
    } else {
      initFontEngine();
      int glyph = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, ch);
      if (glyph == 0) glyph = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, '\u25A1');
      if (glyph != 0) {
        wdt += ttfGetKerning(prevgl, glyph);

        FTC_ImageTypeRec fimg;
        fimg.face_id = FontID;
        fimg.width = 0;
        fimg.height = fontSize;
        version(none) {
          fimg.flags = FT_LOAD_TARGET_MONO;
        } else {
          fimg.flags = FT_LOAD_TARGET_MONO|FT_LOAD_MONOCHROME|FT_LOAD_RENDER;
        }

        FT_Glyph fg;
        version(none) {
          if (FTC_ImageCache_Lookup(ttfcacheimage, &fimg, glyph, &fg, null) == 0) {
            prevgidx = glyph;
            lastadv = fg.advance.x>>16;
          }
        } else {
          if (FTC_ImageCache_Lookup(ttfcacheimage, &fimg, glyph, &fg, null) == 0) {
            int advdec = 0;
            if (fg.format == FT_GLYPH_FORMAT_BITMAP) {
              FT_BitmapGlyph fgi = cast(FT_BitmapGlyph)fg;
              if (firstchar && fgi.bitmap.width > 0) {
                lastcw = fgi.bitmap.width;
                advdec = fgi.left;
                if (lastcw < 1) { advdec = 0; lastcw = cast(int)(fg.advance.x>>16); }
              } else {
                lastcw = fgi.left+fgi.bitmap.width;
                if (lastcw < 1) lastcw = cast(int)(fg.advance.x>>16);
              }
            }
            prevgidx = glyph;
            lastadv = cast(int)(fg.advance.x>>16)-advdec;
            firstchar = false;
          }
        }
      }
    }
    return wdt;
  }

  @property int finalWidth () const { pragma(inline, true); return wdt+/*lastadv*/lastcw; }

  // BUGGY!
  @property int nextCharOfs () const { pragma(inline, true); return wdt+lastadv; }

  @property int currOfs () const { pragma(inline, true); return wdt; }

  @property int nextOfsNoSpacing () const { pragma(inline, true); return wdt+lastcw; }
}


// ////////////////////////////////////////////////////////////////////////// //
public struct GxDrawTextOptions {
  int tabsize = 0;
  uint clr = gxTransparent;
  bool firstCharIsFull = false;

static pure nothrow @safe @nogc:
  auto Color (uint aclr) { pragma(inline, true); return GxDrawTextOptions(0, aclr, false); }
  auto Tab (int atabsize) { pragma(inline, true); return GxDrawTextOptions(atabsize, gxTransparent, false); }
  auto TabColor (int atabsize, uint aclr) { pragma(inline, true); return GxDrawTextOptions(atabsize, aclr, false); }
  auto TabColorFirstFull (int atabsize, uint aclr, bool fcf) { pragma(inline, true); return GxDrawTextOptions(atabsize, aclr, fcf); }
  auto ColorNFC (uint aclr) { pragma(inline, true); return GxDrawTextOptions(0, aclr, true); }
  auto TabColorNFC (int atabsize, uint aclr) { pragma(inline, true); return GxDrawTextOptions(atabsize, aclr, true); }
  // more ctors?
}

public struct GxDrawTextState {
  usize spos; // current codepoint starting position
  usize epos; // current codepoint ending position (exclusive; i.e. *after* codepoint)
  int curx; // current x (before drawing the glyph)
}


// delegate should return color
public int gxDrawTextUtf(R) (Display* dpy, Drawable d, GC gc, in auto ref GxDrawTextOptions opt, int x, int y, auto ref R srng, uint delegate (in ref GxDrawTextState state) nothrow @trusted clrdg=null)
if (Imp!"std.range.primitives".isInputRange!R && is(Imp!"std.range.primitives".ElementEncodingType!R == char))
{
  // rely on the assumption that font face won't be unloaded while we are in this function
  initFontEngine();

  GxDrawTextState state;

  y += fontBaselineOfs;

  immutable int tabpix = (opt.tabsize > 0 ? opt.tabsize*gxCharWidth(' ') : 0);

  FT_Size ttfontsz;

  int prevglyph = 0;
  immutable int stx = x;

  bool dokern = true;
  bool firstchar = !opt.firstCharIsFull;
  Utf8DecoderFast dc;

  while (!srng.empty) {
    immutable ubyte srbyte = cast(ubyte)srng.front;
    srng.popFront();
    ++state.epos;

    if (dc.decode(srbyte)) {
      int ch = (dc.complete ? dc.codepoint : dc.replacement);
      int pgl = prevglyph;
      prevglyph = 0;
      state.curx = x;

      if (opt.tabsize > 0) {
        if (ch == '\t') {
          firstchar = false;
          int wdt = x-stx;
          state.curx = x;
          x += (wdt/tabpix+1)*tabpix-wdt;
          if (clrdg !is null) clrdg(state);
          state.spos = state.epos;
          continue;
        }
      }

      int glyph = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, ch);
      if (glyph == 0) glyph = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, '\u25A1');
      if (glyph != 0) {
        // kerning
        int kadv = 0;
        if (pgl != 0 && dokern) {
          if (ttfontsz is null) {
            FTC_ScalerRec fsc;
            fsc.face_id = FontID;
            fsc.width = 0;
            fsc.height = fontSize;
            fsc.pixel = 1; // size in pixels
            if (FTC_Manager_LookupSize(ttfcache, &fsc, &ttfontsz)) {
              dokern = false;
              ttfontsz = null;
            } else {
              dokern = (FT_HAS_KERNING(ttfontsz.face) != 0);
            }
          }
          if (dokern) {
            FT_Vector kk;
            if (FT_Get_Kerning(ttfontsz.face, pgl, glyph, FT_KERNING_UNSCALED, &kk) == 0) {
              if (kk.x) {
                auto kadvfrac = FT_MulFix(kk.x, ttfontsz.metrics.x_scale); // 1/64 of pixel
                kadv = cast(int)((kadvfrac/*+(kadvfrac < 0 ? -32 : 32)*/)>>6);
              }
            }
          }
        }
        x += kadv;
        uint clr = opt.clr;
        if (clrdg !is null) { state.curx = x; clr = clrdg(state); }
        x += ttfDrawGlyph(dpy, d, gc, firstchar, x, y, glyph, clr);
        firstchar = false;
      }
      state.spos = state.epos;
      prevglyph = glyph;
    }
  }

  return x-stx;
}


public int gxDrawTextUtf() (Display* dpy, Drawable d, GC gc, in auto ref GxDrawTextOptions opt, int x, int y, const(char)[] s, uint delegate (in ref GxDrawTextState state) nothrow @trusted clrdg=null) {
  static struct StrIterator {
  private:
    const(char)[] str;
  public pure nothrow @trusted @nogc:
    this (const(char)[] s) { pragma(inline, true); str = s; }
    @property bool empty () const { pragma(inline, true); return (str.length == 0); }
    @property char front () const { pragma(inline, true); return (str.length ? str.ptr[0] : 0); }
    void popFront () { if (str.length) str = str[1..$]; }
  }

  return gxDrawTextUtf(dpy, d, gc, opt, x, y, StrIterator(s), clrdg);
}

public int gxTextHeightUtf () nothrow @trusted { initFontEngine(); return fontHeight; }

public int gxTextWidthUtf (const(char)[] s, int tabsize=0, bool firstCharIsFull=false) nothrow @trusted {
  auto kern = GxKerning(tabsize, firstCharIsFull);
  s.utfByDChar(delegate (dchar ch) @trusted { kern.fixWidthPre(ch); });
  return kern.finalWidth;
}


// ////////////////////////////////////////////////////////////////////////// //
public int gxDrawTextUtf() (Display* dpy, Drawable d, GC gc, int x, int y, const(char)[] s, uint clr) { return gxDrawTextUtf(dpy, d, gc, GxDrawTextOptions.Color(clr), x, y, s); }


// ////////////////////////////////////////////////////////////////////////// //
public int gxDrawTextUtf() (SimpleWindow w, int x, int y, const(char)[] s, uint clr) { return gxDrawTextUtf(w.impl.display, cast(Drawable)w.impl.buffer, w.impl.gc, GxDrawTextOptions.Color(clr), x, y, s); }
