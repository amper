module egfx.xshape;

import arsd.simpledisplay;


// ////////////////////////////////////////////////////////////////////////// //
public extern(C) nothrow @nogc {

import core.stdc.config : c_long, c_ulong;

struct XBoxRec {
  short x1, x2, y1, y2;
}
alias XBox = XBoxRec*;


// clip region
struct XRegionRec {
  c_long size;
  c_long numRects;
  XBox rects;
  XBoxRec extents;
}
alias XRegion = XRegionRec*;


enum ShapeSet = 0;
enum ShapeUnion = 1;
enum ShapeIntersect = 2;
enum ShapeSubtract = 3;
enum ShapeInvert = 4;

enum ShapeBounding = 0;
enum ShapeClip = 1;
enum ShapeInput = 2;

enum ShapeNotifyMask = 1U<<0;
enum ShapeNotify = 0;

enum ShapeNumberEvents = ShapeNotify+1;

struct XShapeEvent {
  int type;       /* of event */
  c_ulong serial;   /* # of last request processed by server */
  Bool send_event;      /* true if this came frome a SendEvent request */
  Display* display;     /* Display the event was read from */
  Window window;      /* window of event */
  int kind;       /* ShapeBounding or ShapeClip */
  int x, y;       /* extents of new region */
  uint width, height;
  Time time;        /* server timestamp when region changed */
  Bool shaped;      /* true if the region exists */
}


Bool XShapeQueryExtension (
  Display* display,
  int* event_base,
  int* error_base
);

Status XShapeQueryVersion (
  Display* display,
  int* major_version,
  int* minor_version
);

void XShapeCombineRegion (
  Display* display,
  Window dest,
  int dest_kind,
  int x_off,
  int y_off,
  XRegion region,
  int op
);

void XShapeCombineRectangles (
  Display* display,
  Window dest,
  int dest_kind,
  int x_off,
  int y_off,
  XRectangle* rectangles,
  int n_rects,
  int op,
  int ordering
);

void XShapeCombineMask (
  Display* display,
  Window dest,
  int dest_kind,
  int x_off,
  int y_off,
  Pixmap src,
  int op
);

void XShapeCombineShape (
  Display* display,
  Window dest,
  int dest_kind,
  int x_off,
  int y_off,
  Window src,
  int src_kind,
  int op
);

void XShapeOffsetShape (
  Display* display,
  Window dest,
  int dest_kind,
  int x_off,
  int y_off
);

Status XShapeQueryExtents (
  Display* display,
  Window window,
  Bool* bounding_shaped,
  int* x_bounding,
  int* y_bounding,
  uint* w_bounding,
  uint* h_bounding,
  Bool* clip_shaped,
  int* x_clip,
  int* y_clip,
  uint* w_clip,
  uint* h_clip
);

void XShapeSelectInput (
  Display* display,
  Window window,
  c_ulong mask
);

c_ulong XShapeInputSelected (
  Display* display,
  Window window
);

XRectangle *XShapeGetRectangles (
  Display* display,
  Window window,
  int kind,
  int* count,
  int* ordering
);

// ////////////////////////////////////////////////////////////////////////// //
//struct XRegionRec {}
//alias XRegion = XRegionRec*;

enum {
  EvenOddRule = 0,
  WindingRule = 1,
}


XRegion XCreateRegion ();
int XDestroyRegion (XRegion r);
XRegion XPolygonRegion (XPoint* points, int n, int fill_rule);
int XUnionRegion (XRegion sra, XRegion srb, XRegion dr_return);

}


// ////////////////////////////////////////////////////////////////////////// //
// <0: ooops
public int shapeEvent () {
  __gshared static int event = -666;
  if (event == -666) {
    int dummy;
    if (!XShapeQueryExtension(XDisplayConnection.get(), &event, &dummy)) event = -1;
  }
  return event;
}


/*
  {
    XRectangle[2] rects;
    rects[0] = XRectangle(0, 0, 60, 20);
    rects[1] = XRectangle(100, 100, 60, 20);
    XShapeCombineRectangles(win.impl.display, win.impl.window, ShapeBounding, 0, 0, rects.ptr, rects.length, ShapeIntersect, 0);
    //XShapeCombineRectangles(win.impl.display, win.impl.window, ShapeClip, 0, 0, rects.ptr, rects.length, ShapeIntersect, 0);
  }
*/
