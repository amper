/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module amperskin;

import core.time;

import arsd.color;
import arsd.image;
import arsd.simpledisplay;

import iv.alice;
import iv.cmdcon;
import iv.cmdcongl;
import iv.simplealsa;
import iv.strex;
import iv.utfutil;
import iv.vfs.io;

import aplayer;

import egfx;
import egfx.xshape;

import amperopts;


// ////////////////////////////////////////////////////////////////////////// //
class QuitEvent {}


void postQuitEvent () {
  if (glconCtlWindow is null) return;
  if (!glconCtlWindow.eventQueued!QuitEvent) glconCtlWindow.postEvent(new QuitEvent());
}


// ////////////////////////////////////////////////////////////////////////// //
version(default_skin_is_winamp) {
  static immutable ubyte[] BuiltinSkinData = cast(immutable(ubyte)[])import("skins/winamp2.zip");
} else {
  static immutable ubyte[] BuiltinSkinData = cast(immutable(ubyte)[])import("skins/pb2000.zip");
}


// ////////////////////////////////////////////////////////////////////////// //
struct AmperSkin {
  static struct ImgFile { string name; }
  @ImgFile("main") EPixmap imgMain;
  @ImgFile("cbuttons") EPixmap imgCButtons;
  @ImgFile("shufrep") EPixmap imgShufRep;
  @ImgFile("numbers") EPixmap imgNums;
  @ImgFile("nums_ex") EPixmap imgNumsEx;
  @ImgFile("pledit") EPixmap imgPlaylist;
  @ImgFile("titlebar") EPixmap imgTitle;
  @ImgFile("volume") EPixmap imgVolume;
  @ImgFile("posbar") EPixmap imgPosBar;
  @ImgFile("text") EPixmap imgText;
  @ImgFile("monoster") EPixmap imgMonoStereo;
  @ImgFile("eqmain") EPixmap imgEq;
  uint plistColNormal = gxTransparent;
  uint plistColCurrent = gxTransparent;
  uint plistColNormalBG = gxTransparent;
  uint plistColCurrentBG = gxTransparent;
  XPoint[][] mainRegion;
  XPoint[][] eqRegion;
  uint[6] tickerColClear = gxTransparent;
  bool tickerColClearEasy;
  uint tickerColText = gxTransparent;

  // i can generate this with mixin, but meh...
  void updateWith (ref AmperSkin sk) {
    if (sk.imgMain.valid) { imgMain = sk.imgMain; mainRegion = sk.mainRegion; }
    if (sk.imgCButtons.valid) imgCButtons = sk.imgCButtons;
    if (sk.imgShufRep.valid) imgShufRep = sk.imgShufRep;
    if (sk.imgNums.valid) imgNums = sk.imgNums;
    if (sk.imgPlaylist.valid) imgPlaylist = sk.imgPlaylist;
    if (sk.imgTitle.valid) imgTitle = sk.imgTitle;
    if (sk.imgVolume.valid) imgVolume = sk.imgVolume;
    if (sk.imgPosBar.valid) imgPosBar = sk.imgPosBar;
    if (sk.imgText.valid) imgText = sk.imgText;
    if (sk.imgMonoStereo.valid) imgMonoStereo = sk.imgMonoStereo;
    if (sk.imgEq.valid) { imgEq = sk.imgEq; eqRegion = sk.eqRegion; }
    if (!sk.plistColNormal.gxIsTransparent) plistColNormal = sk.plistColNormal;
    if (!sk.plistColCurrent.gxIsTransparent) plistColCurrent = sk.plistColCurrent;
    if (!sk.plistColNormalBG.gxIsTransparent) plistColNormalBG = sk.plistColNormalBG;
    if (!sk.plistColCurrentBG.gxIsTransparent) plistColCurrentBG = sk.plistColCurrentBG;
    foreach (immutable idx, uint c; sk.tickerColClear[]) {
      if (!c.gxIsTransparent) tickerColClear[idx] = c;
    }
    if (!sk.tickerColText.gxIsTransparent) tickerColText = sk.tickerColText;
    tickerColClearEasy = true;
    foreach (uint c; tickerColClear[1..$]) if (c != tickerColClear[0]) { tickerColClearEasy = false; break; }
  }

  static void setWindowRegion (SimpleWindow w, XPoint[][] region) {
    if (w is null || w.closed) return;
    if (region.length == 0) {
      XRectangle[1] rects;
      rects[0] = XRectangle(0, 0, cast(short)ampMain.width, cast(short)ampMain.height);
      XShapeCombineRectangles(w.impl.display, w.impl.window, ShapeBounding, 0, 0, rects.ptr, cast(int)rects.length, ShapeSet, 0);
    } else {
      // create regions
      XRegion reg;
      foreach (XPoint[] poly; region) {
        auto r1 = XPolygonRegion(poly.ptr, cast(int)poly.length, WindingRule);
        if (reg is null) {
          reg = r1;
        } else {
          XRegion dr = XCreateRegion();
          XUnionRegion(reg, r1, dr);
          XDestroyRegion(reg);
          XDestroyRegion(r1);
          reg = dr;
        }
      }
      assert(reg !is null);
      XShapeCombineRegion(w.impl.display, w.impl.window, ShapeBounding, 0, 0, reg, ShapeSet);
      XDestroyRegion(reg);
    }
  }

  void setupRegions () {
    setWindowRegion(sdampwin, mainRegion);
    setWindowRegion(sdeqwin, eqRegion);
  }
}


__gshared AmperSkin skin;


public void setupSkinRegions () {
  .skin.setupRegions();
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared AmpMainWindow ampMain;
__gshared AmpPListWindow ampPList;
__gshared AmpEqWindow ampEq;


// ////////////////////////////////////////////////////////////////////////// //
void loadSkin(bool initial=false) (ConString skinfile) {
  static if (!initial) AmperSkin skin;

  static if (initial) {
    skin.plistColNormal = gxRGB!(0, 255, 0);
    skin.plistColCurrent = gxRGB!(255, 255, 255);
    skin.plistColNormalBG = gxRGB!(0, 0, 0);
    skin.plistColCurrentBG = gxRGB!(0, 0, 255);
    skin.tickerColClear[] = gxRGB!(0, 0, 0);
    skin.tickerColText = skin.plistColNormal;
  }

  static struct KeyValue {
    ConString key;
    ConString value;
  nothrow @trusted @nogc:
    this (ConString s) {
      s = s.xstrip;
      usize pos = 0;
      while (pos < s.length && s.ptr[pos] != '=' && s.ptr[pos] != ';') ++pos;
      key = s[0..pos].xstrip;
      if (pos < s.length && s.ptr[pos] == '=') {
        ++pos;
        //while (pos < s.length && s.ptr[pos] <= ' ') ++pos;
        auto stpos = pos;
        while (pos < s.length && s.ptr[pos] != ';') ++pos;
        value = s[stpos..pos].xstrip;
      }
    }

    uint asColor () const {
      auto val = value.xstrip;
      if (val.length != 4 && val.length != 7) return gxTransparent;
      int[3] rgb;
      if (val.length == 4) {
        foreach (immutable idx, char ch; val[1..$]) {
          int dg = digitInBase(ch, 16);
          if (dg < 0) return gxTransparent;
          dg = 255*dg/15;
          rgb[idx] = dg;
        }
      } else {
        foreach (immutable idx; 0..3) {
          int d0 = digitInBase(val[1+idx*2], 16);
          int d1 = digitInBase(val[2+idx*2], 16);
          if (d0 < 0 || d1 < 0) return gxTransparent;
          rgb[idx] = d0*16+d1;
        }
      }
      return gxrgb(rgb[0], rgb[1], rgb[2]);
    }
  }

  void parsePlaylistConfig (VFile fl) {
    bool inTextSection = false;
    foreach (/*auto*/ line; fl.byLine) {
      line = line.xstrip;
      if (line.length == 0 || line.ptr[0] == ';') continue;
      //conwriteln("<", line, ">");
      if (line[0] == '[') {
        inTextSection = line.strEquCI("[Text]");
        continue;
      }
      if (inTextSection) {
        auto kv = KeyValue(line);
        //conwritefln!"[%s]=<%s>"(kv.key, kv.value);
        auto clr = kv.asColor;
        if (clr.gxIsTransparent) continue;
        //conwritefln!"[%s]=#%06X"(kv.key, clr);
             if (kv.key.strEquCI("Normal")) skin.plistColNormal = clr;
        else if (kv.key.strEquCI("Current")) skin.plistColCurrent = clr;
        else if (kv.key.strEquCI("NormalBG")) skin.plistColNormalBG = clr;
        //else if (kv.key.strEquCI("CurrentBG")) skinPListColCurrentBG = clr;
        else if (kv.key.strEquCI("SelectedBG")) skin.plistColCurrentBG = clr;
      }
    }
  }

  static XPoint[][] parseRegionData (string ptcount, string pts) {
    static string removeDelimiters (string s) {
      while (s.length > 0 && (s.ptr[0] <= ' ' || s.ptr[0] == ',')) s = s[1..$];
      return (s.length ? s : null);
    }

    static int getToken (ref string s) {
      s = removeDelimiters(s);
      if (s.length == 0) throw new Exception("invalid integer");
      if (!s.ptr[0].isdigit) throw new Exception("invalid integer");
      int res = 0;
      while (s.length && s.ptr[0].isdigit) {
        res = res*10+s.ptr[0]-'0';
        s = s[1..$];
      }
      if (s.length > 0 && s.ptr[0] != ',' && s.ptr[0] > ' ') throw new Exception("invalid integer");
      return res;
    }

    XPoint[][] res;
    for (;;) {
      ptcount = removeDelimiters(ptcount);
      if (ptcount.length == 0) break;
      int count = getToken(ptcount);
      if (count < 3) throw new Exception("ooops. degenerate poly in region");
      XPoint[] pta;
      pta.reserve(count);
      foreach (immutable n; 0..count) {
        XPoint p;
        p.x = cast(short)getToken(pts);
        p.y = cast(short)getToken(pts);
        pta ~= p;
      }
      res ~= pta;
    }
    if (res is null) {
      res.reserve(1);
      res ~= [];
      res.length = 0;
      assert(res !is null);
    }
    return res;
  }

  void parseRegionConfig (VFile fl) {
    if (skin.mainRegion !is null && skin.eqRegion !is null) return;
    string cursection = null;
    string[string][string] sections;
    foreach (/*auto*/ line; fl.byLine) {
      line = line.xstrip;
      if (line.length == 0 || line.ptr[0] == ';') continue;
      if (line[0] == '[') {
        if (line.length < 3) { cursection = null; continue; }
        line = line[1..$-1];
        foreach (ref char ch; line) if (ch >= 'A' && ch <= 'Z') ch += 32;
        cursection = line.idup;
        continue;
      }
      if (cursection.length) {
        auto kv = KeyValue(line);
        if (kv.key.length == 0) continue;
        if (auto ssp = cursection in sections) {
          if (kv.key !in *ssp) (*ssp)[kv.key.idup] = kv.value.idup;
        } else {
          string[string] aa;
          aa[kv.key.idup] = kv.value.idup;
          sections[cursection] = aa;
        }
      }
    }

    void parseReg (ref XPoint[][] rg, string name) {
      if (rg is null) {
        if (auto ssp = name in sections) {
          string np, pts;
          foreach (const ref kv; (*ssp).byKeyValue) {
                 if (kv.key.strEquCI("NumPoints")) np = kv.value;
            else if (kv.key.strEquCI("PointList")) pts = kv.value;
          }
          if (np.length && pts.length) rg = parseRegionData(np, pts);
        }
      }
    }

    parseReg(skin.mainRegion, "normal");
    parseReg(skin.eqRegion, "equalizer");
  }

  static bool isGoodImageExtension (ConString fname) {
    return (fname.guessImageFormatFromExtension != ImageFileFormat.Unknown);
  }

  EPixmap loadImage(string skinname) (string fname) {
    try {
      auto fl = VFile(fname);
      MemoryImage ximg = loadImageFromFile(fl);
      scope(exit) delete ximg;
      if (ximg.width < 1 || ximg.height < 1 || ximg.width > 1024 || ximg.height > 1024) throw new Exception("invalid image");
      static if (skinname == "text") {
        // text fill color
        if (ximg.width >= 5) {
          foreach (immutable int dy, ref uint c; skin.tickerColClear[]) {
            Color cc = ximg.getPixel(4, dy%ximg.height);
            c = c2img(cc);
            // try to find out text color
            if (skin.tickerColText.gxIsTransparent) {
              foreach (immutable int dx; 0..4) {
                uint cx = c2img(ximg.getPixel(dx, dy%ximg.height));
                if (cx != c) { skin.tickerColText = cx; break; }
              }
            }
          }
        }
      }
      auto xtc = XlibTCImage(ximg);
      return EPixmap(xtc);
    } catch (Exception e) {
      conwriteln("ERROR loading skin image: '", fname, "'...");
      conwriteln(e.toString);
      throw e;
    }
  }

  bool pleditLoaded, regionLoaded;

  VFSDriverId vid;
  try {
    if (skinfile != "!BUILTIN!") {
      vid = vfsAddPak(skinfile, "skin:");
    } else {
      vid = vfsAddPak(wrapMemoryRO(BuiltinSkinData), "skin:");
    }
  } catch (Exception e) {
    conwriteln("ERROR loading archive '"~skinfile.idup~"'");
    throw e;
  }
  scope(exit) vfsRemovePak(vid);
  foreach (/*auto*/ de; vfsFileList) {
    auto fname = de.name;
    auto xpos = fname.lastIndexOf('/');
    if (xpos >= 0) fname = fname[xpos+1..$];
    else if (fname.startsWith("skin:")) fname = fname[5..$];
    xpos = fname.lastIndexOf('.');
    if (xpos <= 0) continue;
    auto iname = fname[0..xpos];
    //conwriteln("[", de.name, "]:[", fname, "]:[", iname, "]");

    if (!pleditLoaded && fname.strEquCI("pledit.txt")) {
      try {
        parsePlaylistConfig(VFile(de.name));
        pleditLoaded = true;
      } catch (Exception e) {
        conwriteln("ERROR loading 'pledit.txt'");
        conwriteln(e.toString);
        throw e;
      }
      continue;
    }

    if (!regionLoaded && fname.strEquCI("region.txt")) {
      try {
        parseRegionConfig(VFile(de.name));
        regionLoaded = true;
      } catch (Exception e) {
        conwriteln("ERROR loading 'pledit.txt'");
        conwriteln(e.toString);
        throw e;
      }
      continue;
    }

    if (!isGoodImageExtension(de.name)) continue;

    foreach (string memb; __traits(allMembers, AmperSkin)) {
      import std.traits : hasUDA, getUDAs;
      static if (hasUDA!(__traits(getMember, AmperSkin, memb), AmperSkin.ImgFile)) {
        enum SFN = getUDAs!(__traits(getMember, AmperSkin, memb), AmperSkin.ImgFile)[0].name;
        if (iname.strEquCI(SFN)) {
          if (!__traits(getMember, skin, memb).valid) __traits(getMember, skin, memb) = loadImage!SFN(de.name);
        }
      }
    }
  }

  if (skin.imgNumsEx.valid) skin.imgNums = skin.imgNumsEx;

  static if (!initial) {
    .skin.updateWith(skin);
  } else {
    foreach (string memb; __traits(allMembers, AmperSkin)) {
      import std.traits : hasUDA, getUDAs;
      static if (hasUDA!(__traits(getMember, AmperSkin, memb), AmperSkin.ImgFile)) {
        enum SFN = getUDAs!(__traits(getMember, AmperSkin, memb), AmperSkin.ImgFile)[0].name;
        static if (SFN != "nums_ex") {
          if (!__traits(getMember, skin, memb).valid) throw new Exception("skin image '"~SFN~"' not found");
        }
      }
    }
  }
  setupSkinRegions();
}


// ////////////////////////////////////////////////////////////////////////// //
class AmpWindow : EWindow {
  EPixmap* img;

  this (EPixmap* aimg) {
    if (aimg is null) throw new Exception("no image for window");
    img = aimg;
    super(img.width, img.height);
  }

  protected override void paintBackground () {
    img.blitAt(swin, 0, 0);
  }

  override bool onKeyPost (KeyEvent event) {
    if (event.pressed) {
      if (event == "C-Q") { postQuitEvent(); return true; }
      if (event == "C-P") { concmd("pl_visible toggle"); return true; }
      if (event == "C-E") { concmd("eq_visible toggle"); return true; }
      if (event == "C-S") { concmd("mode_shuffle toggle"); return true; }
      if (event == "C-R") { concmd("mode_repeat toggle"); return true; }
    }
    return false;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
class AmpWidgetButton : EWidget {
  EPixmap* img;
  GxRect imgrc;
  string cmd;

  void delegate () onAction;

  protected this (GxRect arc) {
    super(arc);
  }

  this (EPixmap* aimg) {
    if (aimg is null) throw new Exception("no image for window");
    img = aimg;
    imgrc = GxRect(0, 0, aimg.width, aimg.height);
    super(imgrc);
  }

  this (EPixmap* aimg, int x, int y, GxRect aimgrc, string acmd=null) {
    if (aimg is null) throw new Exception("no image for window");
    img = aimg;
    imgrc = aimgrc;
    cmd = acmd;
    super(GxRect(x, y, aimgrc.width, aimgrc.height));
  }

  void onGrabbed (MouseEvent event) {}

  void onReleased (MouseEvent event) {
    if (rc.inside(event.x, event.y)) {
      if (cmd.length) concmd(cmd);
      if (onAction !is null) onAction();
    }
  }

  void onGrabbedMotion (MouseEvent event) {}

  override void onPaint () {
    auto irc = imgrc;
    if (active) irc.moveBy(0, imgrc.height);
    img.blitRect(swin, rc.x0, rc.y0, irc);
  }

  override bool onMouse (MouseEvent event) {
    if (event.button == MouseButton.left) {
      if (event.type == MouseEventType.buttonPressed && rc.inside(event.x, event.y)) {
        if (!active) { active = true; onGrabbed(event); }
        return true;
      }
    }
    if (event.type == MouseEventType.buttonReleased && event.button == MouseButton.left) {
      if (active) { onReleased(event); active = false; return true; }
    }
    if (event.type == MouseEventType.motion && event.modifierState&ModifierState.leftButtonDown && active) {
      onGrabbedMotion(event);
      return true;
    }
    if (super.onMouse(event)) return true;
    if (active) return true;
    return false;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
class AmpWidgetTitleButton : AmpWidgetButton {
  GxRect actimgrc;

  this (EPixmap* aimg, int x, int y, GxRect aimgrc, GxRect aactimgrc, string acmd=null) {
    if (aimg is null) throw new Exception("no image for window");
    img = aimg;
    imgrc = aimgrc;
    actimgrc = aactimgrc;
    cmd = acmd;
    super(GxRect(x, y, imgrc.width, imgrc.height));
  }

  override void onPaint () {
    auto irc = imgrc;
    if (active) irc = actimgrc;
    img.blitRect(swin, rc.x0, rc.y0, irc);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
class AmpWidgetToggle(string type) : AmpWidgetButton {
  bool* checked;
  private bool checkedvar;
  GxRect[2][2] xrc; // [active][pressed]

  this (bool* cvp, EPixmap* aimg, int x, int y, GxRect aimgrc, string acmd=null) {
    super(aimg, x, y, aimgrc, acmd);
    rc.width = aimgrc.width;
    rc.height = aimgrc.height;
    xrc[0][] = aimgrc;
    xrc[1][] = aimgrc;
    static if (type == "small") {
      xrc[0][1].moveBy(aimgrc.width*2, 0);
      xrc[1][0].moveBy(0, aimgrc.height);
      xrc[1][1].moveBy(aimgrc.width*2, aimgrc.height);
    } else if (type == "hor") {
      xrc[0][0].moveBy(50*0, 0);
      xrc[0][1].moveBy(59*2, 0);
      xrc[1][0].moveBy(59*1, 0);
      xrc[1][1].moveBy(59*3, 0);
    } else {
      xrc[0][1].moveBy(0, aimgrc.height*1);
      xrc[1][0].moveBy(0, aimgrc.height*2);
      xrc[1][1].moveBy(0, aimgrc.height*3);
    }
    if (cvp is null) {
      cvp = &checkedvar;
      onAction = delegate () { checkedvar = !checkedvar; };
    }
    checked = cvp;
  }

  override void onPaint () {
    auto irc = imgrc;
    int idx0 = (*checked ? 1 : 0);
    int idx1 = (active ? 1 : 0);
    img.blitRect(swin, rc.x0, rc.y0, xrc[idx0][idx1]);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
class AmpWidgetSongTitle : AmpWidgetButton {
  int titleofs = 0;
  int titleofsmovedir = 1;
  int titlemovepause = 8;
  string title;
  bool dragging = false;

  this(T:const(char)[]) (GxRect arc, T atitle) {
         static if (is(T == typeof(null))) title = null;
    else static if (is(T == string)) title = atitle;
    else title = atitle.idup;
    super(arc);
  }

  void setTitle(T:const(char)[]) (T atitle) {
    static if (is(T == typeof(null))) {
      setTitle("");
    } else {
      if (title == atitle) return;
      static if (is(T == string)) title = atitle; else title = atitle.idup;
      titleofs = 0;
      titleofsmovedir = 1;
      titlemovepause = 8;
    }
  }

  override void onPaint () {
    if (title.length && !rc.empty) {
      if (skin.tickerColClearEasy) {
        uint clr = skin.tickerColClear[0];
        drawFillRect(rc.x0, rc.y0, rc.width, rc.height, clr);
      } else {
        foreach (immutable int dy; 0..rc.height) {
          uint clr = skin.tickerColClear[dy%skin.tickerColClear.length];
          drawFillRect(rc.x0, rc.y0+dy, rc.width, 1, clr);
        }
      }
      setClipRect(rc);
      scope(exit) resetClip();
      gxDrawTextUtf(swin.impl.display, cast(Drawable)swin.impl.buffer, swin.impl.gc, rc.x0-titleofs, rc.y0-2, title, skin.tickerColText);
    }
  }

  final void scrollTitle () {
    if (parent.activeWidget !is this) dragging = false;
    if (dragging) return;
    if (titlemovepause-- >= 0) return;
    titlemovepause = 0;
    if (title.length == 0) { titleofs = 0; titleofsmovedir = 1; titlemovepause = 8; return; }
    auto tw = gxTextWidthUtf(title);
    if (tw <= rc.width) { titleofs = 0; titleofsmovedir = 1; titlemovepause = 8; return; }
    titleofs += titleofsmovedir;
         if (titleofs <= 0) { titleofs = 0; titleofsmovedir = 1; titlemovepause = 8; }
    else if (titleofs >= tw-rc.width) { titleofs = tw-rc.width; titleofsmovedir = -1; titlemovepause = 8; }
  }

  override void onDeactivate () { dragging = false; }

  override void onGrabbed (MouseEvent event) { dragging = true; }

  override void onReleased (MouseEvent event) {
    dragging = false;
    if (title.length != 0) {
      titlemovepause = 10;
      auto tw = gxTextWidthUtf(title);
           if (titleofs <= 0) { titleofs = 0; titleofsmovedir = 1; }
      else if (titleofs >= tw-rc.width) { titleofs = tw-rc.width; titleofsmovedir = -1; }
    }
  }

  override void onGrabbedMotion (MouseEvent event) {
    if (title.length != 0 && event.dx != 0) {
      titleofs -= event.dx;
      //conwriteln("dx=", event.dx, "; titleofs=", titleofs);
      titleofsmovedir = (event.dx < 0 ? 1 : -1);
      auto tw = gxTextWidthUtf(title);
           if (titleofs <= 0) titleofs = 0;
      else if (titleofs >= tw-rc.width) titleofs = tw-rc.width;
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
class AmpWidgetVolumeSlider : AmpWidgetButton {
  enum maxvalue = 63;

  this (EPixmap* aimg, GxRect arc) {
    super(aimg);
    img = aimg;
    rc = arc;
  }

  override void onPaint () {
    int val = softVolume;
    if (val < 0) val = 0;
    if (val > maxvalue) val = maxvalue;
    int sliderx = 0;
    auto bgrc = GxRect(0, 0, img.width, 13);
    if (maxvalue > 0) {
      int iidx = 27*val/maxvalue;
      bgrc.moveBy(0, 15*iidx);
      sliderx = (rc.width-14)*val/maxvalue;
    }
    img.blitRect(swin, rc.x0, rc.y0, bgrc);
    img.blitRect(swin, rc.x0+sliderx, rc.y0, GxRect(active ? 0 : 15, img.height-12, 14, 12));
  }

  override void onGrabbedMotion (MouseEvent event) {
    int nx = event.x-rc.x0;
    if (nx < 0) nx = 0;
    if (nx >= rc.width-14) nx = rc.width-14;
    int nv = 63*nx/(rc.width-14);
    if (nv != softVolume) concmdf!"soft_volume %d"(nv);
  }

  override bool onMouse (MouseEvent event) {
    // mouse wheel
    if (super.onMouse(event)) return true;
    if (event.type == MouseEventType.buttonPressed && rc.inside(event.x, event.y) && !active) {
      if (event.button == MouseButton.wheelDown) {
        if (softVolume > 0) concmdf!"soft_volume %d"(softVolume-1);
        return true;
      }
      if (event.button == MouseButton.wheelUp) {
        if (softVolume < maxvalue) concmdf!"soft_volume %d"(softVolume+1);
        return true;
      }
    }
    return active;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
class AmpWidgetPosBar : AmpWidgetButton {
  // at: 16,72
  // bar size: 248,10
  // 248,0; 29,10
  // 278,0; 29,10
  int newknobx = -666;
  int dragofs;
  int frozenPos = -666; // fixed position
  MonoTime unfreezeTime;

  this (EPixmap* aimg, GxRect arc) {
    super(aimg);
    img = aimg;
    rc = arc;
  }

  final void freezeAt (int frx) {
    frozenPos = frx;
    unfreezeTime = MonoTime.currTime+100.msecs;
  }

  final void unfreeze () {
    frozenPos = -666;
  }

  final int calcKnobX () {
    if (frozenPos != -666) {
      auto ctt = MonoTime.currTime;
      if (ctt < unfreezeTime) return frozenPos;
      frozenPos = -666;
    }
    int knobx = 0;
    int cur = aplayCurTimeMS;
    int tot = aplayTotalTimeMS;
    if (tot > 0) {
      if (cur < 0) cur = 0; else if (cur > tot) cur = tot;
      knobx = (248-28)*cur/tot;
    }
    return knobx;
  }

  override void onPaint () {
    int knobx = (newknobx == -666 ? calcKnobX() : newknobx);
    img.blitRect(swin, rc.x0, rc.y0, GxRect(0, 0, 248, 10));
    img.blitRect(swin, rc.x0+knobx, rc.y0, GxRect(active ? 278 : 248, 0, 29, 10));
  }

  override void onDeactivate () { newknobx = -666; }

  override void onGrabbed (MouseEvent event) {
    newknobx = (newknobx == -666 ? calcKnobX : newknobx);
    int x = event.x-rc.x0;
    if (x >= newknobx && x < newknobx+29) {
      dragofs = x-newknobx;
    } else {
      dragofs = 29/2;
      newknobx = x-dragofs;
      if (newknobx < 0) newknobx = 0;
      if (newknobx >= rc.width-28) newknobx = rc.width-28;
    }
  }

  override void onReleased (MouseEvent event) {
    auto tot = aplayTotalTimeMS;
    if (tot > 0) {
      freezeAt(newknobx);
      aplaySeekMS(tot*newknobx/(248-28));
    }
    newknobx = -666;
  }

  override void onGrabbedMotion (MouseEvent event) {
    int nx = event.x-rc.x0-dragofs;
    if (nx < 0) nx = 0;
    if (nx >= rc.width-28) nx = rc.width-28;
    newknobx = nx;
  }

  override bool onMouse (MouseEvent event) {
    // mouse wheel
    if (super.onMouse(event)) return true;
    if (event.type == MouseEventType.buttonPressed && rc.inside(event.x, event.y) && !active) {
      if (event.button == MouseButton.wheelDown) {
        concmdf!"song_seek_rel %d"(-10);
        return true;
      }
      if (event.button == MouseButton.wheelUp) {
        concmdf!"song_seek_rel %d"(10);
        return true;
      }
    }
    return active;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
class AmpMainWindow : AmpWindow {
  // option buttons (vertical): O A I D U
  //   pos: 11, 24
  //   size: 7, 8
  // menu button: 6, 3  size: 9, 9
  // minimize button: 244, 3  size 9, 9
  // maximize button: 254, 3  size 9, 9
  // close button: 264, 3  size 9, 9
  AmpWidgetSongTitle wtitle;
  MonoTime stblinktime;
  bool blinking;

  this () {
    super(&skin.imgMain);
    addWidget(new AmpWidgetButton(&skin.imgCButtons, 16, 88, GxRect(0, 0, 23, 18), "song_prev"));
    addWidget(new AmpWidgetButton(&skin.imgCButtons, 39, 88, GxRect(23, 0, 23, 18), "song_play"));
    addWidget(new AmpWidgetButton(&skin.imgCButtons, 62, 88, GxRect(46, 0, 23, 18), "song_pause_toggle"));
    addWidget(new AmpWidgetButton(&skin.imgCButtons, 85, 88, GxRect(69, 0, 23, 18), "song_stop"));
    addWidget(new AmpWidgetButton(&skin.imgCButtons, 108, 88, GxRect(92, 0, 22, 18), "song_next"));
    addWidget(new AmpWidgetButton(&skin.imgCButtons, 136, 88, GxRect(114, 0, 22, 16), "song_eject"));
    addWidget(new AmpWidgetToggle!"vert"(&modeShuffle, &skin.imgShufRep, 164, 89, GxRect(28, 0, 46, 15), "mode_shuffle toggle"));
    addWidget(new AmpWidgetToggle!"vert"(&modeRepeat, &skin.imgShufRep, 210, 89, GxRect(0, 0, 28, 15), "mode_repeat toggle"));
    addWidget(new AmpWidgetToggle!"small"(&eqVisible, &skin.imgShufRep, 219, 58, GxRect(0, 61, 23, 12), "eq_visible toggle"));
    addWidget(new AmpWidgetToggle!"small"(&plVisible, &skin.imgShufRep, 242, 58, GxRect(23, 61, 23, 12), "pl_visible toggle"));
    wtitle = cast(AmpWidgetSongTitle)addWidget(new AmpWidgetSongTitle(GxRect(109+2, 24, 157-4, 12), "Amper audio player: the blast from the past!"));
    addWidget(new AmpWidgetVolumeSlider(&skin.imgVolume, GxRect(107, 57, 68, 13)));
    addWidget(new AmpWidgetPosBar(&skin.imgPosBar, GxRect(16, 72, 248, 10)));

    addWidget(new AmpWidgetTitleButton(&skin.imgTitle, 6, 3, GxRect(0, 0, 9, 9), GxRect(0, 9, 9, 9), "")); // menu
    addWidget(new AmpWidgetTitleButton(&skin.imgTitle, 244, 3, GxRect(9, 0, 9, 9), GxRect(9, 9, 9, 9), "")); // minimize
    addWidget(new AmpWidgetTitleButton(&skin.imgTitle, 254, 3, GxRect(0, 18, 9, 9), GxRect(9, 18, 9, 9), "")); // maximize
    addWidget(new AmpWidgetTitleButton(&skin.imgTitle, 264, 3, GxRect(18, 0, 9, 9), GxRect(18, 9, 9, 9), "win_toggle"));
  }

  final void scrollTitle () {
    if (wtitle !is null) wtitle.scrollTitle();
  }

  final bool isInsideTimer (int x, int y) {
    return (x >= 36 && y >= 26 && x < 99 && y < 26+13);
  }

  final void drawTime () {
    // 9x13
    void drawDigit (int x, int dig) {
      if (dig < 0) dig = 0; else if (dig > 9) dig = 9;
      skin.imgNums.blitRect(swin, x, 26, GxRect(9*dig, 0, 9, 13));
    }
    if (aplayIsPlaying && aplayIsPaused) {
      enum BlinkPeriod = 800;
      //glconPostScreenRepaintDelayed(100);
      auto ctt = MonoTime.currTime;
      if (!blinking) { stblinktime = ctt; blinking = true; }
      auto tm = (ctt-stblinktime).total!"msecs"%(BlinkPeriod*2);
      //conwriteln(tm);
      if (tm >= BlinkPeriod) return;
    } else {
      blinking = false;
    }
    int curtime = aplayCurTime;
    if (modeRemaining) {
      int dur = aplayTotalTime;
      if (dur > 0) curtime = dur-curtime;
    }
    if (curtime < 0) curtime = 0;
    int mins = curtime/60;
    int secs = curtime%60;
    //if (mins > 99) mins = 99;
    drawDigit(48, mins/10);
    drawDigit(60, mins%10);
    drawDigit(78, secs/10);
    drawDigit(90, secs%10);
    if (modeRemaining) {
      // here, it is complicated
      if (skin.imgNums.width > 99) {
        // has minus
        skin.imgNums.blitRect(swin, 36, 26, GxRect(9*11, 0, 9, 13));
      } else {
        // no minus, use "2"
        skin.imgNums.blitRect(swin, 36, 26+6, GxRect(9*2, 6, 9, 1));
      }
    } else {
      // blank space instead of minus
      skin.imgNums.blitRect(swin, 36, 26, GxRect(9*10, 0, 9, 13));
    }
  }

  final void drawSampleRate () {
    void drawDigit (int x, int dig) {
      if (dig < 0) dig = 0; else if (dig > 9) dig = 9;
      skin.imgText.blitRect(swin, x, 43, GxRect(5*dig, 6, 5, 6));
    }
    int srt = aplaySampleRate()/1000;
    drawDigit(156+5*0, (srt/10)%10);
    drawDigit(156+5*1, srt%10);
  }

  final void drawMonoStereo () {
    //212, 41  29, 12
    //239, 41  29, 12
    int actidx = -1; // 0: stereo; 1: mono
    if (aplayIsPlaying) actidx = (aplayIsStereo ? 0 : 1);
    enum x = 212;
    enum y = 41;
    auto irc = GxRect(29, 0, 29, 12);
    irc.y0 = (actidx == 1 ? 0 : 12);
    skin.imgMonoStereo.blitRect(swin, x, y, irc);
    irc.x0 = 0;
    irc.y0 = (actidx == 0 ? 0 : 12);
    skin.imgMonoStereo.blitRect(swin, x+irc.width, y, irc);
  }

  protected override void paintBackground () {
    img.blitAt(swin, 0, 0);
    auto irc = GxRect(27, 0, 275, 14);
    if (!active) irc.moveBy(0, 15);
    skin.imgTitle.blitRect(swin, 0, 0, irc); // title
    skin.imgTitle.blitRect(swin, 11, 24, GxRect(305, 2, 7, 39)); // option buttions
    /*
    skinImgTitle.blitRect(264, 3, GxRect(18, 0, 9, 9)); // close
    skinImgTitle.blitRect(244, 3, GxRect(9, 0, 9, 9)); // minimize
    skinImgTitle.blitRect(254, 3, GxRect(0, 18, 9, 9)); // maximize
    */
    drawTime();
    drawSampleRate();
    drawMonoStereo();
  }

  override bool onKeyPost (KeyEvent event) {
    if (event.pressed) {
      if (event == "P") { concmd("pl_visible toggle"); return true; }
      if (event == "E") { concmd("eq_visible toggle"); return true; }
      if (event == "S") { concmd("mode_shuffle toggle"); return true; }
      if (event == "R") { concmd("mode_repeat toggle"); return true; }
      if (event == "Z") { concmd("song_prev"); return true; }
      if (event == "X") { concmd("song_play"); return true; }
      if (event == "C") { concmd("song_pause_toggle"); return true; }
      if (event == "V") { concmd("song_stop"); return true; }
      if (event == "B") { concmd("song_next"); return true; }
      if (event == "Left") { concmd("song_seek_rel -10"); return true; }
      if (event == "Right") { concmd("song_seek_rel +10"); return true; }
      if (event == "Up") { concmd("soft_volume_rel +2"); return true; }
      if (event == "Down") { concmd("soft_volume_rel -2"); return true; }
      if (event == "Delete") { concmd("soft_volume 31"); return true; }
    }
    return super.onKeyPost(event);
  }

  override bool onMousePost (MouseEvent event) {
    if (event.type == MouseEventType.buttonPressed && event.button == MouseButton.left) {
      if (isInsideTimer(event.x, event.y)) { concmd("mode_remaining toggle"); return true; }
    }
    return false;
  }

  void newSong(T:const(char)[]) (T tit) {
    wtitle.setTitle!T(tit);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
struct PListItem {
  string fname;
  string visline;
  string title;
  string album;
  string artist;
  int duration; // in seconds
  bool scanned;
}


class AmpPListWindow : AmpWindow {
  static struct State {
    PListItem[] items;
    int[] shuffleidx;
    int totaldur = 0;
    int topitem = 0;
    int curitem = 0;
    int curplayingitem = -1;
  }

  State state;
  int lastItemClickIndex = -1;
  MonoTime lastItemClickTime;
  //private XlibTCImage* ttc;

public:
  this () {
    super(&skin.imgMain);
    img = &skin.imgPlaylist;
  }

  auto saveData () { return state; }

  void restoreData (ref State sd) {
    state = sd;
    lastItemClickIndex = -1;
  }

  int findShuffleFirst () {
    if (state.items.length == 0) return 0;
    foreach (immutable idx, int val; state.shuffleidx) if (val == 0) return cast(int)idx;
    return 0;
  }

  void playPrevSong () {
    scope(exit) glconPostScreenRepaint();
    if (state.items.length == 0) { concmd("song_stop"); return; }
    int cidx = state.curplayingitem;
    if (cidx < 0) cidx = state.curitem;
    if (cidx < 0 || cidx >= state.items.length) { concmd("song_stop"); return; } // just in case
    int prev;
    if (modeShuffle) {
      prev = state.shuffleidx[cidx]-1;
      if (prev < 0) {
        if (!modeRepeat) { concmd("song_stop"); return; }
        prev = cast(int)state.items.length-1;
      }
      foreach (immutable idx, int val; state.shuffleidx) if (val == prev) { prev = cast(int)idx; break; }
      //conwriteln("P: cur=", state.curitem, "; prev=", prev, "; shuf=", state.shuffleidx);
      if (prev < 0 || prev >= state.items.length) { concmd("song_stop"); return; }
    } else {
      prev = cidx-1;
      if (prev < 0) {
        if (!modeRepeat) { concmd("song_stop"); return; }
        prev = cast(int)state.items.length-1;
      }
    }
    if (state.curitem == cidx) state.curitem = prev;
    //state.curplayingitem = prev;
    normTop();
    concmdf!"song_play_by_index %d"(prev);
  }

  void playNextSong (bool forceplay=false) {
    scope(exit) glconPostScreenRepaint();
    if (state.items.length == 0) { concmd("song_stop"); return; }
    int cidx = state.curplayingitem;
    if (cidx < 0) cidx = (modeShuffle ? findShuffleFirst : state.curitem);
    if (cidx < 0 || cidx >= state.items.length) { concmd("song_stop"); return; } // just in case
    int next;
    if (modeShuffle) {
      next = state.shuffleidx[cidx]+1;
      if (next >= state.items.length) {
        if (!modeRepeat) { concmd("song_stop"); return; }
        next = 0;
      }
      foreach (immutable idx, int val; state.shuffleidx) if (val == next) { next = cast(int)idx; break; }
      //conwriteln("N: cur=", state.curitem, "; next=", next, "; shuf=", state.shuffleidx);
    } else {
      next = cidx+1;
      if (next >= state.items.length) {
        if (!modeRepeat) { concmd("song_stop"); return; }
        next = 0;
      }
      if (state.curitem == cidx) state.curitem = next;
      state.curplayingitem = next;
    }
    if (state.curitem == cidx) state.curitem = next;
    //state.curplayingitem = next;
    normTop();
    concmdf!"song_play_by_index %d %s"(next, (forceplay ? "tan" :""));
  }

  void playCurrentSong () {
    if (state.items.length == 0) { concmd("song_stop"); return; }
    int cidx = state.curplayingitem;
    if (cidx < 0) cidx = (modeShuffle ? findShuffleFirst : 0);
    if (cidx < 0 || cidx >= state.items.length) { concmd("song_stop"); return; } // just in case
    normTop();
    playSongByIndex(cidx, true);
  }

  void playSelectedSong () {
    if (state.items.length == 0) { concmd("song_stop"); return; }
    normTop();
    if (state.curitem < 0 || state.curitem >= state.items.length) { concmd("song_stop"); return; }
    playSongByIndex(state.curitem, true);
  }

  void playSongByIndex (int plidx, bool forcestart=false) {
    if (plidx >= 0 && plidx < state.items.length) {
      ampPList.state.curplayingitem = plidx;
      ampMain.newSong(state.items[plidx].visline);
      aplayPlayFile(state.items[plidx].fname, forcestart);
    }
  }

  void stopSong () {
    aplayStopFile();
  }

  void clear () {
    foreach (ref PListItem pi; state.items) if (!pi.scanned) aplayCancelScan(pi.fname);
    state.items[] = PListItem.init;
    state.items.length = 0;
    state.items.assumeSafeAppend;
    state.shuffleidx.length = 0;
    state.shuffleidx.assumeSafeAppend;
    state.totaldur = 0;
    state.topitem = 0;
    state.curitem = 0;
    state.curplayingitem = -1;
    lastItemClickIndex = -1;
  }

  void appendListItem (string fname) {
    auto li = PListItem(fname);
    //if (li.duration > 0) state.totaldur += li.duration;
    {
      auto optr = state.items.ptr;
      state.items ~= li;
      if (state.items.ptr !is optr) {
        import core.memory : GC;
        if (state.items.ptr is GC.addrOf(state.items.ptr)) GC.setAttr(state.items.ptr, GC.BlkAttr.NO_INTERIOR);
      }
    }
    // update shuffle
    {
      auto optr = state.shuffleidx.ptr;
      state.shuffleidx ~= cast(int)state.shuffleidx.length;
      if (state.shuffleidx.ptr !is optr) {
        import core.memory : GC;
        if (state.shuffleidx.ptr is GC.addrOf(state.shuffleidx.ptr)) GC.setAttr(state.shuffleidx.ptr, GC.BlkAttr.NO_INTERIOR);
      }
      import std.random;
      auto swapn = uniform!"[)"(0, state.shuffleidx.length);
      if (swapn != state.shuffleidx.length-1) {
        auto v = state.shuffleidx[swapn];
        state.shuffleidx[swapn] = state.shuffleidx[$-1];
        state.shuffleidx[$-1] = v;
      }
    }
    lastItemClickIndex = -1;

    foreach (immutable idx, ref PListItem pi; state.items) {
      if (pi.scanned && pi.fname == li.fname) {
        scanResult(pi.fname, pi.album, pi.artist, pi.title, pi.duration*1000);
        break;
      }
    }
    if (!state.items[$-1].scanned) {
      state.items[$-1].visline = state.items[$-1].fname;
      aplayQueueScan(state.items[$-1].fname);
    }
  }

  final void scanResult (string filename, string album, string artist, string title, int durationms) {
    string tit;
    if (artist.length != 0 && title.length != 0) {
      tit = artist~" \u2014 "~title;
    } else if (artist.length != 0) {
      tit = artist~" \u2014 Unkown Song";
    } else if (title.length != 0) {
      tit = "Unknown Artist \u2014 "~title;
    } else {
      tit = filename;
    }
    bool updated = false;
    foreach (immutable idx, ref PListItem pi; state.items) {
      if (!pi.scanned && pi.fname == filename) {
        pi.visline = tit;
        pi.title = title;
        pi.album = album;
        pi.artist = artist;
        pi.duration = durationms/1000;
        pi.scanned = true;
        if (pi.duration > 0) state.totaldur += pi.duration;
        updated = true;
      }
    }
    if (updated) glconPostScreenRepaint();
  }

  private bool removeItemByIndex (usize idx) {
    if (idx >= state.items.length) return false;
    auto pi = &state.items[idx];
    if (pi.scanned) {
      if (pi.duration > 0) state.totaldur -= pi.duration;
    } else {
      aplayCancelScan(pi.fname);
    }
    pi.visline = null;
    pi.title = null;
    pi.album = null;
    pi.artist = null;
    pi.duration = -1;
    pi.scanned = true;
    auto ssidx = state.shuffleidx[idx];
    foreach (immutable cc; idx+1..state.items.length) {
      state.items.ptr[cc-1] = state.items.ptr[cc];
      state.shuffleidx.ptr[cc-1] = state.shuffleidx.ptr[cc];
    }
    state.items[$-1] = PListItem.init;
    state.items.length -= 1;
    state.items.assumeSafeAppend;
    state.shuffleidx.length -= 1;
    state.shuffleidx.assumeSafeAppend;
    // fix shuffle indicies
    foreach (ref sv; state.shuffleidx) if (sv >= ssidx) --sv;
    normTop();
    return true;
  }

  final void scanResultFailed (string filename) {
    bool updated = false;
    usize idx = 0;
    while (idx < state.items.length) {
      if (state.items[idx].fname == filename) {
        removeItemByIndex(idx);
        updated = true;
      } else {
        ++idx;
      }
    }
    if (updated) glconPostScreenRepaint();
  }

  final @property int visibleFullItems () const nothrow @trusted {
    int hgt = height-20-38;
    int res = hgt/gxTextHeightUtf;
    if (res < 1) res = 1;
    return res;
  }

  final @property int visibleItems () const nothrow @trusted {
    int hgt = height-20-38;
    int res = hgt/gxTextHeightUtf;
    if (hgt%gxTextHeightUtf) ++res;
    if (res < 1) res = 1;
    return res;
  }

  void normTop () nothrow @trusted {
    if (state.curitem < 0) state.curitem = 0;
    if (state.items.length == 0) { state.topitem = 0; return; }
    if (state.curitem >= state.items.length) state.curitem = cast(int)state.items.length-1;
    immutable vfi = visibleFullItems;
    immutable vi = visibleItems;
    immutable ci = state.curitem;
    int tl = state.topitem;
    if (tl < 0) tl = 0;
    if (tl > ci) tl = ci;
    if (tl+vfi <= ci) tl = ci-vfi+1;
    int el = tl+vi-1;
    if (el >= state.items.length) tl = cast(int)state.items.length-vfi;
    if (tl < 0) tl = 0;
    state.topitem = tl;
  }

  final @property int knobYOfs () nothrow @trusted {
    normTop();
    version (none) {
      if (state.items.length < visibleFullItems+1) return 0;
      int hgt = imgrc.height-37-19-18;
      if (hgt < 1) return 0;
      return cast(int)(hgt*state.topitem/(state.items.length-visibleFullItems));
    } else {
      if (state.items.length < 2) return 0;
      int hgt = height-37-19-18;
      if (hgt < 1) return 0;
      return cast(int)(hgt*state.curitem/(state.items.length-1));
    }
    // normal knob: 52,53  8x18  at width-15,[19..height-37)
  }

  final void paintScrollKnob () {
    int xofs = width-15;
    int yofs = knobYOfs+19;
    img.blitRect(swin, xofs, yofs, GxRect(52, 53, 8, 18));
  }

  final GxRect listClipRect () {
    return GxRect(11, 20, width-11-20+1, height-20-38);
  }

  final void paintList () {
    char[1024] buf = void;
    GxRect savedclip = listClipRect();

    normTop();

    setClipRect(savedclip);
    scope(exit) resetClip();
    drawFillRect(savedclip, skin.plistColNormalBG);
    if (state.items.length < 1) return;
    int ty = savedclip.y0;
    int idx = state.topitem;
    assert(idx >= 0);
    while (idx < state.items.length && ty <= savedclip.y1) {
      import core.stdc.stdio : snprintf;

      if (state.curitem == idx) drawFillRect(savedclip.x0, ty, savedclip.width, gxTextHeightUtf, skin.plistColCurrentBG);

      uint clr = (/*state.curitem == idx ||*/ state.curplayingitem == idx ? skin.plistColCurrent : skin.plistColNormal);

      int timeWidth = 1;
      auto dur = state.items.ptr[idx].duration;
      usize len;
      if (dur > 0) {
        if (dur >= 60*60) {
          len = snprintf(buf.ptr, buf.length, "%d:%02d:%02d", dur/(60*60), (dur/60)%60, dur%60);
        } else {
          len = snprintf(buf.ptr, buf.length, "%d:%02d", dur/60, dur%60);
        }
        auto tw = gxTextWidthUtf(buf[0..len]);
        timeWidth = tw+6;
        gxDrawTextUtf(swin.impl.display, cast(Drawable)swin.impl.buffer, swin.impl.gc, savedclip.x1-tw, ty, buf[0..len], clr);
      }
      int availWidth = savedclip.width-timeWidth-1;

      {
        string tit = state.items.ptr[idx].visline;
        len = 0;
        if (plEllipsisAtStart) {
          uint start = 0;
          while (start < tit.length) {
            len = snprintf(buf.ptr, buf.length, "%d. %s%.*s", idx+1, (start ? "...".ptr : "".ptr), cast(uint)(tit.length-start), tit.ptr+start);
            auto twdt = gxTextWidthUtf(buf[0..len]);
            if (twdt <= availWidth) break;
            Utf8DecoderFast dc;
            while (start < tit.length) if (dc.decode(cast(ubyte)tit.ptr[start++])) break;
            len = 0;
          }
        } else {
          bool ellipsis = false;
          while (tit.length) {
            len = snprintf(buf.ptr, buf.length, "%d. %.*s%s", idx+1, cast(uint)tit.length, tit.ptr, (ellipsis ? "...".ptr : "".ptr));
            auto twdt = gxTextWidthUtf(buf[0..len]);
            if (twdt <= availWidth) break;
            tit = tit.utfchop;
            ellipsis = true;
            len = 0;
          }
        }
        if (len == 0) len = snprintf(buf.ptr, buf.length, "%d. ...", idx+1);
        gxDrawTextUtf(swin.impl.display, cast(Drawable)swin.impl.buffer, swin.impl.gc, savedclip.x0+2, ty, buf[0..len], clr);
      }

      ty += gxTextHeightUtf;
      ++idx;
    }
  }

  protected override void paintBackground () {
    // frame
    int tx, ty;
    auto irc = GxRect(0, 0, 25, 20);
    if (!active) irc.moveBy(0, 21);
    // top frame
    // tiles
    irc.x0 = 127;
    tx = irc.width;
    while (tx < width-irc.width) {
      img.blitRect(swin, tx, 0, irc);
      tx += irc.width;
    }
    // top title
    auto irtitle = irc;
    irtitle.x0 = 26;
    irtitle.width = 100;
    img.blitRect(swin, (width-irtitle.width)/2, 0, irtitle);
    // top left corner
    irc.x0 = 0;
    img.blitRect(swin, 0, 0, irc);
    // top right corner
    irc.x0 = 153;
    img.blitRect(swin, width-irc.width, 0, irc);
    // left and right bars
    ty = irc.height;
    irc.width = 25;
    //irc.width = 12;
    irc.height = 29;
    irc.y0 = 42;
    while (ty < height-irc.height) {
      // left bar
      irc.x0 = 0;
      img.blitRect(swin, 0, ty, irc);
      // right bar
      irc.x0 = 26;
      //irc.x0 = 31;
      img.blitRect(swin, width-irc.width, ty, irc);
      ty += irc.height;
    }
    // bottom frame
    // left part: 0,72  125x38
    // right part: 126,72  150x38
    // tile: 179,0  25x38
    irc.height = 38;
    ty = height-irc.height;
    // tiles
    irc.width = 25;
    irc.x0 = 179;
    irc.y0 = 0;
    tx = 125;
    while (tx < width-150) {
      img.blitRect(swin, tx, ty, irc);
      tx += irc.width;
    }
    // bottom left corner
    irc.x0 = 0;
    irc.y0 = 72;
    irc.width = 125;
    img.blitRect(swin, 0, ty, irc);
    // bottom right corner
    irc.x0 = 126;
    irc.width = 150;
    img.blitRect(swin, width-irc.width, ty, irc);
    // paint list
    paintList();
  }

  protected override void paintFinished () {
    paintScrollKnob();
    // close pressed: 52,42  9x9  at width-10
    // normal knob: 52,53  8x18  at width-15,[19..height-37)
    // pressed knob: 61,53  8x18  at width-15,[19..height-37)
    // resize knob at top-right, size is 20x20
  }

  override bool onKeyPost (KeyEvent event) {
    if (event.pressed) {
      scope(exit) normTop();
      if (event == "Up") { --state.curitem; return true; }
      if (event == "Down") { ++state.curitem; return true; }
      if (event == "Home") { state.curitem = 0; return true; }
      if (event == "End") { state.curitem = cast(int)state.items.length-1; return true; }
      if (event == "Enter") { concmdf!"song_play_by_index %d tan"(state.curitem); return true; }
      if (event == "Delete") { removeItemByIndex(state.curitem); return true; }
    }
    return super.onKeyPost(event);
  }

  override bool onMousePost (MouseEvent event) {
    //setListClip();
    if (listClipRect.inside(event.x, event.y)) {
      normTop();
      scope(exit) { normTop(); if (lastItemClickIndex != state.curitem) lastItemClickIndex = -1; }
      if (event.type == MouseEventType.buttonPressed) {
        switch (event.button) {
          case MouseButton.wheelUp:
            if (state.curitem > 0) --state.curitem;
            return true;
          case MouseButton.wheelDown:
            ++state.curitem;
            return true;
          case MouseButton.left:
            int ci = state.topitem+(event.y-listClipRect.y0)/gxTextHeightUtf;
            if (ci != state.curitem) { lastItemClickIndex = -1; state.curitem = ci; normTop(); }
            auto ctt = MonoTime.currTime;
            //conwriteln("curitem=", curitem, "; lastItemClickIndex=", lastItemClickIndex, "; delta=", (ctt-lastItemClickTime).total!"msecs");
            if (lastItemClickIndex != state.curitem) {
              lastItemClickIndex = state.curitem;
              lastItemClickTime = ctt;
            } else {
              if ((ctt-lastItemClickTime).total!"msecs" < 350) {
                concmdf!"song_play_by_index %d tan"(lastItemClickIndex);
              }
              lastItemClickIndex = -1;
            }
            break;
          default:
        }
      }
    }
    return false;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
class AmpWidgetEqSlider : AmpWidgetButton {
  // first row: 13, 164
  // second row: 13, 229
  // element dimensions: 14, 63
  // 2 rows by 14 items; item index is idx*15
  // preamp: 21, 38
  // 10 eq bars: 78, 38
  // knob: 0, 164  0, 176
  // knob size: 11, 11
  // x: +1; y: +52-pos
  enum zerovalue = 14;
  enum maxvalue = zerovalue*2-1; // 2 rows by 14 items
  //int value;
  bool dragging;
  int dragYOfs;
  int bandidx; // -1: preamp

  this (int aidx, int x, int y) {
    super(&skin.imgEq);
    img = &skin.imgEq;
    rc = GxRect(x, y, 14, 63);
    bandidx = aidx;
    //value = aplayGetEqBand(aidx);
  }

  override void onPaint () {
    int val = value;
    if (val < 0) val = 0;
    if (val > maxvalue) val = maxvalue;
    int col = val%14;
    int row = val/14;
    img.blitRect(swin, rc.x0, rc.y0, GxRect(13+15*col, 164+65*row, 14, 63));
    img.blitRect(swin, rc.x0+1, rc.y0+knobYOfs, GxRect(0, (active ? 176 : 164), 11, 11));
  }

  final int value () { return aplayGetEqBand(bandidx); }

  final void setValue (int newv) {
    if (newv < 0) newv = 0;
    if (newv > maxvalue) newv = maxvalue;
    //if (newv != value) value = newv;
    aplaySetEqBand(bandidx, newv);
  }

  final int knobYOfs () {
    int val = value;
    if (val < 0) val = 0;
    if (val > maxvalue) val = maxvalue;
    return 52-(51*val/maxvalue);
  }

  final int locy2value (int locy) {
    if (locy < 2) locy = 2;
    if (locy > 51) locy = 51;
    return maxvalue-maxvalue*locy/51;
  }

  override void onGrabbed (MouseEvent event) {
    if (bandidx < 0) { parent.activeWidget = null; return; }
    if (!rc.inside(event.x, event.y)) { parent.activeWidget = null; return; }
    dragging = true;
    event.y -= rc.y0;
    int kyofs = knobYOfs;
    if (event.y >= kyofs && event.y < kyofs+11) {
      // inside the knob
    } else {
      // outside the knob
      setValue(locy2value(event.y-5));
    }
    dragYOfs = event.y-kyofs;
  }

  override void onReleased (MouseEvent event) {
    if (dragging) {
      dragging = false;
      //if (cmd.length) concmd(cmd);
      //if (onAction !is null) onAction();
    }
  }

  override void onGrabbedMotion (MouseEvent event) {
    if (dragging) {
      event.y -= rc.y0;
      event.y -= dragYOfs;
      setValue(locy2value(event.y));
    }
  }

  override bool onMouse (MouseEvent event) {
    // mouse wheel
    if (super.onMouse(event)) return true;
    if (event.type == MouseEventType.buttonPressed && rc.inside(event.x, event.y) && !active) {
      if (event.button == MouseButton.wheelDown) { if (bandidx >= 0) setValue(value-1); return true; }
      if (event.button == MouseButton.wheelUp) { if (bandidx >= 0) setValue(value+1); return true; }
    }
    return active;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
class AmpEqWindow : AmpWindow {
  AmpWidgetEqSlider preamp;
  AmpWidgetEqSlider[10] bands;

  this () {
    super(&skin.imgEq);
    setSize(275, 116);
    addWidget(new AmpWidgetToggle!"hor"(&alsaEnableEqualizer, &skin.imgEq, 14, 18, GxRect(10, 119, 25, 12), "use_equalizer toggle")); // on/off
    addWidget(new AmpWidgetToggle!"hor"(null, &skin.imgEq, 39, 18, GxRect(35, 119, 33, 12), "")); // auto
    addWidget(new AmpWidgetButton(&skin.imgEq, 217, 18, GxRect(224, 164, 44, 12), "song_prev"));
    // preamp
    preamp = cast(AmpWidgetEqSlider)addWidget(new AmpWidgetEqSlider(-1, 21, 38));
    // band sliders
    foreach (immutable int idx; 0..10) bands[idx] = cast(AmpWidgetEqSlider)addWidget(new AmpWidgetEqSlider(idx, 72+18*idx, 38));
  }

  override bool onKeyPost (KeyEvent event) {
    return super.onKeyPost(event);
  }
}
