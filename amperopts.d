/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module amperopts;
private:

import core.time;

import arsd.color;
import arsd.image;
import arsd.simpledisplay;

import iv.cmdcon;
import iv.cmdcongl;
import iv.strex;
import iv.vfs.io;

import aplayer;

import egfx;


// ////////////////////////////////////////////////////////////////////////// //
public __gshared EgfxWindow sdampwin;
public __gshared EgfxWindow sdplwin;
public __gshared EgfxWindow sdeqwin;

public __gshared int lastPlOffsetX = 0, lastPlOffsetY = 116;
public __gshared int lastEqOffsetX = 275, lastEqOffsetY = 0;
public __gshared int lastPlWidth = 275, lastPlHeight = 116;


// ////////////////////////////////////////////////////////////////////////// //
//__gshared string skinfile = "skins/WastedAMP1-0.zip";
//__gshared string skinfile = "skins/winamp2.zip";
//public __gshared string skinfile = "/mnt/bigass/src/xmms/skindev/winamp29x.zip";
public __gshared string skinfile = "!BUILTIN!";
public __gshared bool modeShuffle = false;
public __gshared bool modeRepeat = false;
public __gshared int softVolume = 31; // [0..63]; *100/31 will be volume in percents
public __gshared bool eqVisibleCfg = false;
public __gshared bool plVisibleCfg = true;
public __gshared bool eqVisible = false;
public __gshared bool plVisible = true;
public __gshared bool eqVisibleChanged = false;
public __gshared bool plVisibleChanged = false;
public __gshared bool modeRemaining = false;
public __gshared bool plEllipsisAtStart = false;


shared static this () {
  conRegVar!lastPlOffsetX("cfg_last_offset_pl_x", "internal configuration variable");
  conRegVar!lastPlOffsetY("cfg_last_offset_pl_y", "internal configuration variable");
  conRegVar!lastEqOffsetX("cfg_last_offset_eq_x", "internal configuration variable");
  conRegVar!lastEqOffsetY("cfg_last_offset_eq_y", "internal configuration variable");

  conRegVar!lastPlWidth("cfg_last_width_pl", "internal configuration variable");
  conRegVar!lastPlHeight("cfg_last_height_pl", "internal configuration variable");

  conRegVar!plEllipsisAtStart("pl_ellipsis_at_start", "put ellipsis at start in too long playlist items",
    delegate (ConVarBase self, bool oldval, bool newval) {
      if (oldval != newval) glconPostScreenRepaint();
    },
  );
  conRegVar!modeShuffle("mode_shuffle", "shuffle playlist",
    delegate (ConVarBase self, bool oldval, bool newval) {
      if (oldval != newval) glconPostScreenRepaint();
    },
  );
  conRegVar!modeRepeat("mode_repeat", "repeat playlist",
    delegate (ConVarBase self, bool oldval, bool newval) {
      if (oldval != newval) glconPostScreenRepaint();
    },
  );
  conRegVar!modeRemaining("mode_remaining", "show remaining time in song timer instead of passed",
    delegate (ConVarBase self, bool oldval, bool newval) {
      if (oldval != newval) glconPostScreenRepaint();
    },
  );

  conRegVar!softVolume(0, 63, "soft_volume", "soft volume; 31 is normal",
    delegate (ConVarBase self, int oldval, int newval) {
      //conwriteln("volume changed: old=", oldval, "; new=", newval);
      if (oldval != newval) glconPostScreenRepaint();
      aplayPlayGain(newval*100/31-100);
    },
  );
  conRegFunc!((int delta) {
    int newval = softVolume+delta;
    if (newval < 0) newval = 0;
    if (newval > 63) newval = 63;
    if (newval != softVolume) {
      softVolume = newval;
      glconPostScreenRepaint();
      aplayPlayGain(newval*100/31-100);
    }
  })("soft_volume_rel", "soft volume, relative");

  conRegVar!plVisibleCfg("cfg_pl_visible", "internal configuration variable");
  conRegVar!eqVisibleCfg("cfg_eq_visible", "internal configuration variable");

  conRegVar!plVisible("pl_visible", "toggle playlist",
    delegate (ConVarBase self, bool oldval, bool newval) {
      plVisibleChanged = true;
      if (oldval != newval) {
        if (sdampwin !is null && !sdampwin.closed && !sdampwin.hidden && sdplwin !is null && !sdplwin.closed) {
          if (sdplwin.hidden != !newval) {
            if (newval) sdplwin.show(); else sdplwin.hide();
          }
        }
        glconPostScreenRepaint();
      }
    },
  );

  conRegVar!eqVisible("eq_visible", "toggle equalizer",
    delegate (ConVarBase self, bool oldval, bool newval) {
      eqVisibleChanged = true;
      if (oldval != newval) {
        if (sdampwin !is null && !sdampwin.closed && !sdampwin.hidden && sdeqwin !is null && !sdeqwin.closed) {
          if (sdeqwin.hidden != !newval) {
            if (newval) sdeqwin.show(); else sdeqwin.hide();
          }
        }
        glconPostScreenRepaint();
      }
    },
  );
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared string homepath = null;


string getHomePath () {
  if (homepath is null) {
    import core.stdc.stdlib : getenv;
    homepath = "";
    auto home = getenv("HOME");
    if (home !is null && home[0]) {
      import std.string : fromStringz;
      homepath = home.fromStringz.idup;
      if (homepath[$-1] != '/') homepath ~= '/';
    }
  }
  return homepath;
}


public void loadHomeConfig () {
  auto hd = getHomePath();
  if (hd.length == 0) return;
  concmdf!"exec \"%s.amper.rc\" tan"(hd); // user config
  conProcessQueue(); // load config
}


public void loadWindowConfig () {
  auto hd = getHomePath();
  if (hd.length == 0) return;
  concmdf!"exec \"%s.amper.windows.rc\" tan"(hd); // user config
  conProcessQueue(); // load config
}


public void saveWindowConfig () {
  __gshared string wincfile = null;
  if (wincfile is null) {
    auto hd = getHomePath();
    if (hd.length == 0) { wincfile = ""; return; }
    wincfile = hd~".amper.windows.rc";
  }
  if (wincfile.length == 0) return;
  try {
    auto fo = VFile(wincfile, "w");
    if (lastPlOffsetX != int.min) fo.writeln("cfg_last_offset_pl_x ", lastPlOffsetX);
    if (lastPlOffsetY != int.min) fo.writeln("cfg_last_offset_pl_y ", lastPlOffsetY);
    if (lastEqOffsetX != int.min) fo.writeln("cfg_last_offset_eq_x ", lastEqOffsetX);
    if (lastEqOffsetY != int.min) fo.writeln("cfg_last_offset_eq_y ", lastEqOffsetY);
    fo.writeln("cfg_last_width_pl ", lastPlWidth);
    fo.writeln("cfg_last_height_pl ", lastPlHeight);
    fo.writeln("cfg_pl_visible ", (plVisible ? "tan" : "ona"));
    fo.writeln("cfg_eq_visible ", (eqVisible ? "tan" : "ona"));
    fo.close();
  } catch (Exception e) {}
}
