module bmpx is aliced;

import arsd.color;
import arsd.simpledisplay;
import arsd.image;

import iv.cmdcon;
import iv.cmdcongl;

import iv.vfs;

//version = use_framebuffer;


// ////////////////////////////////////////////////////////////////////////// //
uint c2i (uint v) pure nothrow @safe @nogc { pragma(inline, true); return (v&0xff_00ff00u)|((v>>16)&0x00_0000ffu)|((v<<16)&0x00_ff0000u); }


// ////////////////////////////////////////////////////////////////////////// //
public class BmpWindow : SimpleWindow {
  TrueColorImage img;
  Image backbuf;
  Image fontbuf;
  Pixmap pximg;
  int pximgwdt, pximghgt, pximgscl;
  int mScale = 5;
  int xofs = 0, yofs;
  int rcx0, rcy0, rcx1, rcy1;
  bool hasrc;
  bool msdown;
  bool msdrag;
  int dragxofs, dragyofs;
  int dragmx0, dragmy0;

  // minsize will be taken from aampw
  // if resizestep is zero, size on that dimension is fixed
  this (MemoryImage aimg) {
    if (aimg is null) assert(0, "wtf?!");
    img = aimg.getAsTrueColorImage;
    sdpyWindowClass = "BMPVIEW_WINDOW";
    super(img.width, img.height, "Bitmap Viewer", OpenGlOptions.no, Resizability.allowResizing);
    setMinSize(img.width, img.height);
    createPixmap();
    //backbuf = new Image(img.width*scale, img.height*scale);
    //glconBackBuffer = this.backbuf;
    setupHandlers();
    glconCtlWindow = this;
    //glconResize(img.width, img.height);
    glconCtlWindow.addEventListener((GLConScreenRebuildEvent evt) { redraw(true); });
    glconCtlWindow.addEventListener((GLConScreenRepaintEvent evt) { redraw(false); });
  }

  final @property int scale () const pure nothrow @safe @nogc { pragma(inline, true); return mScale; }
  final @property void scale (int v) pure nothrow @safe @nogc { pragma(inline, true); if (v < 1) v = 1; else if (v > 16) v = 16; mScale = v; }

  final void freePixmap () {
    if (!closed) {
      if (pximg) { XFreePixmap(impl.display, pximg); pximg = 0; flushGui(); }
      delete backbuf;
      delete fontbuf;
    }
  }

  override void close () {
    freePixmap();
    super.close();
  }

  final int rcpixel (int x, int y) nothrow @safe @nogc {
    if (!hasrc) return 0;
    immutable int x0 = (rcx0 <= rcx1 ? rcx0 : rcx1);
    immutable int x1 = (rcx0 <= rcx1 ? rcx1 : rcx0);
    immutable int y0 = (rcy0 <= rcy1 ? rcy0 : rcy1);
    immutable int y1 = (rcy0 <= rcy1 ? rcy1 : rcy0);
    if (x < x0 || y < y0 || x > x1 || y > y1) return 0;
    int pixnum = -1;
         if (y == y0) pixnum = (x >= x0 && x <= x1 ? x-x0 : -1);
    else if (x == x1) pixnum = (y > y0 && y <= y1 ? (x1-x0+1)+(y-y0-1) : -1);
    else if (y == y1) pixnum = (x >= x0 && x < x1 ? (x1-x0+1)+(y1-y0+1)+(x1-x) : -1);
    else if (x == x0) pixnum = (y > y0 && y < y1 ? (x1-x0+1)*2+(y1-y0+1)+(y1-y-1) : -1);
    return (pixnum >= 0 ? pixnum%2+1 : 0);
  }

  final int rcwidth () nothrow @safe @nogc {
    immutable int x0 = (rcx0 <= rcx1 ? rcx0 : rcx1);
    immutable int x1 = (rcx0 <= rcx1 ? rcx1 : rcx0);
    return (x0 <= x1 ? x1-x0+1 : 0);
  }

  final int rcheight () nothrow @safe @nogc {
    immutable int y0 = (rcy0 <= rcy1 ? rcy0 : rcy1);
    immutable int y1 = (rcy0 <= rcy1 ? rcy1 : rcy0);
    return (y0 <= y1 ? y1-y0+1 : 0);
  }

  final void drawChar (int x, int y, char ch, Color clr) {
    immutable int bbwdt = fontbuf.width;
    immutable int bbhgt = fontbuf.height;
    if (x < 0 || y < 0 || x > bbwdt-10 || y > bbhgt-10) return;
    uint v = clr.asUint.c2i;
    //uint* destp = (cast(uint*)backbuf.getDataPointer)+y*bbwdt+x;
    uint* destp = (cast(uint*)fontbuf.getDataPointer)+y*bbwdt+x;
    foreach (immutable dy; 0..10) {
      ushort vv = glConFont10.ptr[cast(uint)ch*10+dy];
      auto dp = destp;
      foreach (immutable dx; 0..10) {
        if (vv&0x8000) *dp = v;
        vv <<= 1;
        ++dp;
      }
      destp += bbwdt;
    }
  }

  final void drawStr (int x, int y, const(char)[] s, Color clr) {
    foreach (char ch; s) {
      drawChar(x, y, ch, clr);
      x += 10;
    }
  }

  final void drawStrOut (int x, int y, const(char)[] s, Color clr, Color outline=Color.black) {
    foreach (immutable int dy; -1..2) {
      foreach (immutable int dx; -1..2) {
        if (dx || dy) drawStr(x+dx, y+dy, s, outline);
      }
    }
    drawStr(x, y, s, clr);
  }

  final void createPixmap (bool forceredraw=false) {
    if (mScale < 1) mScale = 1; else if (mScale > 16) mScale = 16;
    immutable int scl = mScale;
    immutable int iwdt = img.width;
    immutable int ihgt = img.height;
    //conwriteln("mScale=", mScale, "; scl=", scl, "; pximgscl=", pximgscl);
    if (pximgwdt != iwdt*scl || pximghgt != ihgt*scl || pximg == 0 || backbuf is null) {
      if (pximg) { XFreePixmap(impl.display, pximg); pximg = 0; }
      pximgwdt = iwdt*scl;
      pximghgt = ihgt*scl;
      pximgscl = scl;
      auto obb = backbuf;
      backbuf = new Image(pximgwdt, pximghgt, true);
      pximg = XCreatePixmap(display, cast(Drawable)window, pximgwdt, pximghgt, 24);
      flushGui();
      delete obb;
      forceredraw = true;
    } else if (pximgscl != scl) {
      pximgscl = scl;
      forceredraw = true;
    }
    if (forceredraw) {
      //conwriteln("mScale=", mScale, "; scl=", scl, "; pximgscl=", pximgscl, "; pw=", pximgwdt, "; ph=", pximghgt, "; w=", iwdt, "; h=", ihgt, "; bw=", backbuf.width, "; bh=", backbuf.height);
      auto srcp = cast(const(Color)*)img.imageData.colors.ptr;
      auto destp = cast(uint*)backbuf.getDataPointer;
      Color clr;
      foreach (immutable int sy; 0..ihgt) {
        auto dp = destp;
        destp += pximgwdt*scl;
        foreach (immutable int sx; 0..iwdt) {
          if (auto pn = rcpixel(sx, sy)) {
            clr = Color(pn == 1 ? 255 : 0, 0, pn != 1 ? 255 : 0);
          } else {
            clr = *srcp;
          }
          immutable uint v = clr.asUint.c2i;
          foreach (immutable int dy; 0..scl) dp[pximgwdt*dy..pximgwdt*dy+scl] = v;
          dp += scl;
          ++srcp;
        }
      }
      if (backbuf.usingXshm) {
        XShmPutImage(impl.display, cast(Drawable)pximg, impl.gc, backbuf.handle, 0, 0, 0, 0, pximgwdt, pximghgt, false);
      } else {
        XPutImage(impl.display, cast(Drawable)pximg, impl.gc, backbuf.handle, 0, 0, 0, 0, pximgwdt, pximghgt);
      }
      if (hasrc) {
        if (fontbuf is null) fontbuf = new Image(32*10+20, 10+4, true);
        auto fbu = cast(uint*)fontbuf.getDataPointer;
        fbu[0..fontbuf.width*fontbuf.height] = 0;
        import core.stdc.stdio : snprintf;
        char[128] buf = void;
        auto len = snprintf(buf.ptr, buf.length, "pos: %d,%d  size: %d,%d", rcx0, rcy0, rcwidth, rcheight);
        drawStrOut(2, 2, buf[0..len], Color(0, 255, 0));
      }
      //flushGui();
    }
  }

  void redraw (bool forceredraw) {
    if (closed || backbuf is null) return;
    createPixmap(forceredraw);
    XCopyArea(impl.display, cast(Drawable)pximg, cast(Drawable)impl.window, impl.gc, 0, 0, pximgwdt, pximghgt, xofs*mScale, yofs*mScale);
    Color clr = Color(128, 128, 128);
    XSetForeground(impl.display, impl.gc, cast(uint)((clr.r<<16)|(clr.g<<8)|clr.b));
    XSetBackground(impl.display, impl.gc, cast(uint)((clr.r<<16)|(clr.g<<8)|clr.b));
    int sx = xofs*mScale;
    int sy = yofs*mScale;
    int ex = sx+pximgwdt;
    int ey = sy+pximghgt;
    if (sx < 0) sx = 0;
    if (sy < 0) sy = 0;
    if (ex < 0) ex = 0;
    if (ey < 0) ey = 0;
    if (sx > 0) XFillRectangle(impl.display, cast(Drawable)impl.window, impl.gc, 0, 0, sx, height);
    if (sy > 0) XFillRectangle(impl.display, cast(Drawable)impl.window, impl.gc, sx, 0, width-sx, sy);
    if (ex < width) XFillRectangle(impl.display, cast(Drawable)impl.window, impl.gc, ex, 0, width-ex, height); else ex = width;
    if (ey < height) XFillRectangle(impl.display, cast(Drawable)impl.window, impl.gc, 0, ey, ex, height-ey);
    if (hasrc && fontbuf !is null) {
      if (fontbuf.usingXshm) {
        XShmPutImage(impl.display, cast(Drawable)impl.window, impl.gc, fontbuf.handle, 0, 0, 0, 0, fontbuf.width, fontbuf.height, false);
      } else {
        XPutImage(impl.display, cast(Drawable)impl.window, impl.gc, fontbuf.handle, 0, 0, 0, 0, fontbuf.width, fontbuf.height);
      }
    }
    //glconDraw();
    /*
    {
      auto painter = this.draw();
      painter.drawImage(Point(0, 0), backbuf);
    }
    */
    flushGui();
  }

  protected void setupHandlers () {
    handleExpose = delegate (int x, int y, int wdt, int hgt, int eventsLeft) {
      if (eventsLeft == 0) redraw(false);
      return true;
    };
    visibilityChanged = delegate (bool visible) {
      if (visible) createPixmap(); else freePixmap();
    };
    handleKeyEvent = delegate (KeyEvent event) {
      scope(exit) if (!conQueueEmpty()) glconPostDoConCommands();
      //if (glconKeyEvent(event)) { glconPostScreenRepaint(); return; }
      if (isQuitRequested) { close(); return; }
      if ((event.modifierState&ModifierState.numLock) == 0) {
        switch (event.key) {
          case Key.Pad0: event.key = Key.Insert; break;
          case Key.Pad1: event.key = Key.End; break;
          case Key.Pad2: event.key = Key.Down; break;
          case Key.Pad3: event.key = Key.PageDown; break;
          case Key.Pad4: event.key = Key.Left; break;
          //case Key.Pad5: event.key = Key.Insert; break;
          case Key.Pad6: event.key = Key.Right; break;
          case Key.Pad7: event.key = Key.Home; break;
          case Key.Pad8: event.key = Key.Up; break;
          case Key.Pad9: event.key = Key.PageUp; break;
          case Key.PadEnter: event.key = Key.Enter; break;
          case Key.PadDot: event.key = Key.Delete; break;
          default: break;
        }
      } else {
        if (event.key == Key.PadEnter) event.key = Key.Enter;
      }
      if (event.pressed) {
        //conwriteln(event.toStr);
        if (event == "C-Q") { close(); return; }
        if (event == "Escape") { msdown = false; hasrc = false; glconPostScreenRepaint(); return; }
        if (event == "Plus") { scale = scale+1; glconPostScreenRepaint(); return; }
        if (event == "Minus") { scale = scale-1; glconPostScreenRepaint(); return; }
        if (event == "Left") { xofs += 1; glconPostScreenRepaint(); return; }
        if (event == "Right") { xofs -= 1; glconPostScreenRepaint(); return; }
        if (event == "Up") { yofs += 1; glconPostScreenRepaint(); return; }
        if (event == "Down") { yofs -= 1; glconPostScreenRepaint(); return; }
        if (event == "Home") { xofs = yofs = 0; glconPostScreenRepaint(); return; }
      }
    };

    handleMouseEvent = delegate (MouseEvent event) {
      scope(exit) if (!conQueueEmpty()) glconPostDoConCommands();
      if (isQuitRequested) { close(); return; }
      immutable int scl = mScale;
      immutable int mx = event.x/scl-xofs;
      immutable int my = event.y/scl-yofs;
      if (event.type == MouseEventType.buttonPressed) {
        if (event.modifierState&ModifierState.ctrl) {
          if (event.button == MouseButton.wheelUp) { scale = scale+1; glconPostScreenRepaint(); return; }
          if (event.button == MouseButton.wheelDown) { scale = scale-1; glconPostScreenRepaint(); return; }
        }
        if (event.button == MouseButton.left) {
          if (event.modifierState&(ModifierState.ctrl|ModifierState.alt)) {
            msdrag = true;
            msdown = false;
            dragxofs = xofs;
            dragyofs = yofs;
            dragmx0 = event.x;
            dragmy0 = event.y;
            glconPostScreenRepaint();
          } else {
            msdrag = false;
            msdown = true;
            hasrc = true;
            //conwriteln("pos: ", mx, ", ", my);
            rcx0 = rcx1 = mx;
            rcy0 = rcy1 = my;
            glconPostScreenRepaint();
          }
        }
        if (event.button == MouseButton.right) conwriteln(mx, ", ", my);
        return;
      }
      if (event.type == MouseEventType.buttonReleased) {
        if (event.button == MouseButton.left) { msdown = msdrag = false; glconPostScreenRepaint(); }
        return;
      }
      if (event.type == MouseEventType.motion && msdown) {
        immutable int ow = rcwidth, oh = rcheight;
        rcx1 = mx;
        rcy1 = my;
        if (ow != rcwidth || oh != rcheight) glconPostScreenRepaint();
        return;
      }
      if (event.type == MouseEventType.motion && msdrag) {
        immutable int dx = (event.x-dragmx0)/scl;
        immutable int dy = (event.y-dragmy0)/scl;
        immutable int nxofs = dragxofs+dx;
        immutable int nyofs = dragyofs+dy;
        if (nxofs != xofs || nyofs != yofs) {
          xofs = nxofs;
          yofs = nyofs;
          glconPostScreenRepaint();
        }
        return;
      }
    };

    handleCharEvent = delegate (dchar ch) {
      scope(exit) if (!conQueueEmpty()) glconPostDoConCommands();
      //if (glconCharEvent(ch)) { glconPostScreenRepaint(); return; }
      if (isQuitRequested) { close(); return; }
    };

    windowResized = delegate (int wdt, int hgt) {
      scope(exit) if (!conQueueEmpty()) glconPostDoConCommands();
      if (isQuitRequested) { close(); return; }
      if (wdt < 1) wdt = 1;
      if (hgt < 1) hgt = 1;
      //glconResize(wdt, hgt);
      if (backbuf.width != wdt || backbuf.height != hgt) {
        //createPixmap();
        //backbuf = new Image(wdt, hgt);
        //glconBackBuffer = this.backbuf;
      }
      redraw(false);
    };

    onFocusChange = delegate (bool focused) { msdown = msdrag = false; };
  }
}


void main (string[] args) {
  glconAllowOpenGLRender = false;
  if (args.length == 1) args ~= "MAIN.bmp";

  conProcessArgs!true(args);
  conProcessQueue(int.max/4);

  auto win = new BmpWindow(loadImageFromFile(VFile(args[1])));

  flushGui();
  win.eventLoop(0);
  flushGui();
  conProcessQueue(int.max/4);
}
