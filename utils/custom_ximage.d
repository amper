module bmpx is aliced;

import arsd.color;
import arsd.simpledisplay;
import arsd.image;

import iv.cmdcon;
import iv.cmdcongl;

import iv.vfs;

import egfx.base;
import egfx.util;
import egfx.backx : XlibTCImage;

version = custom_img;


// ////////////////////////////////////////////////////////////////////////// //
// "simple" XImage methods
private extern(C) {
  import core.stdc.config : c_long, c_ulong;

  XImage* xxsimple_create_image (XDisplay* display, Visual* visual, uint depth, int format, int offset, ubyte* data, uint width, uint height, int bitmap_pad, int bytes_per_line) {
    return XCreateImage(display, visual, depth, format, offset, data, width, height, bitmap_pad, bytes_per_line);
  }

  int xxsimple_destroy_image (XImage* ximg) {
    if (ximg.data !is null) {
      import core.stdc.stdlib : free;
      free(ximg.data);
      ximg.data = null;
    }
    ximg.width = ximg.height = 0;
    return 0;
  }

  c_ulong xxsimple_get_pixel (XImage* ximg, int x, int y) {
    if (ximg.data is null) return 0;
    if (x < 0 || y < 0 || x >= ximg.width || y >= ximg.height) return 0;
    auto buf = cast(const(uint)*)ximg.data;
    return buf[y*ximg.width+x];
  }

  int xxsimple_put_pixel (XImage* ximg, int x, int y, c_ulong clr) {
    if (ximg.data is null) return 0;
    if (x < 0 || y < 0 || x >= ximg.width || y >= ximg.height) return 0;
    auto buf = cast(uint*)ximg.data;
    buf[y*ximg.width+x] = cast(uint)clr;
    return 0;
  }

  XImage* xxsimple_sub_image (XImage* ximg, int x, int y, uint wdt, uint hgt) {
    import core.stdc.stdlib : malloc, free;
    import core.stdc.string : memset;
    if (wdt < 1 || hgt < 1) return null;
    XImage* res = cast(XImage*)malloc(XImage.sizeof);
    if (res is null) return null;
    memset(res, 0, XImage.sizeof);
    ximageCreateSimple(*res, wdt, hgt);
    foreach (immutable int dy; y..y+hgt) {
      foreach (immutable int dx; x..x+wdt) {
        uint clr = 0;
        if (dx >= 0 && dy >= 0 && dx < ximg.width && dy < ximg.height) clr = cast(uint)ximg.f.get_pixel(ximg, dx, dy);
        res.f.put_pixel(res, dx-x, dy-y, clr);
      }
    }
    return res;
  }

  int xxsimple_add_pixel (XImage* ximg, c_long clr) {
    if (ximg.data is null) return 0;
    auto buf = cast(uint*)ximg.data;
    foreach (ref uint v; buf[0..ximg.width*ximg.height]) v += cast(uint)clr;
    return 0;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// create "simple" XImage with allocated buffer
private void ximageCreateSimple (ref XImage handle, int width, int height) {
  import core.stdc.stdlib : malloc;

  if (width < 1) width = 1;
  if (height < 1) height = 1;

  auto data = malloc(width*height*4);
  if (data is null) assert(0, "out of memory");

  handle.width = width;
  handle.height = height;
  handle.xoffset = 0;
  handle.format = ImageFormat.ZPixmap;
  handle.data = data;
  handle.byte_order = 0;
  handle.bitmap_unit = 0;
  handle.bitmap_bit_order = 0;
  handle.bitmap_pad = 0;
  handle.depth = 24;
  handle.bytes_per_line = 0;
  handle.bits_per_pixel = 0; // THIS MATTERS!
  handle.red_mask = 0;
  handle.green_mask = 0;
  handle.blue_mask = 0;

  handle.obdata = null;
  handle.f.create_image = &xxsimple_create_image;
  handle.f.destroy_image = &xxsimple_destroy_image;
  handle.f.get_pixel = &xxsimple_get_pixel;
  handle.f.put_pixel = &xxsimple_put_pixel;
  handle.f.sub_image = &xxsimple_sub_image;
  handle.f.add_pixel = &xxsimple_add_pixel;
}


// ////////////////////////////////////////////////////////////////////////// //
public struct XlibImage {
  XImage handle;

  @disable this (this);

  this (MemoryImage aimg) {
    if (aimg is null || aimg.width < 1 || aimg.height < 1) throw new Exception("can't create xlib image from empty MemoryImage");
    create(aimg);
  }

  this (int wdt, int hgt) {
    if (wdt < 1 || hgt < 1) throw new Exception("invalid xlib image");
    create(new TrueColorImage(wdt, hgt));
  }

  ~this () { dispose(); }

  @property bool valid () const pure nothrow @trusted @nogc { pragma(inline, true); return (handle.obdata !is null); }

  @property int width () const pure nothrow @trusted @nogc { pragma(inline, true); return handle.width; }
  @property int height () const pure nothrow @trusted @nogc { pragma(inline, true); return handle.height; }

  void setup (MemoryImage aimg) {
    dispose();
    if (aimg is null || aimg.width < 1 || aimg.height < 1) throw new Exception("can't create xlib image from empty MemoryImage");
    create(aimg);
  }

  private void create (MemoryImage ximg) {
    handle.width = ximg.width;
    handle.height = ximg.height;
    handle.xoffset = 0;
    handle.format = ImageFormat.ZPixmap;
    handle.data = null;
    handle.byte_order = 0;
    handle.bitmap_unit = 0;
    handle.bitmap_bit_order = 0;
    handle.bitmap_pad = 0;
    handle.depth = 24;
    handle.bytes_per_line = 0;
    handle.bits_per_pixel = 0; // THIS MATTERS!
    handle.red_mask = 0;
    handle.green_mask = 0;
    handle.blue_mask = 0;

    handle.obdata = *cast(void**)&ximg;
    handle.f.create_image = &xx_create_image;
    handle.f.destroy_image = &xx_destroy_image;
    handle.f.get_pixel = &xx_get_pixel;
    handle.f.put_pixel = &xx_put_pixel;
    handle.f.sub_image = &xx_sub_image;
    handle.f.add_pixel = &xx_add_pixel;
  }

  void dispose () {
    // note: this calls free(rawData) for us
    if (handle.obdata !is null) {
      XDestroyImage(&handle);
      handle.obdata = null;
    }
  }

  // blit to window buffer
  final void blitAt (SimpleWindow w, int destx, int desty) {
    blitRect(w, destx, desty, GxRect(0, 0, width, height));
  }

  // blit to window buffer
  final void blitRect (SimpleWindow w, int destx, int desty, GxRect srect) {
    if (w is null || handle.obdata is null || w.closed) return;
    XPutImage(w.impl.display, cast(Drawable)w.impl.buffer, w.impl.gc, &handle, srect.x0, srect.y0, destx, desty, srect.width, srect.height);
  }

  // blit to window
  final void blitAtWin (SimpleWindow w, int destx, int desty) {
    blitRectWin(w, destx, desty, GxRect(0, 0, width, height));
  }

  // blit to window
  final void blitRectWin (SimpleWindow w, int destx, int desty, GxRect srect) {
    if (w is null || handle.obdata is null || w.closed) return;
    XPutImage(w.impl.display, cast(Drawable)w.impl.window, w.impl.gc, &handle, srect.x0, srect.y0, destx, desty, srect.width, srect.height);
  }

static private extern(C):
  import core.stdc.config : c_long, c_ulong;

  XImage* xx_create_image (XDisplay* display, Visual* visual, uint depth, int format, int offset, ubyte* data, uint width, uint height, int bitmap_pad, int bytes_per_line) {
    return XCreateImage(display, visual, depth, format, offset, data, width, height, bitmap_pad, bytes_per_line);
  }

  int xx_destroy_image (XImage* ximg) {
    MemoryImage img = *cast(MemoryImage*)&ximg.obdata;
    if (img !is null) ximg.obdata = null;
    ximg.width = ximg.height = 0;
    return 0;
  }

  c_ulong xx_get_pixel (XImage* ximg, int x, int y) {
    MemoryImage img = *cast(MemoryImage*)&ximg.obdata;
    if (img is null) return 0;
    return img.getPixel(x, y).c2img;
  }

  int xx_put_pixel (XImage* ximg, int x, int y, c_ulong clr) {
    MemoryImage img = *cast(MemoryImage*)&ximg.obdata;
    if (img !is null && x >= 0 && y >= 0 && x < img.width && y < img.height) img.setPixel(x, y, img2c(cast(uint)clr));
    return 0;
  }

  XImage* xx_sub_image (XImage* ximg, int x, int y, uint wdt, uint hgt) {
    return xxsimple_sub_image(ximg, x, y, wdt, hgt);
  }

  int xx_add_pixel (XImage* ximg, c_long clr) {
    //assert(0, "xx_add_pixel: alas!");
    return 1;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
version(custom_img) {
  __gshared XlibImage xlimg;
} else {
  __gshared XlibTCImage xlimg;
}


// ////////////////////////////////////////////////////////////////////////// //
/+
public struct EPixmapImpl {
  Pixmap xpm;
  private int mWidth, mHeight;

  this (int wdt, int hgt) {
    //if (width < 1 || height < 1) throw new Exception("invalid pixmap dimensions");
    if (wdt < 1) wdt = 1;
    if (hgt < 1) hgt = 1;
    if (wdt > 1024) wdt = 1024;
    if (hgt > 1024) hgt = 1024;
    xpm = XCreatePixmap(glconCtlWindow.impl.display, cast(Drawable)glconCtlWindow.impl.window, wdt, hgt, 24);
    mWidth = wdt;
    mHeight = hgt;
  }

  this (ref XlibTCImage xtc) {
    if (!xtc.valid) throw new Exception("can't create pixmap from empty xlib image");
    int wdt = xtc.width;
    int hgt = xtc.height;
    if (wdt < 1) wdt = 1;
    if (hgt < 1) hgt = 1;
    if (wdt > 1024) wdt = 1024;
    if (hgt > 1024) hgt = 1024;
    xpm = XCreatePixmap(glconCtlWindow.impl.display, cast(Drawable)glconCtlWindow.impl.window, wdt, hgt, 24);
    // source x, source y
    XPutImage(glconCtlWindow.impl.display, cast(Drawable)xpm, glconCtlWindow.impl.gc, xtc.handle, 0, 0, 0, 0, wdt, hgt);
    mWidth = wdt;
    mHeight = hgt;
  }

  @disable this (this);

  ~this () {
    if (glconCtlWindow is null || glconCtlWindow.closed) { xpm = 0; return; }
    if (xpm) {
      XFreePixmap(glconCtlWindow.impl.display, xpm);
      xpm = 0;
    }
  }

  @property bool valid () const pure nothrow @trusted @nogc { pragma(inline, true); return (xpm != 0); }

  @property int width () const pure nothrow @trusted @nogc { pragma(inline, true); return mWidth; }
  @property int height () const pure nothrow @trusted @nogc { pragma(inline, true); return mHeight; }

  // blit to window buffer
  final void blitAt (SimpleWindow w, int x, int y) {
    blitRect(w, x, y, GxRect(0, 0, width, height));
  }

  // blit to window buffer
  final void blitRect (SimpleWindow w, int destx, int desty, GxRect srect) {
    if (w is null || !xpm || w.closed) return;
    XCopyArea(w.impl.display, cast(Drawable)xpm, cast(Drawable)w.impl.buffer, w.impl.gc, srect.x0, srect.y0, srect.width, srect.height, destx, desty);
  }
}


public alias EPixmap = KRC!EPixmapImpl;
+/


// ////////////////////////////////////////////////////////////////////////// //
public class BmpWindow : SimpleWindow {
  this () {
    sdpyWindowClass = "BMPVIEW_WINDOW";
    super(800, 600, "Bitmap Viewer", OpenGlOptions.no, Resizability.allowResizing);
    setMinSize(800, 60);
    setupHandlers();
    glconCtlWindow = this;
    //glconResize(img.width, img.height);
    glconCtlWindow.addEventListener((GLConScreenRepaintEvent evt) { redraw(true); });
    glconCtlWindow.addEventListener((GLConDoConsoleCommandsEvent evt) {
      scope(exit) if (!conQueueEmpty()) glconPostDoConCommands();
      glconProcessEventMessage();
      if (isQuitRequested) { glconCtlWindow.close(); return; }
    });
  }

  void redraw (bool forceredraw) {
    if (closed) return;
    //xlimg.blitAtWin(this, 5, 8);
    xlimg.blitAt(this, 5, 8);
    glconDrawWindow = this;
    glconDrawDirect = false;
    glconDraw();
    XCopyArea(this.impl.display, cast(Drawable)this.impl.buffer, cast(Drawable)this.impl.window, this.impl.gc, 0, 0, this.width, this.height, 0, 0);
    flushGui();
  }

  protected void setupHandlers () {
    handleExpose = delegate (int x, int y, int wdt, int hgt, int eventsLeft) {
      if (eventsLeft == 0) redraw(false);
      return true;
    };
    visibilityChanged = delegate (bool visible) {
      //if (visible) createPixmap(); else freePixmap();
    };
    handleKeyEvent = delegate (KeyEvent event) {
      scope(exit) if (!conQueueEmpty()) glconPostDoConCommands();
      if (glconKeyEvent(event)) { glconPostScreenRepaint(); return; }
      if (isQuitRequested) { close(); return; }
      if ((event.modifierState&ModifierState.numLock) == 0) {
        switch (event.key) {
          case Key.Pad0: event.key = Key.Insert; break;
          case Key.Pad1: event.key = Key.End; break;
          case Key.Pad2: event.key = Key.Down; break;
          case Key.Pad3: event.key = Key.PageDown; break;
          case Key.Pad4: event.key = Key.Left; break;
          //case Key.Pad5: event.key = Key.Insert; break;
          case Key.Pad6: event.key = Key.Right; break;
          case Key.Pad7: event.key = Key.Home; break;
          case Key.Pad8: event.key = Key.Up; break;
          case Key.Pad9: event.key = Key.PageUp; break;
          case Key.PadEnter: event.key = Key.Enter; break;
          case Key.PadDot: event.key = Key.Delete; break;
          default: break;
        }
      } else {
        if (event.key == Key.PadEnter) event.key = Key.Enter;
      }
      if (event.pressed) {
        //conwriteln(event.toStr);
        if (event == "C-Q") { close(); return; }
        if (event == "Escape") { close(); return; }
      }
    };

    handleMouseEvent = delegate (MouseEvent event) {
      scope(exit) if (!conQueueEmpty()) glconPostDoConCommands();
      if (isQuitRequested) { close(); return; }
    };

    handleCharEvent = delegate (dchar ch) {
      scope(exit) if (!conQueueEmpty()) glconPostDoConCommands();
      if (glconCharEvent(ch)) { glconPostScreenRepaint(); return; }
      if (isQuitRequested) { close(); return; }
    };

    windowResized = delegate (int wdt, int hgt) {
      scope(exit) if (!conQueueEmpty()) glconPostDoConCommands();
      if (isQuitRequested) { close(); return; }
      if (wdt < 1) wdt = 1;
      if (hgt < 1) hgt = 1;
      glconResize(wdt, hgt);
      /+
      if (backbuf.width != wdt || backbuf.height != hgt) {
        //createPixmap();
        //backbuf = new Image(wdt, hgt);
        //glconBackBuffer = this.backbuf;
      }
      +/
      redraw(false);
    };

    onFocusChange = delegate (bool focused) {};
  }
}


void main (string[] args) {
  version(none) {
    import core.stdc.stdio;

    printf("%u\n", XImage.sizeof);

    printf("width: %u\n", XImage.width.offsetof);
    printf("height: %u\n", XImage.height.offsetof);
    printf("xoffset: %u\n", XImage.xoffset.offsetof);
    printf("format: %u\n", XImage.format.offsetof);
    printf("data: %u\n", XImage.data.offsetof);
    printf("byte_order: %u\n", XImage.byte_order.offsetof);
    printf("bitmap_unit: %u\n", XImage.bitmap_unit.offsetof);
    printf("bitmap_bit_order: %u\n", XImage.bitmap_bit_order.offsetof);
    printf("bitmap_pad: %u\n", XImage.bitmap_pad.offsetof);
    printf("depth: %u\n", XImage.depth.offsetof);
    printf("bytes_per_line: %u\n", XImage.bytes_per_line.offsetof);
    printf("bits_per_pixel: %u\n", XImage.bits_per_pixel.offsetof);
    printf("red_mask: %u\n", XImage.red_mask.offsetof);
    printf("green_mask: %u\n", XImage.green_mask.offsetof);
    printf("blue_mask: %u\n", XImage.blue_mask.offsetof);
    printf("obdata: %u\n", XImage.obdata.offsetof);
    printf("f: %u\n", XImage.f.offsetof);
    printf("f.create_image: %u\n", XImage.f.offsetof+XImage.f.create_image.offsetof);
    printf("f.destroy_image: %u\n", XImage.f.offsetof+XImage.f.destroy_image.offsetof);
    printf("f.get_pixel: %u\n", XImage.f.offsetof+XImage.f.get_pixel.offsetof);
    printf("f.put_pixel: %u\n", XImage.f.offsetof+XImage.f.put_pixel.offsetof);
    printf("f.sub_image: %u\n", XImage.f.offsetof+XImage.f.sub_image.offsetof);
    printf("f.add_pixel: %u\n", XImage.f.offsetof+XImage.f.add_pixel.offsetof);
  }

  glconAllowOpenGLRender = false;
  if (args.length == 1) args ~= "EQMAIN.BMP";

  concmd("r_console tan");

  conProcessArgs!true(args);
  conProcessQueue(int.max/4);

  auto bmp = loadImageFromFile(VFile(args[1]));
  auto win = new BmpWindow();
  //glconCtlWindow = win;

  xlimg.setup(bmp);
  assert(xlimg.valid);
  conwriteln("size: ", xlimg.width, "x", xlimg.height);
  //xlimg.dispose();
  //return;
  /*
  {
    auto xi = XlibTCImage(bmp);
    conwriteln("size: ", xi.handle.width, "x", xi.handle.height);
    conwriteln("xoffset: ", xi.handle.xoffset);
    conwriteln("format: ", xi.handle.format);
    conwriteln("byte_order: ", xi.handle.byte_order);
    conwriteln("bitmap_unit: ", xi.handle.bitmap_unit);
    conwriteln("bitmap_bit_order: ", xi.handle.bitmap_bit_order);
    conwriteln("bitmap_pad: ", xi.handle.bitmap_pad);
    conwriteln("depth: ", xi.handle.depth);
    conwriteln("bytes_per_line: ", xi.handle.bytes_per_line);
    conwriteln("bits_per_pixel: ", xi.handle.bits_per_pixel);
    conwritefln!"red_mask: 0x%08x"(xi.handle.red_mask);
    conwritefln!"green_mask: 0x%08x"(xi.handle.green_mask);
    conwritefln!"blue_mask: 0x%08x"(xi.handle.blue_mask);
    return;
  }
  */

  flushGui();
  win.eventLoop(0);
  flushGui();
  conProcessQueue(int.max/4);
}
