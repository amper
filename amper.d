/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module amper;

import std.concurrency;

import arsd.color;
import arsd.image;
import arsd.simpledisplay;

import iv.cmdcon;
import iv.cmdcongl;
import iv.sdpyutil;
import iv.strex;
import iv.vfs;

import aplayer;

import egfx;

import amperrpcsrv;
import amperskin;
import amperopts;


// ////////////////////////////////////////////////////////////////////////// //
class EventSaveWindows {}
__gshared EventSaveWindows evSaveWindows;

shared static this () { evSaveWindows = new EventSaveWindows(); }


// ////////////////////////////////////////////////////////////////////////// //
public GxRect getWorkArea () {
  GxRect rc;
  getWorkAreaRect(rc.x0, rc.y0, rc.width, rc.height);
  return rc;
}


// ////////////////////////////////////////////////////////////////////////// //
class GlobalHotkeyEx : GlobalHotkey {
  string cmd;
  override void doHandle () { concmd(cmd); }

  this (ConString kname, ConString acmd) {
    super(kname);
    cmd = acmd.idup;
  }
}

__gshared GlobalHotkeyEx[] ghbindings; // key is key, value is command


void removeAllBindings () {
  char[128] knbuf;
  foreach (GlobalHotkeyEx b; ghbindings) {
    try { GlobalHotkeyManager.unregister(b.key.toStrBuf(knbuf[])); } catch (Exception e) {}
  }
  ghbindings.length = 0;
  ghbindings.assumeSafeAppend;
}


void addBinding (ConString key, ConString cmd) {
  cmd = cmd.xstrip;
  key = key.xstrip;
  if (cmd.length == 0) {
    GlobalHotkeyManager.unregister(key);
    foreach (immutable idx, GlobalHotkeyEx bind; ghbindings) {
      if (bind.key == key) {
        foreach (immutable cc; idx+1..ghbindings.length) ghbindings[cc-1] = ghbindings[cc];
        ghbindings[$-1] = null;
        ghbindings.length -= 1;
        ghbindings.assumeSafeAppend;
        return;
      }
    }
    return;
  }
  try {
    char[128] knbuf;
    auto bind = new GlobalHotkeyEx(key, cmd);
    GlobalHotkeyManager.unregister(bind.key.toStrBuf(knbuf[]));
    GlobalHotkeyManager.register(bind);
    foreach (immutable idx, GlobalHotkeyEx b; ghbindings) {
      if (b.key == key) {
        ghbindings[idx] = bind;
        return;
      }
    }
    auto optr = ghbindings.ptr;
    ghbindings ~= bind;
    if (optr !is ghbindings.ptr) {
      import core.memory : GC;
      if (ghbindings.ptr is GC.addrOf(ghbindings.ptr)) {
        GC.setAttr(ghbindings.ptr, GC.BlkAttr.NO_INTERIOR);
      }
    }
  } catch (Exception e) {
    conwriteln("ERROR registering hotkey: '", key, "'");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared SimpleWindow sdhint;
__gshared string hinttext = "not playing";
__gshared Timer hintHideTimer;
__gshared int hintX, hintY;


// ////////////////////////////////////////////////////////////////////////// //
void setHint(T:const(char)[]) (T str) {
  static if (is(T == typeof(null))) {
    setHint("");
  } else {
    if (hinttext == str) return;
    static if (is(T == string)) hinttext = str; else hinttext = str.idup;
    if (sdhint is null || sdhint.closed) return;
    if (sdhint.hidden) return;
    createHintWindow(hintX, hintY);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
GxPoint hintWindowTextOffset () { return GxPoint(3, 2); }

GxSize hintWindowSize () {
  int textWidth = gxTextWidthUtf(hinttext)+6;
  if (textWidth < 8) textWidth = 8;
  return GxSize(textWidth, gxTextHeightUtf+4);
}


void createHintWindow (int x, int y) {
  if (sdhint !is null) {
    sdhint.close();
    sdhint = null;
  }

  sdpyWindowClass = "AMPER_HINT_WINDOW";
  auto wsz = hintWindowSize();
  {
    auto wrc = getWorkArea();
    int nx = x;
    int ny = y;
    if (nx+wsz.width > wrc.x1) nx = wrc.x1-wsz.width+1;
    if (nx < wrc.x0) nx = wrc.x0;
    if (ny+wsz.height > wrc.y1) ny = wrc.y1-wsz.height+1;
    if (ny < wrc.y0) ny = wrc.y0;
    hintX = nx;
    hintY = ny;
  }
  sdhint = new SimpleWindow(wsz.width, wsz.height, "AmperHint", OpenGlOptions.no, Resizability.fixedSize, WindowTypes.undecorated, WindowFlags.skipTaskbar|WindowFlags.alwaysOnTop|WindowFlags.cannotBeActivated);
  XSetWindowBackground(sdhint.impl.display, sdhint.impl.window, gxRGB!(255, 255, 0));
  sdhint.handleExpose = delegate (int x, int y, int wdt, int hgt, int eventsLeft) {
    if (eventsLeft == 0) {
      if (sdhint is null || sdhint.closed) return false;
      //if (sdhint.hidden) return;
      auto wsz = hintWindowSize();

      XSetForeground(sdhint.impl.display, sdhint.impl.gc, gxRGB!(255, 255, 0));
      XFillRectangle(sdhint.impl.display, cast(Drawable)sdhint.impl.buffer, sdhint.impl.gc, 0, 0, wsz.width+1, wsz.height+1);

      XSetForeground(sdhint.impl.display, sdhint.impl.gc, gxRGB!(0, 0, 0));
      XDrawRectangle(sdhint.impl.display, cast(Drawable)sdhint.impl.buffer, sdhint.impl.gc, 0, 0, wsz.width-1, wsz.height-1);

      auto tofs = hintWindowTextOffset;
      gxDrawTextUtf(sdhint, tofs.x, tofs.y, hinttext, gxRGB!(0, 0, 0));

      //flushGui();
      return false; // sdpy will copy backbuffer
    }
    return true; // so sdpy will not draw backbuffer
  };

  // sorry for this hack
  sdhint.setNetWMWindowType(GetAtom!("_NET_WM_WINDOW_TYPE_DOCK", true)(sdhint.display));
  //sdhint.setNetWMWindowType(GetAtom!("_NET_WM_WINDOW_TYPE_TOOLTIP", true)(sdhint.display));
  {
    Atom[4] atoms;
    atoms[0] = GetAtom!("_NET_WM_STATE_STICKY", true)(sdhint.impl.display);
    atoms[1] = GetAtom!("_NET_WM_STATE_SKIP_TASKBAR", true)(sdhint.impl.display);
    atoms[2] = GetAtom!("_NET_WM_STATE_SKIP_PAGER", true)(sdhint.impl.display);
    atoms[3] = GetAtom!("_NET_WM_STATE_ABOVE", true)(sdhint.impl.display);
    XChangeProperty(
      sdhint.impl.display,
      sdhint.impl.window,
      GetAtom!("_NET_WM_STATE", true)(sdhint.impl.display),
      XA_ATOM,
      32 /* bits */,
      0 /*PropModeReplace*/,
      atoms.ptr,
      cast(int)atoms.length);
  }
  sdhint.moveResize(hintX, hintY, wsz.width, wsz.height);
  //repaintHintWindow(true);
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared NotificationAreaIcon trayicon;
__gshared Image trayimage;
__gshared MemoryImage icon; // 0: normal


void hideShowWindows () {
  if (sdampwin is null || sdampwin.closed) return;
  if (sdampwin.hidden) {
    sdampwin.show();
    flushGui();
    if (plVisible && sdplwin !is null && !sdplwin.closed) { sdplwin.show(); flushGui(); }
    if (eqVisible && sdeqwin !is null && !sdeqwin.closed) { sdeqwin.show(); flushGui(); }
    switchToWindow(sdampwin);
    flushGui();
  } else {
    if (sdeqwin !is null && !sdeqwin.closed && !sdeqwin.hidden) { /*saveEqWindowPosition();*/ sdeqwin.hideInternal(); }
    if (sdplwin !is null && !sdplwin.closed && !sdplwin.hidden) { /*savePlWindowPosition();*/ sdplwin.hideInternal(); }
    sdampwin.hide();
    flushGui();
  }
}


void prepareTrayIcon () {
  static immutable ubyte[] nticonpng = cast(immutable(ubyte)[])import("skins/notifyicon.png");
  //icon = readPng("skins/notifyicon.png");
  icon = imageFromPng(readPng(nticonpng));
  trayimage = Image.fromMemoryImage(icon);
  trayicon = new NotificationAreaIcon("Amper", trayimage, (int x, int y, MouseButton button, ModifierState mods) {
    //conwritefln!"x=%d; y=%d; button=%u; mods=0x%04x"(x, y, button, mods);
    if (button == MouseButton.middle) {
      //trayicon.close();
      //trayicon = null;
      concmd("quit");
      return;
    }
    if (button == MouseButton.left) {
      concmd("win_toggle");
      return;
    }
  });
  trayicon.onEnter = delegate (int x, int y, ModifierState mods) {
    //conwritefln!"icon enter: x=%d; y=%d; mods=0x%04x"(x, y, mods);
    /*
    conwritefln!"icon enter: x=%d; y=%d; mods=0x%04x"(x, y, mods);
    int wx, wy, wdt, hgt;
    trayicon.getWindowRect(wx, wy, wdt, hgt);
    conwriteln("window rect: wx=", wx, "; wy=", wy, "; wdt=", wdt, "; hgt=", hgt);
    */
    if (sdhint is null || sdhint.hidden) {
      createHintWindow(x+18, y+2);
      if (hintHideTimer !is null) hintHideTimer.destroy();
      hintHideTimer = new Timer(3000, delegate () {
        if (hintHideTimer !is null) {
          hintHideTimer.destroy();
          hintHideTimer = null;
          if (sdhint !is null && !sdhint.closed) sdhint.hide();
        }
      });
    }
  };
  trayicon.onLeave = delegate () {
    //conwriteln("icon leave");
    //if (sdhint !is null && !sdhint.closed) sdhint.hide();
  };
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared ubyte vbNewScale = 1;


// ////////////////////////////////////////////////////////////////////////// //
class ScrollTitleEvent {}
__gshared ScrollTitleEvent evScrollTitle;

shared static this () {
  evScrollTitle = new ScrollTitleEvent();
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared bool mainDrag = false;
__gshared int mainDrawPrevX, mainDrawPrevY;


// ////////////////////////////////////////////////////////////////////////// //
void closeAllIfMainIsClosed () {
  bool doQuit =
    (glconCtlWindow is null || glconCtlWindow.closed) ||
    (sdampwin is null || sdampwin.closed);
  if (doQuit) {
    if (sdhint !is null && !sdhint.closed) sdhint.close();
    if (sdeqwin !is null && !sdeqwin.closed) sdeqwin.close();
    if (sdplwin !is null && !sdplwin.closed) sdplwin.close();
    if (sdampwin !is null && !sdampwin.closed) sdampwin.close();
    if (glconCtlWindow !is null && !glconCtlWindow.closed) glconCtlWindow.close();
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void fixWindowPosition (SimpleWindow sw, int ofsx, int ofsy, int* wdt=null, int* hgt=null) {
  if (sdampwin is null || sdampwin.closed || sdampwin.hidden || sw is null || sw.closed) return;

  int ax, ay, aw, ah;
  getWindowRect(sdampwin, ax, ay, aw, ah);
  //conwriteln("pl-onsetup: ax=", ax, "; ay=", ay, "; aw=", aw, "; ah=", ah);
  if (aw < 1 || ah < 1) return;

  if (ofsx != int.min && ofsy != int.min) {
    //conwriteln("pl-onsetup: lastPlOffsetX=", lastPlOffsetX, "; lastPlOffsetY=", lastPlOffsetY);
    sw.move(ax+ofsx, ay+ofsy);
    if (wdt !is null && hgt !is null) sw.resize(*wdt, *hgt);
  }
}

void fixPlWindowPosition () { fixWindowPosition(sdplwin, lastPlOffsetX, lastPlOffsetY, &lastPlWidth, &lastPlHeight); }
void fixEqWindowPosition () { fixWindowPosition(sdeqwin, lastEqOffsetX, lastEqOffsetY); }


// ////////////////////////////////////////////////////////////////////////// //
void saveWindowPosition (SimpleWindow sw, ref int ofsx, ref int ofsy, int* wdt=null, int* hgt=null) {
  if (sdampwin is null || sdampwin.closed || sdampwin.hidden || sw is null || sw.closed) return;

  int ax, ay, aw, ah;
  getWindowRect(sdampwin, ax, ay, aw, ah);
  if (aw < 1 || ah < 1) return;

  int x, y, w, h;
  getWindowRect(sw, x, y, w, h);

  bool doSave = false;

  if (ofsx != x-ax) { doSave = true; ofsx = x-ax; }
  if (ofsy != y-ay) { doSave = true; ofsy = y-ay; }

  if (wdt !is null && w >= 275 && w != *wdt) { doSave = true; *wdt = w; }
  if (hgt !is null && h >= 116 && h != *hgt) { doSave = true; *hgt = h; }

  if (doSave) saveWindowConfig();
}

void savePlWindowPosition () { saveWindowPosition(sdplwin, lastPlOffsetX, lastPlOffsetY, &lastPlWidth, &lastPlHeight); }
void saveEqWindowPosition () { saveWindowPosition(sdeqwin, lastEqOffsetX, lastEqOffsetY); }


// ////////////////////////////////////////////////////////////////////////// //
// create hidden control window
void createCtlWindow () {
  sdpyWindowClass = "AMPER_PLAYER_CTL";
  glconCtlWindow = new SimpleWindow(1, 1, "AmperCtl", OpenGlOptions.no, Resizability.fixedSize, WindowTypes.eventOnly);
}


void setupCtlWindow () {
  glconCtlWindow.onClosing = delegate () {
    //conwriteln("closing ctl window...");
    if (sdeqwin !is null && !sdeqwin.closed) sdeqwin.close();
    if (sdplwin !is null && !sdplwin.closed) sdplwin.close();
    if (sdampwin !is null && !sdampwin.closed) sdampwin.close();
  };

  glconCtlWindow.onDestroyed = delegate () {
    //conwriteln("ctl window destroyed");
    closeAllIfMainIsClosed();
  };

  glconCtlWindow.addEventListener((QuitEvent evt) {
    scope(exit) if (!conQueueEmpty()) glconPostDoConCommands();
    scope(exit) closeAllIfMainIsClosed();
    if (glconCtlWindow.closed) return;
    if (isQuitRequested) { glconCtlWindow.close(); return; }
    concmd("quit");
  });

  void rebuildRepaint () {
    scope(exit) if (!conQueueEmpty()) glconPostDoConCommands();
    scope(exit) closeAllIfMainIsClosed();
    if (glconCtlWindow.closed) return;
    if (isQuitRequested) { glconCtlWindow.close(); return; }
    //conwriteln("rebuilding screen");

    if (sdampwin !is null && !sdampwin.closed && !sdampwin.hidden) sdampwin.redraw();
    if (sdplwin !is null && !sdplwin.closed && !sdplwin.hidden) sdplwin.redraw();
    if (sdeqwin !is null && !sdeqwin.closed && !sdeqwin.hidden) sdeqwin.redraw();

    flushGui();
  }

  glconCtlWindow.addEventListener((GLConScreenRepaintEvent evt) { rebuildRepaint(); });

  glconCtlWindow.addEventListener((GLConDoConsoleCommandsEvent evt) {
    scope(exit) if (!conQueueEmpty()) glconPostDoConCommands();
    scope(exit) closeAllIfMainIsClosed();
    glconProcessEventMessage();
    if (glconCtlWindow.closed) return;
    if (isQuitRequested) { glconCtlWindow.close(); return; }
  });

  glconCtlWindow.addEventListener((ScrollTitleEvent evt) {
    scope(exit) if (!conQueueEmpty()) glconPostDoConCommands();
    scope(exit) closeAllIfMainIsClosed();
    if (glconCtlWindow.closed) return;
    if (isQuitRequested) { glconCtlWindow.close(); return; }
    //conwriteln("scrolling title");
    ampMain.scrollTitle();
    if (!glconCtlWindow.eventQueued!ScrollTitleEvent) glconCtlWindow.postTimeout(evScrollTitle, 100);
    glconPostScreenRepaint();
  });

  glconCtlWindow.addEventListener((EventFileLoaded evt) {
    if (!evt.success) {
      conwriteln("ERROR loading '", evt.filename, "'");
      ampMain.newSong("not playing");
      setHint("not playing");
      concmd("song_next");
    } else {
      string cursong = evt.artist~" \u2014 "~evt.title;
      setHint(cursong);
      ampMain.newSong(cursong);
      //conwriteln("playing '", evt.filename, "': ", evt.artist, " -- ", evt.title);
    }
  });

  glconCtlWindow.addEventListener((EventFileScanned evt) {
    //conwriteln("scanned: '", evt.filename, "': ", evt.success);
    if (ampPList is null) return;
    if (evt.success) {
      ampPList.scanResult(evt.filename, evt.album, evt.artist, evt.title, evt.durationms);
    } else {
      ampPList.scanResultFailed(evt.filename);
    }
  });

  glconCtlWindow.addEventListener((EventFileComplete evt) {
    //glconCtlWindow.close();
    setHint("not playing");
    concmd("song_next tan");
  });

  glconCtlWindow.addEventListener!EventSaveWindows((EventSaveWindows evt) {
    saveEqWindowPosition();
    savePlWindowPosition();
    if (!glconCtlWindow.eventQueued!EventSaveWindows) glconCtlWindow.postTimeout(evSaveWindows, 10000);
  });
}


// ////////////////////////////////////////////////////////////////////////// //
void createAmpWindow () {
  ampMain = new AmpMainWindow();
  sdampwin = new EgfxWindow(ampMain, "AMPER_PLAYER", "Amper");
  if (!sdampwin.eventQueued!ScrollTitleEvent) glconCtlWindow.postEvent(evScrollTitle);
  sdampwin.visibleForTheFirstTime = delegate () {
    //flushGui();
    //switchToWindow(sdampwin);
    //glconCtlWindow.postTimeout(new EventFixupPListPosition(), 50);
    if (plVisible) sdplwin.show();
    if (eqVisible) sdeqwin.show();
    if (!glconCtlWindow.eventQueued!EventSaveWindows) glconCtlWindow.postTimeout(evSaveWindows, 10000);
  };
  sdampwin.onDismiss = delegate () { saveWindowConfig(); };
  sdampwin.onSetup = delegate () {
    //if (plVisible) sdplwin.show();
    //if (eqVisible) sdeqwin.show();
    //flushGui();
    //if (!glconCtlWindow.eventQueued!EventSetupWindows) glconCtlWindow.postTimeout!EventSetupWindows(new EventSetupWindows(), 100);
  };
}


// ////////////////////////////////////////////////////////////////////////// //
void createPListWindow () {
  ampPList = new AmpPListWindow();
  sdplwin = new EgfxWindow(ampPList, "AMPER_PLAYLIST", "Amper Playlist", 25, 29);
  sdplwin.onSetup = delegate () { fixPlWindowPosition(); };
  sdplwin.onDismiss = delegate () { savePlWindowPosition(); };
  sdplwin.visibilityChanged = delegate (bool visible) {
    if (!visible) {
      int x, y, w, h;
      getWindowRect(sdplwin, x, y, w, h);
      //conwriteln("vis=", visible, "; x=", x, "; y=", y, "; w=", w, "; h=", h);
      if (w >= 275 && h >= 116 && x > 0 && y > 0) savePlWindowPosition();
    }
  };
}


void createEqWindow () {
  ampEq = new AmpEqWindow();
  sdeqwin = new EgfxWindow(ampEq, "AMPER_EQIALIZER", "Amper Eqializer");
  sdeqwin.onSetup = delegate () { fixEqWindowPosition(); };
  sdeqwin.onDismiss = delegate () { saveEqWindowPosition(); };
  //sdeqwin.visibilityChanged = delegate (bool visible) { if (!visible) saveEqWindowPosition(); };
  sdeqwin.visibilityChanged = delegate (bool visible) {
    if (!visible) {
      int x, y, w, h;
      getWindowRect(sdeqwin, x, y, w, h);
      //conwriteln("vis=", visible, "; x=", x, "; y=", y, "; w=", w, "; h=", h);
      if (w >= 275 && h >= 116 && x > 0 && y > 0) saveEqWindowPosition();
    }
  };
}


// ////////////////////////////////////////////////////////////////////////// //
void scanDir (ConString path, bool append) {
  void appendFile(T:const(char)[]) (T fname) {
    static if (is(T == typeof(null))) {
      return;
    } else {
      static if (is(T == string)) alias fn = fname; else string fn = fname.idup;
      ampPList.appendListItem(fn);
    }
  }

  // scan directory
  import std.file;
  try {
    if (!append) ampPList.clear();
    if (path.exists && path.isFile) { appendFile(path); return; }
    foreach (DirEntry de; dirEntries(path.idup, SpanMode.shallow)) {
      if (!de.isFile) continue;
      appendFile(de.name);
    }
  } catch (Exception e) {
    conwriteln("ERROR scanning: ", e.msg);
  }
  //if (modeShuffle) ampPList.state.curitem = ampPList.findShuffleFirst();
  //conwriteln(ampPList.findShuffleFirst(), " : ", ampPList.state.shuffleidx, " : ", ampPList.state.curplayingitem);
}


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  conRegFunc!(() { import core.memory : GC; GC.collect(); GC.minimize(); })("gc_collect", "force garbage collection");

  //vbNewScale = 2;
  //vbufEffScale = 2;
  glconShowKey = "M-Grave";

  //conRegVar!vbNewScale(1, 8, "v_scale", "window scale");

  conRegFunc!((ConString key, ConString cmd) { addBinding(key, cmd); })("gh_bind", "global hotkey bind: key command");
  conRegFunc!((ConString key) { addBinding(key, null); })("gh_unbind", "global hotkey unbind: key");
  conRegFunc!((ConString key) { removeAllBindings(); })("gh_unbind_all", "unbind all global hotkeys unbind: key");

  conRegVar!skinfile("skin_file", "load skin from the given file",
    delegate (ConVarBase self, string oldval, string newval) {
      try {
        loadSkin(newval);
        skinfile = newval;
        glconPostScreenRepaint();
      } catch (Exception e) {
        conwriteln("ERROR loading skin: ", e.msg);
      }
    },
  );

  aplayStart();
  scope(exit) aplayShutdown();

  createCtlWindow();

  loadSkin!true("!BUILTIN!");

  createAmpWindow();
  createPListWindow();
  createEqWindow();
  setupSkinRegions();

  setupCtlWindow();

  aplayStartScanner();

  concmd("gh_bind M-H-A win_toggle");
  concmd("gh_bind M-H-Z song_prev");
  concmd("gh_bind M-H-X song_play");
  concmd("gh_bind M-H-C song_pause_toggle");
  concmd("gh_bind M-H-V song_stop");
  concmd("gh_bind M-H-B song_next");
  concmd("gh_bind M-H-Left \"song_seek_rel -10\"");
  concmd("gh_bind M-H-Right \"song_seek_rel +10\"");
  concmd("gh_bind M-H-Up \"soft_volume_rel +2\"");
  concmd("gh_bind M-H-Down \"soft_volume_rel -2\"");
  concmd("gh_bind M-H-Delete \"soft_volume 31\"");


  conRegFunc!((ConString path, bool append=false) {
    try {
      scanDir(path, append);
    } catch (Exception e) {
      conwriteln("scanning error: ", e.msg);
    }
    glconPostScreenRepaint();
  })("scan_dir", "scan the given directory; 2nd ard is \"append\" bool flag");

  conRegFunc!(() {
    ampPList.clear();
    glconPostScreenRepaint();
  })("pl_clear", "clear playlist");

  conRegFunc!(() { hideShowWindows(); })("win_toggle", "show/hide Amper windows");

  conRegFunc!((int plidx, bool forcestart=false) {
    if (!sdampwin.closed) {
      ampPList.playSongByIndex(plidx, forcestart);
      glconPostScreenRepaint();
    }
  })("song_play_by_index", "play song from playlist by index");

  conRegFunc!((){ if (!sdampwin.closed) ampPList.playPrevSong(); })("song_prev", "play previous song");
  conRegFunc!((bool forceplay=false){ if (!sdampwin.closed) ampPList.playNextSong(forceplay); })("song_next", "play next song");
  conRegFunc!((){ if (!sdampwin.closed) ampPList.playCurrentSong(); })("song_play", "play current song");
  conRegFunc!((){ if (!sdampwin.closed) ampPList.stopSong(); })("song_stop", "stop current song");
  conRegFunc!((){ aplayTogglePause(); })("song_pause_toggle", "pause/unpause current song");
  conRegFunc!((bool pause){ aplayPause(pause); })("song_pause", "pause/unpause current song, with bool arg");

  conRegFunc!((uint msecs){ aplaySeekMS(msecs*1000); })("song_seek_abs", "absolute seek, in seconds");
  conRegFunc!((int msecs){ aplaySeekMS(aplayCurTimeMS+msecs*1000); })("song_seek_rel", "relative seek, in seconds");


  concmd("exec /etc/amper.rc tan"); // global config
  conProcessQueue(); // load config
  loadHomeConfig();
  loadWindowConfig();
  // local config
  concmd("exec amper.rc tan");
  conProcessQueue();

  conProcessArgs!true(args);
  conProcessQueue(int.max/4);

  startRPCServer();
  scope(exit) stopRPCServer();

  if (!plVisibleChanged) plVisible = plVisibleCfg;
  if (!eqVisibleChanged) eqVisible = eqVisibleCfg;

  sdampwin.show();
  flushGui();

  /+
  if (plVisible) {
    /*
    static class EventFixupPListPosition {}
    glconCtlWindow.addEventListener((EventFixupPListPosition evt) {
      int xmain, ymain, wdtmain, hgtmain;
      int xpl, ypl, wdtpl, hgtpl;
      int xwork, ywork, wdtwork, hgtwork;
      getWorkAreaRect(xwork, ywork, wdtwork, hgtwork);
      sdampwin.getWindowRect(xmain, ymain, wdtmain, hgtmain);
      sdplwin.getWindowRect(xpl, ypl, wdtpl, hgtpl);
      conwriteln("work: (", xwork, ",", ywork, ")-(", wdtwork, "x", hgtwork, ")");
      conwriteln("main: (", xmain, ",", ymain, ")-(", wdtmain, "x", hgtmain, ")");
      conwriteln("list: (", xpl, ",", ypl, ")-(", wdtpl, "x", hgtpl, ")");
      if (GxRect(xmain, ymain, wdtmain, hgtmain).overlaps(GxRect(xpl, ypl, wdtpl, hgtpl))) {
        int nx = xmain;
        int ny = ymain+hgtmain;
        if (nx+wdtpl > xwork+wdtwork) nx = xwork+wdtwork-wdtpl;
        if (ny+hgtpl > ywork+hgtwork) ny = ywork+hgtwork-hgtpl;
        if (nx < xwork) nx = xwork;
        if (ny < ywork) ny = ywork;
        conwriteln("newpos: (", nx, ",", ny, ")");
        sdplwin.move(nx, ny);
        //sdplwin.move(50, 50);
        flushGui();
      }
    });
    sdplwin.visibleForTheFirstTime = delegate () {
      flushGui();
      switchToWindow(sdampwin);
      //glconCtlWindow.postTimeout(new EventFixupPListPosition(), 50);
    };
    */
    sdplwin.show();
  }
  +/

  prepareTrayIcon();
  flushGui();

  foreach (string path; args[1..$]) concmdf!"scan_dir \"%s\" tan"(path);

  glconCtlWindow.eventLoop(0);

  flushGui();
  conProcessQueue(int.max/4);
}
