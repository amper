/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module amperrpcsrv;

import core.time;

import std.concurrency;

import iv.cmdcon;
import iv.ncrpc;
import iv.vfs;

import amperrpcproto;
import amperskin;


// ////////////////////////////////////////////////////////////////////////// //
void startRPCServer () {
  srvsk.create(AmperRPCSocket);

  servertid = spawn(&rpcServerThread, thisTid);
}


void stopRPCServer () {
  UDSocket sk;
  sk.connect(AmperRPCSocket);
  sk.writeNum!ushort(RPCommand.Err);
}


// ////////////////////////////////////////////////////////////////////////// //
private:
__gshared Tid servertid;
__gshared UDSocket srvsk;


void rpcServerThread (Tid ownerTid) {
  rpcRegisterEndpoint!amperGetSongVisTitle(delegate () { return (ampMain !is null ? ampMain.wtitle.title : ""); });
  rpcRegisterEndpoint!amperScanDir(delegate (string path, bool append) { concmdf!"scan_dir \"%s\" %s"(path, append ? "tan" : "ona"); });
  rpcRegisterEndpoint!amperPlay(delegate () { concmd("song_play"); });
  rpcRegisterEndpoint!amperStop(delegate () { concmd("song_stop"); });
  rpcRegisterEndpoint!amperTogglePause(delegate () { concmd("song_pause_toggle"); });
  rpcRegisterEndpoint!amperClearPlaylist(delegate () { concmd("pl_clear"); });
  rpcRegisterEndpoint!amperConsoleCommand(delegate (string cmd) { concmd(cmd); });

  //conwriteln(rpcEndpointNames);

  for (;;) {
    receiveTimeout(Duration.min,
      (Variant v) { conwriteln("FUUUUUU"); },
    );
    {
      auto cl = srvsk.accept();
      auto cmd = cl.readNum!ushort;
      if (cmd != RPCommand.Call) {
        if (cmd != RPCommand.Err) conwriteln("invalid command");
        break;
      }
      //conwriteln("connection comes!");
      cl.rpcProcessCall;
    }
  }
}
