/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module amperrpcproto;

import iv.cmdcon;
import iv.ncrpc;


// ////////////////////////////////////////////////////////////////////////// //
__gshared string delegate () amperGetSongVisTitle;
__gshared void delegate (string path, bool append=false) amperScanDir;
__gshared void delegate () amperPlay;
__gshared void delegate () amperStop;
__gshared void delegate () amperTogglePause;
__gshared void delegate () amperClearPlaylist;
__gshared void delegate (string cmd) amperConsoleCommand;


// ////////////////////////////////////////////////////////////////////////// //
auto amperRPCall(alias func, A...) (A args) {
  UDSocket sk;
  sk.connect(AmperRPCSocket);
  return sk.rpcall!func(args);
}


// ////////////////////////////////////////////////////////////////////////// //
enum AmperRPCSocket = "/k8/amper";
