/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module aplayer;
private:

//version = amper_debug_decoder;

import core.atomic;
import core.time;

import std.concurrency;
import std.datetime;

import arsd.simpledisplay;

import iv.audiostream;
import iv.cmdcon;
import iv.cmdcongl;
import iv.simplealsa;
import iv.strex;
import iv.utfutil;
import iv.vfs;


// ////////////////////////////////////////////////////////////////////////// //
__gshared bool playerStarted = false;
__gshared bool scannerStarted = false;

__gshared Tid playertid;
__gshared Tid scannertid;


// ////////////////////////////////////////////////////////////////////////// //
public class EventFileLoaded { string filename; string album; string artist; string title; int durationms; bool success; }
public class EventFileComplete {}

// ////////////////////////////////////////////////////////////////////////// //
struct TMsgQuitReq {}

public void aplayStart () {
  if (!playerStarted) {
    playertid = spawn(&playerThread, thisTid);
    playerStarted = true;
  }
}

public void aplayStartScanner () {
  if (!scannerStarted) {
    scannertid = spawn(&scannerThread, thisTid);
    scannerStarted = true;
  }
}

public void aplayShutdown () {
  if (playerStarted) {
    playerStarted = false;
    playertid.send(TMsgQuitReq());
  }
  if (scannerStarted) {
    scannerStarted = false;
    scannertid.send(TMsgQuitReq());
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// reply with EventFileLoaded
struct TMsgPlayFileReq { string filename; bool forcestart; }
struct TMsgStopFileReq {}
struct TMsgTogglePauseReq {}
struct TMsgPauseReq { bool pause; }
struct TMsgSeekReq { int timems; }

//TODO: queue this
public void aplayPlayFile (string fname, bool forcestart) { if (playerStarted) playertid.send(TMsgPlayFileReq(fname.length ? fname : "", forcestart)); }
public void aplayStopFile () { if (playerStarted) playertid.send(TMsgStopFileReq()); }
public void aplayTogglePause () { if (playerStarted) playertid.send(TMsgTogglePauseReq()); }
public void aplayPause (bool pause) { if (playerStarted) playertid.send(TMsgPauseReq(pause)); }
public void aplaySeekMS (int timems) { if (playerStarted) playertid.send(TMsgSeekReq(timems)); }


// ////////////////////////////////////////////////////////////////////////// //
// convert from [-20..20] to [0..27]
public int aplayGetEqBand (int idx) {
  int v = 14;
  if (idx >= 0 && idx < 10) {
    v = alsaEqBands[idx];
    if (v < -20) v = -20; else if (v > 20) v = 20;
    v = 27*(v+20)/40;
  }
  return v;
}

// convert from [0..27] to [-20..20]
public void aplaySetEqBand (int idx, int v) {
  if (idx >= 0 && idx < 10) {
    if (v < 0) v = 0; else if (v > 27) v = 27;
    v = (v != 14 ? (40*v/27)-20 : 0);
    alsaEqBands[idx] = v;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// reply with EventFileLoaded
struct TMsgPlayGainReq {
  int prc; // percents
}

public void aplayPlayGain (int prc) { if (playerStarted) playertid.send(TMsgPlayGainReq(prc)); else alsaGain = prc; }
public int aplayPlayGain () { return alsaGain; }


// ////////////////////////////////////////////////////////////////////////// //
public bool aplayIsPlaying () { return atomicLoad(aplPlaying); }
public bool aplayIsPaused () { return atomicLoad(aplPaused); }
//public int aplayCurTime () { return atomicLoad(aplCurTime)/1000; }
//public int aplayCurTimeMS () { return atomicLoad(aplCurTime); }
public int aplayCurTime () { lockBuffer(); scope(exit) unlockBuffer(); return cast(int)(aplFramesFed/atomicLoad(aplSampleRate)); }
public int aplayCurTimeMS () { lockBuffer(); scope(exit) unlockBuffer(); return cast(int)(aplFramesFed*1000/atomicLoad(aplSampleRate)); }
public int aplayTotalTime () { return atomicLoad(aplTotalTime)/1000; }
public int aplayTotalTimeMS () { return atomicLoad(aplTotalTime); }

public int aplaySampleRate () { return atomicLoad(aplSampleRate); }
public bool aplayIsStereo () { return (atomicLoad(aplChannels) == 2); }


// ////////////////////////////////////////////////////////////////////////// //
shared bool aplPlaying = false;
shared bool aplPaused = false;
//shared int aplCurTime = 0;
shared int aplTotalTime = 0;
shared int aplSampleRate = 48000;
shared int aplChannels = 2;
__gshared ulong aplFramesFed = 0; // for current track


// ////////////////////////////////////////////////////////////////////////// //
enum BUF_SIZE = 4096*10;
// read/write only when buffer is locked!
__gshared short[BUF_SIZE][2] buffers;
__gshared uint curbuffer = 0; // current buffer decoder is filling; alternate buffer *can* have some data
__gshared uint[2] buffrmused = 0; // for both buffers
shared bool buflocked = false;


void lockBuffer () nothrow @nogc {
  import core.atomic;
  while (!cas(&buflocked, false, true)) {}
}

void unlockBuffer () nothrow @nogc {
  import core.atomic;
  atomicStore(buflocked, false);
}


void withLockedBuffer (scope void delegate () dg) {
  lockBuffer();
  scope(exit) unlockBuffer();
  dg();
}


bool hasBufferedData () nothrow @nogc {
  lockBuffer();
  scope(exit) unlockBuffer();
  return ((buffrmused[0]|buffrmused[1]) != 0);
}


// ////////////////////////////////////////////////////////////////////////// //
struct TMsgPingDecoder {}
struct TMsgSomeDataDecoded {}
struct TMsgReplaceSIO { shared AudioStream sio; }

// audio decoding thread; should keep buffer filled
void decoderThread (Tid ownerTid) {
  AudioStream sio = null;
  bool hasmorefrms = false;
  int newtime = -666;
  bool doQuit = false;
  bool waitingForDecoder = true; // waiting for decoder to warm up
  version(amper_debug_decoder) conwriteln("decoder thread started");
  while (!doQuit) {
    bool longWait = true;
    if (sio is null || !sio.valid) hasmorefrms = false;
    withLockedBuffer((){
      if (!hasmorefrms) { longWait = ((buffrmused[0]|buffrmused[1]) == 0); return; }
      if (buffrmused[curbuffer] == BUF_SIZE/sio.channels) { longWait = (buffrmused[curbuffer^1] != 0); return; }
      longWait = false;
    });
    receiveTimeout((longWait ? 2.hours : Duration.min),
      (TMsgQuitReq req) {
        doQuit = true;
      },
      (TMsgSeekReq req) {
        newtime = req.timems;
        if (newtime < 0) newtime = 0;
        version(amper_debug_decoder) conwriteln("decoder: seek request, newtime=", newtime);
        withLockedBuffer(() {
          buffrmused[] = 0;
          curbuffer = 0;
          hasmorefrms = (sio !is null && sio.valid);
        });
        waitingForDecoder = true;
      },
      (TMsgPingDecoder req) {
        version(amper_debug_decoder) conwriteln("decoder: ping");
        // do nothing here, this is just a ping to go on with decoding
      },
      (TMsgReplaceSIO req) {
        AudioStream newsio = cast()req.sio; // remove `shared`
        if (sio !is newsio) {
          if (sio !is null) { sio.close(); delete sio; }
        }
        sio = newsio;
        withLockedBuffer(() {
          curbuffer = 0;
          buffrmused[] = 0;
          hasmorefrms = (sio !is null && sio.valid);
        });
        waitingForDecoder = true;
      },
    );
    if (doQuit) break;
    bool sendPing = false;
    decodeloop: for (;;) {
      if (sio is null || !sio.valid) hasmorefrms = false;
      if (!hasmorefrms) {
        sendPing = true;
        newtime = -666;
        // switch buffers if necessary
        withLockedBuffer(() {
          if (buffrmused[curbuffer^1] == 0) curbuffer ^= 1;
        });
        break decodeloop;
      }
      // seek?
      if (newtime != -666) {
        version(amper_debug_decoder) conwriteln("decoder: seeking to ", newtime);
        int tm = newtime;
        newtime = -666;
        assert(sio !is null && sio.valid);
        if (tm >= sio.timeTotal) tm = (sio.timeTotal ? sio.timeTotal-1 : 0);
        sio.seekToTime(cast(uint)tm);
        //conwriteln("tm=", tm, "; timeRead=", sio.timeRead, "; framesRead=", sio.framesRead, "; frok=", sio.timeRead*sio.rate/1000);
        aplFramesFed = sio.framesRead;
        hasmorefrms = (sio !is null && sio.valid);
        continue decodeloop;
      }
      // it is safe to work with current buffer without the lock here
      uint bsmpused = buffrmused[curbuffer]*sio.channels;
      version(amper_debug_decoder) conwriteln("decoder: curbuffer=", curbuffer, "; used[0]=", buffrmused[0], "; used[1]=", buffrmused[1]);
      // fill current buffer, switch to next
      if (bsmpused < BUF_SIZE) {
        // decode
        int frmread = sio.readFrames(buffers[curbuffer].ptr+bsmpused, (BUF_SIZE-bsmpused)/sio.channels);
        version(amper_debug_decoder) conwriteln("decoder: frmread=", frmread);
        if (frmread <= 0) {
          hasmorefrms = false; // no more frames, we're done
        } else {
          bsmpused = (buffrmused[curbuffer] += cast(uint)frmread)*sio.channels;
        }
        assert(bsmpused <= BUF_SIZE);
      }
      // switch buffers, if alternate buffer was drained (and fill it)
      version(amper_debug_decoder) conwriteln("decoder: bsmpused=", bsmpused, "; BUF_SIZE=", BUF_SIZE);
      if (bsmpused == BUF_SIZE) {
        // but here we should aquire lock
        withLockedBuffer(() {
          if (buffrmused[curbuffer^1] == 0) {
            version(amper_debug_decoder) conwriteln("decoder: curbuffer=", curbuffer, " is full, switching buffers; used[0]=", buffrmused[0], "; used[1]=", buffrmused[1], "; hasmorefrms=", hasmorefrms);
            curbuffer ^= 1;
          }
          if (!hasmorefrms || (buffrmused[0]|buffrmused[1]) != 0) sendPing = true;
        });
        // get out of decoder if current buffer is still full
        if (buffrmused[curbuffer] == BUF_SIZE/sio.channels) break decodeloop;
      }
    }
    //version(amper_debug_decoder) conwriteln("decoder: done; curbuffer=", curbuffer, "; used[0]=", buffrmused[0], "; used[1]=", buffrmused[1], "; sendPing=", sendPing, "; waitingForDecoder=", waitingForDecoder);
    // ping owner thread
    if (sendPing && waitingForDecoder) {
      waitingForDecoder = false;
      ownerTid.send(TMsgSomeDataDecoded());
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared short[4096] sndplaybuf;

void playerThread (Tid ownerTid) {
  AudioStream sio;
  scope(exit) if (sio !is null && sio.valid) sio.close();
  bool doQuit = false;
  string newfilereq = null;
  bool forcestart = false;
  bool waitingForDecoder = true; // waiting for decoder to warm up
  bool paused = false;

  uint realRate = alsaGetBestSampleRate(48000);
  conwriteln("real sampling rate: ", realRate);
  if (realRate != 44100 && realRate != 48000) {
    realRate = 48000;
    conwriteln("WARNING! something is wrong with ALSA! trying to fix it...");
  }

  auto decodertid = spawn(&decoderThread, thisTid);

  while (!doQuit) {
    //conwriteln("***: hasBufferedData=", hasBufferedData, "; paused=", paused);
    receiveTimeout((hasBufferedData && !paused ? Duration.min : 42.seconds),
      (TMsgQuitReq req) {
        doQuit = true;
      },
      (TMsgPlayFileReq req) {
        newfilereq = (req.filename.length ? req.filename : "");
        forcestart = req.forcestart;
      },
      (TMsgPlayGainReq req) {
        if (req.prc < 0) req.prc = 0;
        if (req.prc > 200) req.prc = 200;
        alsaGain = req.prc;
        //conwriteln("prc=", alsaGain);
      },
      (TMsgStopFileReq req) {
        paused = false;
        if (sio !is null) { sio.close(); delete sio; }
        decodertid.send(TMsgReplaceSIO(null));
        if (alsaIsOpen) alsaShutdown();
      },
      (TMsgTogglePauseReq req) {
        if (hasBufferedData) paused = !paused; else paused = false;
      },
      (TMsgPauseReq req) {
        if (hasBufferedData) paused = req.pause;
      },
      (TMsgSeekReq req) {
        int newtime = req.timems;
        if (newtime < 0) newtime = 0;
        version(amper_debug_decoder) conwriteln("player: seek request, newtime=", newtime);
        decodertid.send(TMsgSeekReq(newtime));
        waitingForDecoder = true; // wait while decoder is warming up
      },
      (TMsgSomeDataDecoded req) {
        waitingForDecoder = false;
        version(amper_debug_decoder) conwriteln("decoder sent ping; hasBufferedData=", hasBufferedData);
      },
    );

    if (doQuit) break;

    if (newfilereq !is null) {
      // create new sio
      auto reply = new EventFileLoaded();
      reply.filename = newfilereq;
      reply.success = false;
      newfilereq = null;
      bool wasplaying = hasBufferedData();
      sio = AudioStream.detect(VFile(reply.filename));
      if (sio !is null && sio.valid) {
        reply.durationms = cast(int)sio.timeTotal;
        reply.album = sio.album;
        reply.artist = sio.artist;
        reply.title = sio.title;
        reply.success = true;
        if (forcestart) paused = false; else paused = !wasplaying;
        // setup new parameters
        atomicStore(aplTotalTime, cast(int)sio.timeTotal);
        atomicStore(aplSampleRate, cast(int)sio.rate);
        atomicStore(aplChannels, cast(int)sio.channels);
        withLockedBuffer(() { aplFramesFed = 0; });
        // (re)init alsa
        if (!alsaIsOpen || alsaRate != sio.rate || alsaChannels != sio.channels) {
          if (alsaIsOpen) alsaShutdown();
          if (!alsaInit(sio.rate, sio.channels)) assert(0, "cannot init ALSA playback");
        }
        waitingForDecoder = true;
        decodertid.send(TMsgReplaceSIO(cast(shared)sio)); // notify decoder that we (possibly) have new sio
        sio = null; // now sio is owned by decoder
      } else {
        if (alsaIsOpen) alsaShutdown();
      }
      if (glconCtlWindow !is null) glconCtlWindow.postEvent(reply);
      //conwriteln("starting...");
      continue;
    }

    if (waitingForDecoder) continue; // still waiting for decoder to warm up

    //conwriteln("hasBufferedData=", hasBufferedData);
    if (!hasBufferedData) {
      paused = false;
      atomicStore(aplPaused, false);
      atomicStore(aplPlaying, false);
      //atomicStore(aplCurTime, 0);
      atomicStore(aplTotalTime, 0);
      withLockedBuffer(() { aplFramesFed = 0; });
      //conwriteln("!!! 000");
      continue;
    }

    if (!paused) {
      atomicStore(aplPaused, false);
      if (hasBufferedData()) {
        if (!alsaIsOpen) {
          if (!alsaInit(atomicLoad(aplSampleRate), cast(ubyte)atomicLoad(aplChannels))) assert(0, "cannot init ALSA playback");
        }
        uint samplesToSend = 0;
        withLockedBuffer(() {
          import core.stdc.string : memcpy, memmove;
          // drain current buffer
          uint playbuf = curbuffer^1;
          uint frmleft = buffrmused[playbuf];
          assert(frmleft > 0);
          uint chans = atomicLoad(aplChannels);
          assert(chans == 1 || chans == 2);
          uint loadsamples = frmleft*chans;
          if (loadsamples > sndplaybuf.length) loadsamples = cast(uint)sndplaybuf.length;
          //conwriteln("loadsamples=", loadsamples, "; smpleft=", frmleft*chans, "; framesfed=", aplFramesFed);
          atomicStore(aplPaused, false);
          atomicStore(aplPlaying, true);
          //atomicStore(aplFramesFed, atomicLoad(aplFramesFed)+loadsamples/chans);
          aplFramesFed += loadsamples/chans;
          // copy bytes to play
          memcpy(sndplaybuf.ptr, buffers[playbuf].ptr, loadsamples*2);
          // remove bytes from buffer
          uint oldsmp = buffrmused[playbuf]*chans;
          uint delsmp = loadsamples;
          assert(delsmp <= oldsmp);
          if (delsmp != oldsmp) memmove(buffers[playbuf].ptr, buffers[playbuf].ptr+delsmp, (oldsmp-delsmp)*2);
          buffrmused[playbuf] -= delsmp/chans;
          samplesToSend = loadsamples;
          // ping decoder (we want more data)
          if (buffrmused[playbuf] == 0) decodertid.send(TMsgPingDecoder());
        });
        //conwriteln("004: ", alsaIsOpen, "; ", samplesToSend, " : ", sndplaybuf.length);
        alsaWriteShort(sndplaybuf[0..samplesToSend]);
      }
      if (!hasBufferedData) {
        if (alsaIsOpen) alsaShutdown(); // so it will finish playing
        atomicStore(aplPaused, false);
        atomicStore(aplPlaying, false);
        withLockedBuffer(() { aplFramesFed = 0; });
        atomicStore(aplTotalTime, 0);
        //conwriteln("!!! 001");
        if (glconCtlWindow !is null) glconCtlWindow.postEvent(new EventFileComplete());
        decodertid.send(TMsgReplaceSIO(null)); // no more
      }
    } else {
      atomicStore(aplPlaying, true);
      atomicStore(aplPaused, true);
      if (alsaIsOpen) alsaShutdown();
    }
  }

  // quited
  decodertid.send(TMsgQuitReq());
}


// ////////////////////////////////////////////////////////////////////////// //
shared static this () {
  alsaEqBands[] = 0;

  conRegVar!alsaRQuality(0, 10, "rsquality", "resampling quality; 0=worst, 10=best, default is 8");
  conRegVar!alsaDevice("device", "audio output device");
  //conRegVar!alsaGain(-100, 1000, "gain", "playback gain (0: normal; -100: silent; 100: 2x)");
  conRegVar!alsaLatencyms(5, 5000, "latency", "playback latency, in milliseconds");
  conRegVar!alsaEnableResampling("use_resampling", "allow audio resampling?");
  conRegVar!alsaEnableEqualizer("use_equalizer", "allow audio equalizer?");

  // lol, `std.trait : ParameterDefaults()` blocks using argument with name `value`
  conRegFunc!((int idx, byte value) {
    if (value < -70) value = -70;
    if (value > 30) value = 30;
    if (idx >= 0 && idx < alsaEqBands.length) {
      if (alsaEqBands[idx] != value) {
        alsaEqBands[idx] = value;
      }
    } else {
      conwriteln("invalid equalizer band index: ", idx);
    }
  })("eq_band", "set equalizer band #n to v (band 0 is preamp)");

  conRegFunc!(() {
    alsaEqBands[] = 0;
  })("eq_reset", "reset equalizer");
}


// ////////////////////////////////////////////////////////////////////////// //
public class EventFileScanned { string filename; string album; string artist; string title; int durationms; bool success; }
// reply with EventFileScanned
struct TMsgScanFileReq { string fname; }
struct TMsgScanCancelReq { string fname; }

__gshared bool[string] scanQueue;


public void aplayQueueScan(T:const(char)[]) (T filename) {
  static if (is(T == typeof(null))) {
    aplayQueueScan("");
  } else {
    static if (is(T == string)) alias fn = filename; else auto fn = filename.idup;
    if (scannerStarted) {
      //conwriteln("zqueued: '", filename, "'...");
      scannertid.send(TMsgScanFileReq(fn));
    } else {
      //conwriteln("xqueued: '", filename, "'...");
      scanQueue[fn] = true;
    }
  }
}


public void aplayCancelScan(T:const(char)[]) (T filename) {
  static if (is(T == typeof(null))) {
    aplayQueueScan("");
  } else {
    if (scannerStarted) {
      static if (is(T == string)) alias fn = filename; else auto fn = filename.idup;
      scannertid.send(TMsgScanCancelReq(fn));
    } else {
      //conwriteln("xdequeued: '", filename, "'...");
      scanQueue.remove(filename);
    }
  }
}


void scannerThread (Tid ownerTid) {
  bool doQuit = false;
  //conwriteln("scan tread started...");
  while (!doQuit) {
    receiveTimeout((scanQueue.length ? Duration.min : 1.hours),
      (TMsgQuitReq req) {
        doQuit = true;
      },
      (TMsgScanFileReq req) {
        scanQueue[req.fname] = true;
        //conwriteln("queued: '", req.fname, "'...");
      },
      (TMsgScanCancelReq req) {
        //conwriteln("dequeued: '", req.fname, "'...");
        scanQueue.remove(req.fname);
      },
    );

    if (doQuit) break;
    if (scanQueue.length == 0) continue;

    string fname = scanQueue.byKey.front;
    EventFileScanned reply = new EventFileScanned();
    reply.filename = scanQueue.byKey.front;
    reply.success = false;
    //conwriteln("scanning '", reply.filename, "'...");
    auto sio = AudioStream.detect(VFile(reply.filename), true); // only metadata
    if (sio !is null && sio.valid) {
      reply.album = sio.album;
      reply.artist = sio.artist;
      reply.title = sio.title;
      reply.durationms = cast(int)sio.timeTotal;
      reply.success = true;
    }
    if (sio !is null) {
      try { sio.close(); } catch (Exception e) {}
      delete sio;
    }
    scanQueue.remove(reply.filename);
    //conwriteln("  scanned '", reply.filename, "': ", reply.success);
    if (glconCtlWindow !is null) glconCtlWindow.postEvent(reply);
  }
}
