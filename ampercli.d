/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module ampercli;

import std.path : absolutePath;

import iv.alice;
import iv.cmdcon;
import iv.vfs.io;

import amperrpcproto;


void main (string[] args) {
  if (args.length < 2) {
    conwrite(
      "usage: ampercli command [args]\n",
      "commands:\n",
      "  add path\n",
      "  replace path\n",
      "  play\n",
      "  stop\n",
      "  pause\n",
      "  clear\n",
      "  title\n",
    );
    return;
  }
  //amperRPCall!amperScanDir("/home/www/dox/muzax/irij/flac/2006_rusalka");
  // hack! convert anything that looks like a file to a full path
  foreach (ref string s; args[1..$]) {
    import std.file : exists;
    try {
      if (s.exists) s = absolutePath(s);
    } catch (Exception e) {}
  }
  // process args
  for (usize aidx = 1; aidx < args.length; ) {
    string cmd = args[aidx++];
    switch (cmd) {
      case "add":
        if (aidx >= args.length) assert(0, "no argument for command '"~cmd~"'");
        amperRPCall!amperScanDir(absolutePath(args[aidx++]), true);
        break;
      case "replace":
        if (aidx >= args.length) assert(0, "no argument for command '"~cmd~"'");
        amperRPCall!amperScanDir(absolutePath(args[aidx++]), false);
        break;
      case "play": amperRPCall!amperPlay(); break;
      case "stop": amperRPCall!amperStop(); break;
      case "pause": amperRPCall!amperTogglePause(); break;
      case "clear": amperRPCall!amperClearPlaylist(); break;
      case "title": writeln(amperRPCall!amperGetSongVisTitle()); break;
      default:
        if (cmd.length && cmd[0] == '+') {
          string command = cmd[1..$];
          while (aidx < args.length) {
            string s = args[aidx++];
            if (s.length == 0) continue;
            if (command.length && command[$-1] > ' ') command ~= ' ';
            command ~= s;
          }
          amperRPCall!amperConsoleCommand(command);
          return;
        }
        import std.file;
        if (cmd.exists) {
          amperRPCall!amperClearPlaylist();
          --aidx;
          while (aidx < args.length) amperRPCall!amperScanDir(absolutePath(args[aidx++]), true);
          amperRPCall!amperPlay();
          return;
        }
        assert(0, "invalid command: '"~cmd~"'");
    }
  }
}
